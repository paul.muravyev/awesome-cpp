# awesome-cpp (c)

A curated list of awesome C++ (or C) frameworks, libraries, resources, and shiny things.

- [Awesome C++](#awesome-cpp)
	- [Standard Libraries](#standard-libraries)
	- [Frameworks](#frameworks)
	- [Artificial Intelligence](#artificial-intelligence)
	- [Asynchronous Event Loop](#asynchronous-event-loop)
	- [Audio](#audio)
	- [Authentication and OAuth](#authentication-and-oauth)
	- [Biology](#biology)
	- [BitTorrent](#bittorrent)
	- [Blockchain](#blockchain)
	- [Chemistry](#chemistry)
	- [CLI](#cli)
	- [Computer vision](#computer-vision)
	- [Compression](#compression)
	- [Concurrency](#concurrency)
	- [Configuration](#configuration)
	- [Containers](#containers)
	- [Cryptography](#cryptography)
	- [CSV](#csv)
	- [Database](#database)
		- [Database Tools](#database-tools)
	- [Data Structures](#data-structures)
	    - [Big Integer](#big-integer)
		- [Graph](#graph)
		- [Interval Tree](#interval-tree)
		- [Miscellaneous Data Structures and Algorithms](#miscellaneous-data-structures-and-algorithms)
		- [Queue](#queue)
		- [Segment Tree](#segment-tree)
	- [Data visualization](#data-visualization)	
	- [Debug](#debug)
	- [Distributed Systems](#distributed-systems)
	- [Documentation](#documentation)
	- [DSP](#dsp)
	- [Email](#email)
    - [Embeddable Scripting Languages](#embeddable-scripting-languages)
	- [Financial](#financial)
	- [Font](#font)
	- [Game Engine](#game-engine)
	- [GUI](#gui)
	- [Graphics](#graphics)
	- [Image Processing](#image-processing)
	- [Industrial automation](#industrial-automation) 
	- [Internationalization](#internationalization)
	- [Inter-process communication](#inter-process-communication)
	- [JSON](#json)
	- [Logging](#logging)
	- [Machine Learning](#machine-learning)
	- [Math](#math)
	- [Memory Allocation](#memory-allocation)
	    - [Memory Mapping](#memory-mapping)
		- [Memory Pool](#memory-pool)
	- [Messaging](#messaging)
		- [Kafka](#kafka)
    - [Microsoft Office](#microsoft-office)
        - [Microsoft Excel](#microsoft-excel)		
	- [Multimedia](#multimedia)
	- [Networking](#networking)
	- [Node.js](#node.js)
	- [PDF](#pdf)
	- [Performance](#performance)
	- [Physics](#physics)
	- [QrCode](#qrcode)
	- [Reflection](#reflection)
	- [Regular Expression](#regular-expression)
	- [Robotics](#robotics)
	- [Scientific Computing](#scientific-computing)
	- [Scripting](#scripting)
	- [Security](#security)
	- [Serialization](#serialization)
		- [BER TLV](#ber-tlv)
	- [Serial Port](#serial-port)
	- [Server](#server)
	- [Sorting](#sorting)
	- [String Manipulation](#string-manipulation)
	- [Template Engines](#template-engines)
	- [UUID](#uuid)
	- [Validation](#validation)
	- [Video](#video)
	- [Virtual Machines](#virtual-machines)
	- [Web Application Framework](#web-application-framework)
	- [WebAssembly](#webassembly)
	- [XML](#xml)
	- [Yaml](#yaml)
	- [Miscellaneous](#miscellaneous)
- [Software](#software)
	- [Compiler](#compiler)
	- [Online Compiler](#online-compiler)
	- [Debugger](#debugger)
	- [Integrated Development Environment](#integrated-development-environment)
	- [Build Systems](#build-systems)
	- [Static Code Analysis](#static-code-analysis)
	- [System tools](#system-tools)
	- [Testing tools](#testing-tools)
	- [Coding Style Tools](#coding-style-tools)
- [Resources](#resources)
	- [API Design](#api-design)
	- [Articles](#articles)
	- [Beginner resources online](#beginner-resources-online)
	- [Books](#books)
	- [Coding Standards](#coding_standards)
	- [Coding Style](#coding-style)
	- [Podcasts](#podcasts)
	- [Talks](#talks)
	- [Videos](#videos)
	- [Websites](#websites)
	- [Weblogs](#Weblogs)
	- [Other Awesome Projects](#other-awesome-projects)
- [Other Awesome Lists](#other-awesome-lists)

## Standard Libraries
*C++ Standard Library - including STL Containers, STL Aglorithm, STL Functional, etc.*

* [c_std](https://github.com/KaisenAmin/c_std) - Implementation of C++ standard libraries in C.
* [C++ Standard Library](https://en.wikipedia.org/wiki/C%2B%2B_Standard_Library) - A collection of classes and functions, which are written in the core language and part of the C++ ISO Standard itself.
* [Standard Template Library](https://en.wikipedia.org/wiki/Standard_Template_Library) - The Standard Template Library (STL).
* [C POSIX library](https://en.wikipedia.org/wiki/C_POSIX_library) - A specification of a C standard library for POSIX systems.
* [CPPAtomic](https://github.com/macmade/CPPAtomic) - Replacement of std::atomic supporting non trivially-copyable types.
* [function2](https://github.com/Naios/function2) - Improved and configurable drop-in replacement to std::function that supports move only types, multiple overloads and more.
* [ISO C++ Standards Committee](https://github.com/cplusplus) - ISO/IEC JTC1/SC22/WG21 - The C++ Standards Committee. [website](http://www.open-std.org/JTC1/SC22/WG21/)
* [The GNU C Library](https://www.gnu.org/software/libc/manual) - The purpose of this manual is to tell you how to use the facilities of the GNU C Library.
* [result](https://github.com/p-ranav/result) - Result<T, E> for Modern C++.
* [te](https://github.com/boost-ext/te) - C++17 Run-time polymorphism (type erasure) library.

## Frameworks
*C++ generic frameworks and libraries.*

* [C++ Micro Services](https://github.com/CppMicroServices/CppMicroServices) - An OSGi-like C++ dynamic module system and service registry.
* [abseil-cpp](https://github.com/abseil/abseil-cpp) - Abseil C++ Common Libraries.
* [AUI](https://github.com/aui-framework/aui) - Declarative UI toolkit for C++20.
* [ananas](https://github.com/loveyacper/ananas) - A C++11 RPC framework based on future and protobuf, with utility: timer,ssl,future/promise,log,coroutine,etc
* [Apache C++ Standard Library](http://stdcxx.apache.org/) - STDCXX, A collection of algorithms, containers, iterators, and other fundamental components.
* [APR](http://apr.apache.org/) - Apache Portable Runtime. Another library of cross-platform utility functions.
* [ASL](http://stlab.adobe.com/) - Adobe Source Libraries provides peer-reviewed and portable C++ source libraries.
* [async_simple](https://github.com/alibaba/async_simple) - Simple, light-weight and easy-to-use asynchronous components.
* [Boost](https://github.com/boostorg) - A large collection of generic C++ libraries. [website](http://www.boost.org/)
* [BDE](https://github.com/bloomberg/bde) - The BDE Development Environment from Bloomberg Labs.
* [C++ Workflow](https://github.com/sogou/workflow) - C++ Parallel Computing and Asynchronous Networking Engine.
* [CCAN](http://ccodearchive.net) - Modelled after Perl's CPAN, this is a big collection of code that does stuff.
* [Celix](https://github.com/apache/celix) - Apache Celix is a framework for C, C++14 and C++17 to develop dynamic modular software applications using component and in-process service-oriented programming. Apache Celix is inspired by the [OSGi specification](https://www.osgi.org/) adapted for C and C++.
* [CGraph](https://github.com/ChunelFeng/CGraph) - A cross-platform DAG framework based on C++ without any 3rd-party.
* [cockpit](https://github.com/cockpit-project/cockpit) - Cockpit is an interactive server admin interface. It is easy to use and very lightweight. Cockpit interacts directly with the operating system from a real Linux session in a browser.
* [co](https://github.com/idealvin/co) - includes coroutine library (golang-style), network library (tcp/http/rpc), log library, command line and configuration file parsing library, unit testing framework, json library and other basic components.
* [cocoyaxi](https://github.com/idealvin/cocoyaxi) - A collection of C++ libraries, containing a go-style coroutine library, log, JSON, RPC framework and other base components.
* [Coost](https://github.com/idealvin/coost) - A tiny boost library in C++11.
* [Cinder](https://libcinder.org/) - A community-developed, free and open source library for professional-quality creative coding.
* [Cxxomfort](http://ryan.gulix.cl/fossil.cgi/cxxomfort/) - A small, header-only library that backports various facilities from more recent C++ Standards to C++03 and later.
* [EASTL](https://github.com/electronicarts/EASTL) - Electronic Arts Standard Template Library.
* [esl](https://github.com/SLukasDE/esl) - Enterprise Support Library - An attempt to define important C++ classes for developing enterprise applications.
* [ETL](https://github.com/ETLCPP/etl) - Embedded Template Library.
* [EFL](https://www.enlightenment.org/) - Large collection of useful data structures and functions.
* [Fatal](https://github.com/facebook/fatal) - Fatal (Facebook Template Library) is a library for fast prototyping software in modern C++.
* [ffead-cpp](https://github.com/sumeetchhetri/ffead-cpp) - Framework for Enterprise Application Development in C++, HTTP1/HTTP2/HTTP3 compliant, Supports multiple server backends.
* [Flecs](https://github.com/SanderMertens/flecs) - Flecs is a fast and lightweight Entity Component System for C & C++ that lets you build games and simulations with millions of entities.
* [Folly](https://github.com/facebook/folly) - An open-source C++ library developed and used at Facebook.
* [foundation_lib](https://github.com/mjansson/foundation_lib) - This library provides a cross-platform foundation library in C providing basic support data types and functions to write applications and games in a platform-independent fashion.
* [FunctionalPlus](https://github.com/Dobiasd/FunctionalPlus) - Functional Programming Library for C++. Write concise and readable C++ code.
* [GLib](https://wiki.gnome.org/Projects/GLib) - GLib provides the core application building blocks for libraries and applications written in C.
* [gnulib](https://www.gnu.org/software/gnulib/) - Collection of common GNU code.
* [goaccess](https://github.com/allinurl/goaccess) - GoAccess is a real-time web log analyzer and interactive viewer that runs in a terminal in *nix systems or through your browser.
* [itlib](https://github.com/iboB/itlib) - A collection of std-like single-header C++ libraries.
* [helio](https://github.com/romange/helio) - A modern framework for backend development based on io_uring Linux interface.
* [JUCE](https://github.com/julianstorer/JUCE) - An all-encompassing C++ class library for developing cross-platform software.
* [Kigs framework](https://github.com/Kigs-framework/kigs) - A free and open source C++ modular multi-purpose cross platform RAD framework.
* [libdjb](http://www.fefe.de/djb/) - Collection of libraries doing various things.
* [libdynamic](https://github.com/fredrikwidlund/libdynamic) - libdynamic is a utility library for C that will give you dynamic data structures like buffers, lists, vectors, maps and strings. It also includes asynchronous worker pools, and a core event driven framework. It is used, for example, in conjunction with [libreactor](https://github.com/fredrikwidlund/libreactor) handling many millions of HTTP transactions daily for over 5 years.
* [libPhenom](https://github.com/facebook/libphenom) - libPhenom is an eventing framework for building high performance and high scalability systems in C.
* [LibSourcey](https://github.com/sourcey/libsourcey) - C++11 evented IO for real-time video streaming and high performance networking applications.
* [LibU](https://github.com/koanlogic/libu) - A multiplatform utility library written in C.
* [libnih](https://github.com/keybuk/libnih) - Lightweight library of C functions and structures.
* [libreactor](https://github.com/fredrikwidlund/libreactor) - Extendable event driven high performance C-abstractions.
* [libreactorng](https://github.com/fredrikwidlund/libreactorng) - High performance, robust and secure, generic event-driven application framework for Linux.
* [libxutils](https://github.com/kala13x/libxutils) - Simple and yet powerful cross-platform C library providing data structures, algorithms and much more.
* [Loki](http://loki-lib.sourceforge.net/) - A C++ library of designs, containing flexible implementations of common design patterns and idioms.
* [Melon](https://github.com/Water-Melon/Melon) - A generic cross-platform asynchronous high-performance C framework, including a lot of components and a new coroutine script language Melang.
* [Memoria](https://github.com/victor-smirnov/memoria) - Memoria is an advanced data engineering Framework written in modern C++.
* [MiLi](https://bitbucket.org/fudepan/mili/) - Minimal headers-only C++ Library.
* [mmx](https://github.com/vurtun/mmx) - Collection of single-header libraries.
* [OpenFrameworks](https://github.com/openframeworks/openFrameworks) - A cross platform open source toolkit for creative coding in C++. [website](http://www.openframeworks.cc/)
* [par](https://github.com/prideout/par) - Bunch of single-file libraries.
* [pico](https://github.com/foxweb/pico) - This is a very simple HTTP server for Unix, using fork(). It's very easy to use.
* [plibsys](https://github.com/saprykin/plibsys) - Cross-platform system C library. Zero third-party dependencies, uses only native system calls.
* [Qt](https://www.qt.io/download-open-source/) - A cross-platform application and UI framework.
* [Reason](http://code.google.com/p/reason/) - A cross platform framework designed to bring the ease of use of Java, .Net, or Python to developers who require the performance and strength of C++.
* [ROOT](https://root.cern.ch/) - A set of OO frameworks with all the functionality needed to handle and analyze large amounts of data in a very efficient way. Used at CERN.
* [rpp](https://github.com/TheNumbat/rpp) - Minimal Rust-inspired C++20 STL replacement.
* [SaneCppLibraries](https://github.com/Pagghiu/SaneCppLibraries) - C++ platform abstraction libraries for macOS, Windows and Linux. [website](https://pagghiu.github.io/SaneCppLibraries/)
* [Seastar](https://github.com/scylladb/seastar) - An advanced, open-source C++ framework for high-performance server applications on modern hardware.
* [sc](https://github.com/tezc/sc) - Common C libraries and data structures. (C99).
* [Siv3D](https://github.com/Siv3D/OpenSiv3D) - Siv3D (OpenSiv3D) is a C++20 framework for creative coding (2D/3D games, media art, visualizers, and simulators). [website](https://siv3d.github.io/)
* [stb](https://github.com/nothings/stb) - Range of single-file libraries for C.
* [STLport](http://www.stlport.org/) - An exemplary version of STL.
* [STXXL](http://stxxl.sourceforge.net/) - Standard Template Library for Extra Large Data Sets.
* [Tars](https://github.com/TarsCloud/Tars) - Tars is a high-performance RPC framework based on name service and Tars protocol, also integrated administration platform, and implemented hosting-service via flexible schedule.
* [tbox](https://github.com/tboox/tbox) - A glib-like multi-platform c library. [website](http://tboox.org/)
* [tinyheaders](https://github.com/RandyGaul/tinyheaders) - Collection of header-only libraries, primarily oriented toward game development.
* [Ultimate++](http://www.ultimatepp.org/) - A C++ cross-platform rapid application development framework.
* [uSTL](http://msharov.github.io/ustl/) - The small STL library.
* [Windows Template Library](http://sourceforge.net/projects/wtl/) - A C++ library for developing Windows applications and UI components.
* [Yomm2](https://github.com/jll63/yomm2) - Fast, Orthogonal, Open multi-methods. Supersedes [Yomm11](https://github.com/jll63/yomm11)
* [ZLMediaKit](https://github.com/ZLMediaKit/ZLMediaKit) - WebRTC/RTSP/RTMP/HTTP/HLS/HTTP-FLV/WebSocket-FLV/HTTP-TS/HTTP-fMP4/WebSocket-TS/WebSocket-fMP4/GB28181 server and client framework based on C++11.
* [zpl](https://github.com/zpl-c/zpl) - C99 cross-platform header-only library with many goodies.

## Artificial Intelligence

* [ANNetGPGPU](https://github.com/ANNetGPGPU/ANNetGPGPU) - A GPU (CUDA) based Artificial Neural Network library.
* [btsk](https://github.com/aigamedev/btsk) - Game Behavior Tree Starter Kit.
* [Evolving Objects](http://eodev.sourceforge.net/) - A template-based, ANSI-C++ evolutionary computation library which helps you to write your own stochastic optimization algorithms insanely fast.
* [frugally-deep](https://github.com/Dobiasd/frugally-deep) - Header-only library for using Keras models in C++.
* [Genann](https://github.com/codeplea/genann) - Simple neural network library in C.
* [MXNet](https://github.com/apache/incubator-mxnet) - Lightweight, Portable, Flexible Distributed/Mobile Deep Learning with Dynamic, Mutation-aware Dataflow Dep Scheduler; for Python, R, Julia, Scala, Go, Javascript and more.
* [PyTorch](https://github.com/pytorch/pytorch) - Tensors and Dynamic neural networks in Python with strong GPU acceleration.
* [Recast/Detour](https://github.com/recastnavigation/recastnavigation) - (3D) Navigation mesh generator and pathfinder, mostly for games.
* [TensorFlow](https://github.com/tensorflow/tensorflow) - An open source software library for numerical computation using data flow graphs.
* [Txeo](https://github.com/rdabra/txeo) - A modern C++ wrapper for TensorFlow.
* [oneDNN](https://github.com/oneapi-src/oneDNN) - An open-source cross-platform performance library for deep learning applications.
* [CNTK](https://github.com/Microsoft/CNTK) - Microsoft Cognitive Toolkit (CNTK), an open source deep-learning toolkit.
* [tiny-dnn](https://github.com/tiny-dnn/tiny-dnn) - A header only, dependency-free deep learning framework in C++11.
* [Veles](https://github.com/Samsung/veles) - Distributed platform for rapid Deep learning application development.
* [Kaldi](https://github.com/kaldi-asr/kaldi) - Toolkit for speech recognition.
* [wenet](https://github.com/wenet-e2e/wenet) - Production First and Production Ready End-to-End Speech Recognition Toolkit.

## Asynchronous Event Loop

* [Amplitude Audio SDK](https://github.com/SparkyStudios/AmplitudeAudioSDK) - A cross-platform audio engine designed with the needs of games in mind. [website](https://amplitudeaudiosdk.com)
* [Aubio](https://github.com/aubio/aubio) - A library for audio and music analysis.[website](https://aubio.org/)
* [Asio](https://github.com/chriskohlhoff/asio/) - A cross-platform C++ library for network and low-level I/O programming that provides developers with a consistent asynchronous model using a modern C++ approach.
* [Boost.Asio](https://github.com/boostorg/asio) - A cross-platform C++ library for network and low-level I/O programming.
* [C++ Actor Framework](https://github.com/actor-framework/actor-framework) - An Open Source Implementation of the Actor Model in C++. [Website](http://actor-framework.org/)
* [Dasynq](https://github.com/davmac314/dasynq) - Thread-safe cross-platform event loop library in C++.
* [eventpp](https://github.com/wqking/eventpp) - Event Dispatcher and callback list for C++.
* [hiactor](https://github.com/alibaba/hiactor) - Hiactor is an actor framework for building high-performance, concurrent and scalable event-driven systems using C++.
* [ivykis](https://github.com/buytenh/ivykis) - library for asynchronous I/O readiness notification. It is a thin, portable wrapper around OS-provided mechanisms such as epoll_create, kqueue, poll, poll (/dev/poll) and port_create.
* [libev](http://libev.schmorp.de/) - A full-featured and high-performance event loop that is loosely modelled after libevent, but without its limitations and bugs.
* [libevent](http://libevent.org/) - An event notification library.
* [libevent-cpp](https://github.com/wxggg/libevent-cpp) - libevent library reimplemented with C++.
* [libhv](https://github.com/ithewei/libhv) - Cross-platform event loop library. Like libevent, libev, and libuv, libhv provides event-loop with non-blocking IO and timer, but simpler apis and richer protocols.
* [libunifex](https://github.com/facebookexperimental/libunifex) - The 'libunifex' project is a prototype implementation of the C++ sender/receiver async programming model that is currently being considered for standardisation.
* [libuv](https://github.com/libuv/libuv) - Cross-platform asynchronous I/O.
* [uvw](https://github.com/skypjack/uvw) - C++ wrapper for libuv.
* [uv-cpp](https://github.com/wlgq2/uv-cpp) - a simple interface, high-performance network library based on C++11.

## Audio
*Audio, Sound, Music, Digitized Voice Libraries*

* [AudioFile](https://github.com/adamstark/AudioFile) - A simple C++ library for reading and writing audio files.
* [audioFlux](https://github.com/libAudioFlux/audioFlux) - A C library for audio and music analysis, feature extraction.
* [dr_libs](https://github.com/mackron/dr_libs) - Single file audio decoding libraries for C and C++.
* [FMOD](http://www.fmod.org/) - An easy to use crossplatform audio engine and audio content creation tool for games.
* [KFR](https://www.kfrlib.com/) - Fast, modern C++ DSP framework, FFT, FIR/IIR filters, Sample Rate Conversion.
* [LAME](https://lame.sourceforge.io/using.php) - LAME is a high quality MPEG Audio Layer III (MP3) encoder.
* [libsndfile](https://github.com/erikd/libsndfile/) - C library with C++ wrapper for reading and writing files containing sampled sound through one standard library interface.
* [libsoundio](https://github.com/andrewrk/libsoundio) - C library for cross-platform real-time audio input and output.
* [Maximilian](https://github.com/micknoise/Maximilian) - C++ Audio and Music DSP Library.
* [OpenAL](http://www.openal.org/) - Open Audio Library - A crossplatform audio API.
* [miniaudio](https://github.com/dr-soft/miniaudio) - Single file audio playback and capture library.
* [Opus](http://opus-codec.org/) - A totally open, royalty-free, highly versatile audio codec.
* [PortAudio](http://www.portaudio.com/) - PortAudio is a free, cross-platform, open-source, audio I/O library.
* [rnnoise](https://github.com/xiph/rnnoise) - Recurrent neural network for audio noise reduction.
* [SELA](https://github.com/sahaRatul/sela) - SimplE Lossless Audio.
* [SoLoud](https://github.com/jarikomppa/soloud) - Easy, portable audio engine for games.
* [Speex](http://www.speex.org/) - A free codec for free speech. Obsoleted by Opus.
* [Tonic](https://github.com/TonicAudio/Tonic) - Easy and efficient audio synthesis in C++.
* [Vorbis](http://xiph.org/vorbis/) - Ogg Vorbis is a fully open, non-proprietary, patent-and-royalty-free, general-purpose compressed audio format.
* [minimp3](https://github.com/lieff/minimp3) - Public domain, header-only MP3 decoder with clean-room implementation.
* [Verovio](https://github.com/rism-ch/verovio) - Verovio is a fast and lightweight music notation engraving library.[https://www.verovio.org]
* [Wav2Letter++](https://github.com/facebookresearch/wav2letter/) - Public domain, a fast open source speech processing toolkit written entirely in C++ and uses the ArrayFire tensor library and the flashlight machine learning library for maximum efficiency

## Authentication and OAuth

* [Alternative-2FA-Dongle](https://github.com/mizutanilab/Alternative-2FA-Dongle) - A hardware token that works with the two-factor authentication for GitHub, Google, Facebook, and so on.
* [Bad-TOTP](https://github.com/jameshi16/Bad-TOTP) - [Completed] TOTP implemented with RFC2104, RFC3174, RFC4226, RFC6234, RFC6238.
* [cjose](https://github.com/cisco/cjose) - C library implementing the Javascript Object Signing and Encryption (JOSE).
* [cjwt](https://github.com/xmidt-org/cjwt) - cjwt is a small JWT handler designed to allow consumers of JWTs of the JWS variant the ability to securely and easily get claims and data from a JWT.
* [cpp-jwt](https://github.com/arun11299/cpp-jwt) - JSON Web Token library for C++.
* [jsmn-web-tokens](https://github.com/TomzBench/jsmn-web-tokens) - Small library for creating and decoding [Json Web Tokens](https://jwt.io/)
* [jwt-cpp](https://github.com/Thalhammer/jwt-cpp) - A header only library for creating and validating JSON web tokens in C++.
* [jwtpp](https://github.com/troian/jwtpp) - JSON Object Signing and Encryption library for C++.
* [l8w8jwt](https://github.com/GlitchedPolygons/l8w8jwt) - Minimal, OpenSSL-less and super lightweight JWT library written in C.
* [libcotp](https://github.com/paolostivanin/libcotp) - C library that generates TOTP and HOTP according to [RFC-6238](https://tools.ietf.org/html/rfc6238).
* [libjwt](https://github.com/benmcollins/libjwt) - JWT C Library.
* [OpenAAA](https://github.com/n13l/openaaa) - Distributed, Authentication, Authorization and Accouting Library based on decentralized trust.
* [OTPClient](https://github.com/paolostivanin/OTPClient) - Highly secure and easy to use GTK+ software for two-factor authentication that supports both Time-based One-time Passwords (TOTP) and HMAC-Based One-Time Passwords (HOTP).
* [rhonabwy](https://github.com/babelouest/rhonabwy) - Javascript Object Signing and Encryption (JOSE) library - JWK, JWKS, JWS, JWE and JWT.
* [totp](https://github.com/arachsys/totp) - RFC 6238 code generator compatible with Google Authenticator.

## Biology
*Bioinformatics, Genomics, Biotech*

* [BioC++](http://biocpp.sourceforge.net/) - C++ Computational Libraries for Bioinformatics.
* [Chaste](http://www.cs.ox.ac.uk/chaste/) - An open source C++ library for the computational simulation of mathematical models developed for physiology and biology. [BSD]
* [libsequence](http://molpopgen.github.io/libsequence/) - A C++ library for representing and analyzing population genetics data.
* [SeqAn](http://www.seqan.de/) - Algorithms and data structures for the analysis of sequences with the focus on biological data.
* [Vcflib](https://github.com/ekg/vcflib) - A C++ library for parsing and manipulating VCF files.
* [Wham](https://github.com/zeeev/wham) - Structural variants (SVs) in Genomes by directly applying association tests to BAM files.

## BitTorrent

* [jech/dht](https://github.com/jech/dht) - BitTorrent DHT library in C.
* [libtorrent](https://github.com/arvidn/libtorrent) (a.k.a. libtorrent-rasterbar) - An efficient feature complete C++ bittorrent implementation.
* [LibTorrent](https://github.com/rakshasa/libtorrent) (a.k.a. libtorrent-rakshasa) - BitTorrent library.
* [libutp](https://github.com/bittorrent/libutp) - uTorrent Transport Protocol library.

## Blockchain

*Tools for building blockchains.*

* [ELLI](https://github.com/elliotproject/elli) - ELLI is an opensource cryptocurrency specialized on fast transactions with low transaction fees and very high security and anonymity level. [elliotproject.org](https://elliotproject.org/)
* [mako](https://github.com/chjj/mako) - Full bitcoin implementation in C.
* [okcash](https://github.com/okcashpro/okcash) - OK wallet integration/staging tree • Okcash
* [PluraCoin](https://github.com/pluracoin/PluraCoin) - PLURA is a cryptocurrency focused on crypto adoption by masses through affordable Crypto e-Commerce Payment Solution.
* [wallet-core](https://github.com/trustwallet/wallet-core) - Cross-platform, cross-blockchain wallet library.

## Chemistry
*Chemistry, Geochemistry, Biochemistry*

* [d-SEAMS](https://github.com/d-SEAMS/seams-core) - A molecular dynamics trajectory analysis engine in C++ and Lua with Nix. It is an acronym for Deferred Structural Elucidation Analysis for Molecular Simulations.
* [gromacs](https://github.com/gromacs/gromacs) - A message-passing parallel molecular dynamics implementation.
* [LAMMPS](https://github.com/lammps/lammps) - A classical molecular dynamics code with a focus on materials modeling. It's an acronym for Large-scale Atomic/Molecular Massively Parallel Simulator.
* [MADNESS](https://github.com/m-a-d-n-e-s-s/madness) - Multiresolution Adaptive Numerical Environment for Scientific Simulation. [website](https://github.com/m-a-d-n-e-s-s/madness)
* [MPQC](https://github.com/ValeevGroup/mpqc) - The Massively Parallel Quantum Chemistry program, MPQC, computes properties of atoms and molecules from first principles using the time independent Schrödinger equation. [website](https://mpqc.org/)
* [ORCA](https://en.wikipedia.org/wiki/ORCA_(quantum_chemistry_program)) - An ab initio quantum chemistry program package that contains modern electronic structure methods. [website](https://orcaforum.kofo.mpg.de/)
* [Psi](https://github.com/psi4/psi4) - An ab initio computational chemistry package.
* [Reaktoro](https://github.com/reaktoro/reaktoro) - A computational framework in C++ and Python for modeling chemically reactive systems.

## CLI
*Console/Terminal User Interface, Command Line Interface*

 * [Argh!](https://github.com/adishavit/argh) - A minimalist, frustration-free, header-only argument handler.
 * [argparse](https://github.com/cofyc/argparse) - Command-line argument parsing library, inspired by Python's argparse module.
 * [argparse](https://github.com/morrisfranken/argparse) - A Simple Argument Parser for C++.
 * [ArgParse](https://github.com/p-ranav/argparse) - Argument Parser for Modern C++.
 * [ArgumentParser](https://github.com/hbristow/argparse) - A slimline C++ class for parsing command-line arguments, with an interface similar to python's class of the same name.
 * [barkeep](https://github.com/oir/barkeep) - Small C++ header to display async animations, counters, and progress bars. [website](https://oir.github.io/barkeep/)
 * [Boost.Program_options](https://github.com/boostorg/program_options) - A library to obtain program options via conventional methods such as command line and config file.
 * [Clara](https://github.com/catchorg/Clara) - A simple to use, composable, command line parser for C++ 11 and beyond.
 * [cli](https://github.com/daniele77/cli) - A cross-platform header only C++14 library for interactive command line interfaces (Cisco style).
 * [CLI11](https://github.com/CLIUtils/CLI11) - Header only single or multi-file C++11 library for simple and advanced CLI parsing.
 * [clipp](https://github.com/muellan/clipp) - Easy to use, powerful and expressive command line argument handling for C++11/14/17 contained in a single header file.
 * [cpp-terminal](https://github.com/jupyter-xeus/cpp-terminal) - Small header only C++ library for writing multiplatform terminal applications.
 * [Ctrl+C](https://github.com/evgenykislov/ctrl-c) - Crossplatform C++11 library to handle Ctrl+C event in custom functions.
 * [cxxopts](https://github.com/jarro2783/cxxopts) - Lightweight C++ command line option parser.
 * [docopt.cpp](https://github.com/docopt/docopt.cpp) - A library to generate option parser from docstring.
 * [FINAL CUT](https://github.com/gansm/finalcut) - Library for creating terminal applications with text-based widgets.
 * [FTXUI](https://github.com/ArthurSonzogni/FTXUI) - C++ Functional Terminal User Interface.
 * [gflags](https://gflags.github.io/gflags/) - Commandline flags module for C++.
 * [imtui](https://github.com/ggerganov/imtui) - Immediate Mode Text-based User Interface.
 * [indicators](https://github.com/p-ranav/indicators/) - Activity indicators for Modern C++.
 * [linenoise](https://github.com/antirez/linenoise) - A small self-contained alternative to readline and libedit.
 * [linenoise-ng](https://github.com/arangodb/linenoise-ng) - A small, portable GNU readline replacement for Linux, Windows and MacOS which is capable of handling UTF-8 characters.
 * [nemu](https://github.com/nemuTUI/nemu) - Ncurses UI for QEMU.
 * [Ncurses](http://invisible-island.net/ncurses/) - A terminal user interface.
 * [notcurses](https://github.com/dankamongmen/notcurses) - Blingful character graphics/TUI library. definitely not curses.
 * [oof](https://github.com/s9w/oof) - Convenient, high-performance RGB color and position control for console output.
 * [PDCurses](https://github.com/wmcbrine/PDCurses) - Public domain curses library with both source code and pre-compiled library available.
 * [replxx](https://github.com/AmokHuginnsson/replxx) - A readline and libedit replacement that supports UTF-8, syntax highlighting, hints, works on Unix and Windows.
 * [Rogueutil](https://github.com/sakhmatd/rogueutil) - Rogueutil is a cross-platform C/C++ (Windows and *nix at least) utility for creating text-based user interfaces (TUI) and console-based games.
 * [structopt](https://github.com/p-ranav/structopt) - Parse command line arguments by defining a struct.
 * [structopt](https://github.com/v1nh1shungry/structopt) - A lovely structopt library for C++! Parse command line arguments by defining a struct.
 * [tabulate](https://github.com/p-ranav/tabulate) - Table Maker for Modern C++.
 * [Taywee](https://github.com/taywee/args) - A simple header-only C++ argument parser library.
 * [TCLAP](http://tclap.sourceforge.net) - A mature, stable and feature-rich library for defining and accessing command line arguments in ANSI C++.
 * [termbox](https://github.com/nsf/termbox) - A C library for writing text-based user interfaces.
 * [termbox2](https://github.com/termbox/termbox2) - termbox2 is a terminal rendering library for creating TUIs.
 * [TermOx](https://github.com/a-n-t-h-o-n-y/TermOx) - C++17 Terminal User Interface(TUI) Library.
 * [tuibox](https://github.com/Cubified/tuibox) - A single-header terminal UI (TUI) library, capable of creating mouse-driven, interactive applications on the command line. 
 * [Yet Another Dialog](https://github.com/v1cont/yad) - Program allows you to display GTK+ dialog boxes from command line or shell scripts. YAD depends on GTK+ only.

## Computer vision

* [OpenCV](https://github.com/Itseez/opencv) - Open Source Computer Vision Library.
* [Tesseract](https://github.com/tesseract-ocr/tesseract) - Tesseract Open Source OCR Engine [tesseract-ocr.github.io](https://tesseract-ocr.github.io/)

## Compression
*Compression and Archiving Libraries*

* [bit7z](https://github.com/rikyoz/bit7z) - A C++ static library offering a clean and simple interface to the 7-zip DLLs.
* [Brotli](https://github.com/google/brotli) - Brotli compression format. Developed by Google.
* [bzip2](http://www.bzip.org/) - A freely available, patent free, high-quality data compressor.
* [bzip3](https://github.com/kspalaiologos/bzip3) - A better and stronger spiritual successor to BZip2.
* [clzip](http://lzip.nongnu.org/clzip.html) - C version of the high-quality data compressor [Lzip](http://lzip.nongnu.org/lzip.html) (LZMA implementation).
* [FiniteStateEntropy](https://github.com/Cyan4973/FiniteStateEntropy) - New generation entropy codecs : Finite State Entropy and Huff0.
* [heatshrink](https://github.com/atomicobject/heatshrink) - data compression library for embedded/real-time systems.
* [Kanzi](https://github.com/flanglet/kanzi-cpp) - a modern, modular, portable and efficient lossless data compressor implemented in C++.
* [KArchive](https://quickgit.kde.org/?p=karchive.git) - A library for creating, reading, writing and manipulating file archives like zip and tar. It also provides transparent compression and decompression of data, using formats like gzip, via a subclass of QIODevice.
* [libarchive](https://github.com/libarchive/libarchive) - Multi-format archive and compression library. [website](http://www.libarchive.org/)
* [LZ4](https://github.com/lz4/lz4) - Extremely Fast Compression algorithm.
* [LZFSE](https://github.com/lzfse/lzfse) - LZFSE compression library and command line tool. Developed by Apple.
* [LZHAM](https://code.google.com/p/lzham/) - Lossless data compression library with a compression ratio similar to LZMA but with much faster decompression.
* [LZMA](http://www.7-zip.org/sdk.html) :zap: - The default and general compression method of 7z format.
* [LZMAT](http://www.matcode.com/lzmat.htm) - An extremely fast real-time lossless data compression library.
* [miniz](https://github.com/richgel999/miniz) - Single C source file Deflate/Inflate compression library with zlib-compatible API, ZIP archive reading/writing, PNG writing.
* [Minizip](https://github.com/nmoinvaz/minizip) - Zlib with latest bug fixes that supports PKWARE disk spanning, AES encryption, and IO buffering.
* [minizip-ng](https://github.com/zlib-ng/minizip-ng) - Fork of the popular zip manipulation library found in the zlib distribution.
* [OpenPose](https://github.com/CMU-Perceptual-Computing-Lab/openpose) - Real-time multi-person keypoint detection library for body, face, hands, and foot estimation.
* [PhysicsFS](https://icculus.org/physfs/) - A library to provide abstract access to various archives. It is intended for use in video games, and the design was somewhat inspired by Quake 3's file subsystem.
* [Rapidgzip](https://github.com/mxmlnkn/rapidgzip) - Gzip Decompression and Random Access for Modern Multi-Core Machines.
* [smaz](https://github.com/antirez/smaz) - Small strings compression library.
* [Snappy](https://google.github.io/snappy/) - A fast compressor/decompressor.
* [ZLib](http://zlib.net/) - A very compact compression library for data streams.
* [zlib-ng](https://github.com/zlib-ng/zlib-ng) - zlib for the "next generation" systems. Drop-In replacement with some serious optimizations.
* [zstd](https://github.com/facebook/zstd) - Zstandard - Fast real-time compression algorithm. Developed by Facebook.
* [ZZIPlib](http://zziplib.sourceforge.net/) - Provides read access on ZIP-archives.

## Concurrency
*Concurrency and Multithreading*

* [alpaka](https://github.com/ComputationalRadiationPhysics/alpaka) - Abstraction library for parallel kernel acceleration.
* [ArrayFire](https://github.com/arrayfire/arrayfire) - A general purpose GPU library.
* [Async++](https://github.com/Amanieu/asyncplusplus) - A lightweight concurrency framework for C++11, inspired by the Microsoft PPL library and the N3428 C++ standard proposal.
* [async2](https://github.com/Wirtos/async2.h) - Taking inspiration from protothreads, async.h, coroutines.h and async/await as found in python this is an async/await/fawait/event loop implementation for C based on Duff's device. asynchronous, stackful subroutines.
* [atomic_queue](https://github.com/max0x7ba/atomic_queue) - C++ lockless queue.
* [Boost.Compute](https://github.com/boostorg/compute) - A C++ GPU Computing Library for OpenCL.
* [Boost.coroutine2](https://github.com/boostorg/coroutine2) - boost.coroutine2 provides templates for generalized subroutines which allow multiple entry points for suspending and resuming execution at certain locations.
* [Bolt](https://github.com/HSA-Libraries/Bolt) - A C++ template library optimized for GPUs.
* [Channel](https://github.com/andreiavrammsd/cpp-channel) - Thread-safe container for sharing data between threads.
* [ck](https://github.com/concurrencykit/ck) - Concurrency primitives, safe memory reclamation mechanisms and non-blocking data structures.
* [cchan](http://repo.hu/projects/cchan/) - Small library for channel constructs for inter-thread communication.
* [checkedthreads](https://github.com/yosefk/checkedthreads) - A simple library for parallelism, with built-in checking for race conditions.
* [ck](http://concurrencykit.org) - Concurrency primitives, safe memory reclamation mechanisms and non-blocking data structures.
* [concurrencpp](https://github.com/David-Haim/concurrencpp) - A general concurrency library containing tasks, executors, timers and C++20 coroutines to rule them all.
* [concurrentqueue](https://github.com/cameron314/concurrentqueue) - A fast multi-producer, multi-consumer lock-free concurrent queue for C++11.
* [coro](https://github.com/Felspar/coro) - Coroutine library and toolkit for C++20.
* [coroutine](https://github.com/cloudwu/coroutine) - It's an asymmetric coroutine library (like lua).
* [coroutine](https://github.com/luncliff/coroutine) - C++ 20 Coroutines in Action (Helpers + Test Code Examples)
* [Cpp-Taskflow](https://github.com/taskflow/taskflow) - Fast C++ Parallel Programming with Task Dependencies.
* [cppcoro](https://github.com/lewissbaker/cppcoro) - The 'cppcoro' library provides a large set of general-purpose primitives for making use of the coroutines TS proposal described in [N4680](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/n4680.pdf).
* [C-Thread-Pool](https://github.com/Pithikos/C-Thread-Pool) - A minimal but powerful thread pool in ANSI C.
* [cr_task.h](https://github.com/rkaehn/cr_task.h) - Header-only library for asynchronous tasks in C.
* [CspChan](https://github.com/rochus-keller/CspChan) - A pure C (-std=c89) implementation of Go channels, including blocking and non-blocking selects.
* [CTPL](https://github.com/vit-vit/CTPL) - Modern and efficient C++ Thread Pool Library.
* [CUB](https://github.com/NVlabs/cub) - CUB provides state-of-the-art, reusable software components for every layer of the CUDA programming mode.
* [cupla](https://github.com/ComputationalRadiationPhysics/cupla) - C++ API to run CUDA/C++ on OpenMP, Threads, TBB, ... through Alpaka.
* [C++React](https://github.com/schlangster/cpp.react) - A reactive programming library for C++11.
* [dpool](https://github.com/senlinzhan/dpool) - Dynamic thread pool implemented using C++ 11.
* [executors](https://github.com/chriskohlhoff/executors) - C++14 library for executors.
* [uThreads](https://github.com/samanbarghi/uThreads) - A concurrent user-level thread library implemented in C++.
* [FiberTaskingLib](https://github.com/RichieSams/FiberTaskingLib) - Task-based multi-threading library that supports task graphs with arbitrary dependencies.
* [HPX](https://github.com/STEllAR-GROUP/hpx/) - A general purpose C++ runtime system for parallel and distributed applications of any scale.
* [Intel Games Task Scheduler](https://github.com/GameTechDev/GTS-GamesTaskScheduler) - A task scheduling framework designed for the needs of game developers.
* [Intel Parallel STL](https://github.com/intel/parallelstl) - Intel® implementation of C++17 STL for C++11 and above.
* [Intel TBB](https://www.threadingbuildingblocks.org/) - Intel® Threading Building Blocks.
* [junction](https://github.com/preshing/junction) - A library of concurrent data structures in C++.
* [Kokkos](https://github.com/kokkos/kokkos) - A performance portable programming model for parallel execution and memory abstraction.
* [libaco](https://github.com/hnes/libaco) - A blazing fast and lightweight C asymmetric coroutine library.
* [libcds](https://github.com/khizmax/libcds) - A C++ library of Concurrent Data Structures.
* [Libclsph](https://github.com/libclsph/libclsph) - An OpenCL based GPU accelerated SPH fluid simulation library.
* [Libco](https://github.com/Tencent/libco) - Libco is a c/c++ coroutine library that is widely used in WeChat services. It has been running on tens of thousands of machines since 2013.
* [libcopp](https://github.com/owent/libcopp) - cross-platform coroutine library in C++.
* [libcoro](https://github.com/jbaldwin/libcoro) - C++20 coroutine library.
* [libconcurrent](https://github.com/sharow/libconcurrent) - Concurrent programming library, using coroutines, for C11.
* [libcsp](https://github.com/shiyanhui/libcsp) - A concurrency C library 10x faster than Golang.
* [libdill](https://github.com/sustrik/libdill) - Introduces structured concurrency in C.
* [libdispatch](https://github.com/apple/swift-corelibs-libdispatch) - Grand Central Dispatch (GCD), developed by Apple Inc., is a task parallelism technology based on the thread pool pattern. libdispatch is a library that provides the implementation of GCD's services.
* [libfiber](https://github.com/iqiyi/libfiber) - The high performance coroutine library for Linux/FreeBSD/MacOS/Windows, supporting select/poll/epoll/kqueue/iocp/windows GUI.
* [libfibre](https://git.uwaterloo.ca/mkarsten/libfibre) - libfibre is a user-threading runtime.
* [libfork](https://github.com/ConorWilliams/libfork) - A bleeding-edge, lock-free, wait-free, continuation-stealing tasking library built on C++20's coroutines. [website](https://conorwilliams.github.io/libfork/)
* [libgo](https://github.com/yyzybb537/libgo) - Go-style concurrency in C++11.
* [libhl](https://github.com/xant/libhl) - Library implementing a thread-safe API to manage a range of data structures. Also provides some supporting functions and structures for concurrent and lockfree programming.
* [liburcu](http://liburcu.org/) - Data synchronization library, which scales linearly with the number of cores.
* [libmill](https://github.com/sustrik/libmill/) - Introduces Go-style concurrency in C.
* [librf](https://github.com/tearshark/librf) - A stack-free coroutine library written based on C++ Coroutines.
* [lock-free-object-pool](https://github.com/yuanrongxi/lock-free-object-pool) - A lock-free object pool, CAS/spin/pthread_mutex.
* [lthread](https://github.com/halayli/lthread) - lthread, a multicore enabled coroutine library written in C.
* [marl](https://github.com/google/marl) - Marl is a hybrid thread / fiber task scheduler written in C++ 11.
* [moderngpu](https://github.com/moderngpu/moderngpu) - moderngpu is a productivity library for general-purpose computing on GPUs. It is a header-only C++ library written for CUDA. The unique value of the library is in its accelerated primitives for solving irregularly parallel problems.
* [NCCL](https://github.com/NVIDIA/nccl) - Optimized primitives for collective multi-GPU communication.
* [Neco](https://github.com/tidwall/neco) - Neco is a C library that provides concurrency using coroutines. It's small & fast, and intended to make concurrent I/O & network programming easy.
* [OpenCL](https://www.khronos.org/opencl/) - The open standard for parallel programming of heterogeneous systems.
* [OpenMP](http://openmp.org/) - The OpenMP API.
* [promise-cpp](https://github.com/xhawk18/promise-cpp) - C++ promise/A+ library in Javascript style.
* [PhotonLibOS](https://github.com/alibaba/PhotonLibOS) - Probably the fastest coroutine lib in the world! [website](https://photonlibos.github.io/)
* [pth](https://gnu.org/software/pth/) - Portable implementation for non-preemptive priority-based scheduling for multiple threads of execution.
* [pthreads](https://en.wikipedia.org/wiki/POSIX_Threads) - The POSIX thread library.
* [pthreadpool](https://github.com/Maratyszcza/pthreadpool) - A portable and efficient thread pool implementation.
* [psched](https://github.com/p-ranav/psched) - Priority-based Task Scheduling for Modern C++.
* [RaftLib](http://raftlib.io/) - The RaftLib C++ library, streaming/dataflow concurrency via C++ iostream-like operators.
* [readerwriterqueue](https://github.com/cameron314/readerwriterqueue) - A fast single-producer, single-consumer lock-free queue for C++.
* [rotor](https://github.com/basiliscos/cpp-rotor) -  Event loop friendly C++ actor micro framework.
* [s_task](https://github.com/xhawk18/s_task) - awaitable coroutine library for C.
* [STAPL](http://parasol-lab.gitlab.io/stapl-home/) - A C++ parallel programming framework designed to work on both shared and distributed memory parallel computers.
* [state-threads](https://github.com/ossrs/state-threads) - Light-weight thread library for C/C++ coroutine (similar to goroutine), for high performance network servers.
* [TinyCThread](https://tinycthread.github.io/) - Portable, small implementation of the C11 threads API.
* [stack](https://github.com/tidwall/stack) - C library for managing stacks for coroutines and threads.
* [stdgpu](https://github.com/stotko/stdgpu) - Efficient STL-like Data Structures on the GPU.
* [taskpool](https://github.com/fawdlstty/taskpool) - Modern C++ taskpool, implement an effect exactly equivalent to async/await as a purely synchronous call
* [thread-pool](https://github.com/mtrebi/thread-pool) - Thread pool implementation using c++11 threads.
* [thread-pool](https://github.com/DeveloperPaul123/thread-pool) - A modern thread pool implementation based on C++20.
* [thread-pool](https://github.com/bshoshany/thread-pool) - BS::thread_pool: a fast, lightweight, and easy-to-use C++17 thread pool library.
* [Threadpool](https://github.com/ConorWilliams/Threadpool) - Light, fast, threadpool for C++20.
* [ThreadPool](https://github.com/progschj/ThreadPool) - A simple C++11 Thread Pool implementation.
* [threadpool](https://github.com/lzpong/threadpool) - based on C++11 , a mini threadpool , accept variable number of parameters.
* [Thrust](http://thrust.github.io/) - A parallel algorithms library which resembles the C++ Standard Template Library (STL).
* [transwarp](https://github.com/bloomen/transwarp) - A header-only C++ library for task concurrency.
* [HPX](https://github.com/STEllAR-GROUP/hpx/) - A general purpose C++ runtime system for parallel and distributed applications of any scale.
* [SObjectizer](https://github.com/Stiffstream/sobjectizer) - An implementation of Actor, Publish-Subscribe, and CSP models in one rather small C++ framework. With performance, quality, and stability proved by years in the production.
* [Quantum](https://github.com/bloomberg/quantum) - A powerful C++ coroutine dispatcher framework built on top of [boost::coroutines2](https://www.boost.org/doc/libs/1_65_0/libs/coroutine2/doc/html/index.html).
* [VexCL](https://github.com/ddemidov/vexcl) - A C++ vector expression template library for OpenCL/CUDA.
* [YACLib](https://github.com/YACLib/YACLib) - Yet Another Concurrency Library.

## Configuration
*Configuration files, INI files*

* [config](https://github.com/taocpp/config) - C++ header-only config reader library based on JSON.
* [cpptoml](https://github.com/skystrife/cpptoml) - A header-only library for parsing TOML configuration files.
* [figcone](https://github.com/kamchatka-volcano/figcone) - Read JSON, YAML, TOML, XML or INI configuration by declaring a struct.
* [ini-cpp](https://github.com/SSARCandy/ini-cpp) - Single header only C++ version, with some handy read/write interface, extend from [inih](https://github.com/benhoyt/inih). [website](https://ssarcandy.tw/ini-cpp/index.html)
* [inicpp](https://github.com/SemaiCZE/inicpp) - C++ parser of INI files with schema validation.
* [inifile-cpp](https://github.com/Rookfighter/inifile-cpp) - A header-only and easy to use Ini file parser for C++.
* [inih](https://github.com/benhoyt/inih) - Simple .INI file parser in C, good for embedded systems.
* [iniparser](https://github.com/ndevilla/iniparser) - INI file parser.
* [inipp](https://github.com/mcmtroffaes/inipp) - Simple header-only C++ ini parser and generator.
* [libconfig](https://github.com/hyperrealm/libconfig) - C, C++ library for processing structured configuration files. [website](https://hyperrealm.github.io/libconfig/)
* [libconfuse](https://github.com/martinh/libconfuse) - Small configuration file parser library for C.
* [mINI](https://github.com/metayeti/mINI) - INI file reader and writer.
* [simpleini](https://github.com/brofield/simpleini) - Cross-platform C++ library providing a simple API to read and write INI-style configuration files.
* [tinytoml](https://github.com/mayah/tinytoml) - A header only C++11 library for parsing TOML.
* [toml++](https://github.com/marzer/tomlplusplus) - Header-only TOML parser and serializer for C++17 and later.
* [toml11](https://github.com/ToruNiina/toml11) -  a C++11 (or later) header-only toml parser/encoder depending only on C++ standard library.

## Containers

* [btree.c](https://github.com/tidwall/btree.c) - A B-tree implementation in C.
* [C++ B-tree](https://code.google.com/p/cpp-btree/) - A template library that implements ordered in-memory containers based on a B-tree data structure.
* [C++ Fixed Containers](https://github.com/teslamotors/fixed-containers) - Header-only C++20 library that provides fixed capacity constexpr containers.
* [dynamic_bitset](https://github.com/pinam45/dynamic_bitset) - Simple Useful Libraries: C++17/20 header-only dynamic bitset. [website](https://pinam45.github.io/dynamic_bitset/)
* [emhash](https://github.com/ktprime/emhash) - Fast and memory efficient c++ flat hash map/set.
* [Forest](https://github.com/xorz57/forest) - Template library implementing an AVL, a Binary Search, a KD and a Quad Tree.
* [hashmap.c](https://github.com/tidwall/hashmap.c) - Hash map implementation in C.
* [Hashmaps](https://github.com/goossaert/hashmap) - Implementation of open addressing hash table algorithms in C++.
* [hat-trie](https://github.com/Tessil/hat-trie) - C++ implementation of a fast and memory efficient HAT-trie.
* [Hopscotch map](https://github.com/Tessil/hopscotch-map) - A fast header-only hash map which uses hopscotch hashing for collisions resolution.
* [flat_hash_map](https://github.com/skarupke/flat_hash_map) - A very fast flat hashtable with Fibonacci hashing.
* [frozen](https://github.com/serge-sans-paille/frozen) - a header-only, constexpr alternative to gperf for C++14 users.
* [libcuckoo](https://github.com/efficient/libcuckoo) - A high-performance, concurrent hash table.
* [LSHBOX](https://github.com/RSIA-LIESMARS-WHU/LSHBOX) - A c++ toolbox of locality-sensitive hashing (LSH), provides several popular LSH algorithms, also support Python and MATLAB.
* [MPSCQ](https://github.com/samanbarghi/MPSCQ) - This is an intrusive lock-free multiple-producer, single-consumer queue inspired by Dmitry Vyukov's implementation.
* [PGM-index](https://github.com/gvinciguerra/PGM-index) - A data structure that enables fast lookup, predecessor, range searches and updates in arrays of billions of items using orders of magnitude less space than traditional indexes.
* [plf::colony](https://github.com/mattreecebentley/plf_colony) - An unordered "bag"-type container which outperforms std containers in high-modification scenarios while maintaining permanent pointers to non-erased elements regardless of insertion/erasure. [website](http://www.plflib.org/colony.htm)
* [plf::list](https://github.com/mattreecebentley/plf_list) - A std::list implementation which removes range splicing in order to enable cache-friendlier structure, yielding significant performance gains.
* [plf::stack](https://github.com/mattreecebentley/plf_stack) - A replacement container for the std::stack container adaptor, with better performance than any std container in a stack context.
* [ring_span lite](https://github.com/martinmoene/ring-span-lite) - A simplified implementation of Arthur O'Dwyer's ring_span implementation ie. a circular buffer view.
* [robin-hood-hashing](https://github.com/martinus/robin-hood-hashing) - Fast & memory efficient hashtable based on robin hood hashing for C++14.
* [robin-map](https://github.com/Tessil/robin-map) - Fast hash map and hash set using robin hood hashing.
* [rtree.c](https://github.com/tidwall/rtree.c) - An R-tree implementation in C.
* [span-lite](https://github.com/martinmoene/span-lite) - A C++20-like span for C++98, C++11 and later in a single-file header-only library.
* [sparsepp](https://github.com/greg7mdp/sparsepp) - A fast, memory efficient hash map for C++.
* [svector](https://github.com/martinus/svector) - Compact SVO optimized vector for C++17 or higher.
* [st_tree](https://github.com/erikerlandson/st_tree) - A fast and flexible c++ template class for tree data structures.
* [Thread-safe LRU Cache](https://github.com/tstarling/thread-safe-lru) - A C++ template class providing a thread-safe LRU cache.
* [tree.hh](https://github.com/kpeeters/tree.hh) - An STL-like C++ header-only tree library.
* [unordered_dense](https://github.com/martinus/unordered_dense) - A fast & densely stored hashmap and hashset based on robin-hood backward shift deletion.

## Cryptography
*Cryptography and Encryption Libraries*

* [AES](https://github.com/SergeyBel/AES) - C++ AES implementation.
* [Argon2](https://github.com/P-H-C/phc-winner-argon2) - The password hash Argon2, winner of PHC.
* [Argon2_Calculator](https://github.com/Antidote1911/Argon2_Calculator) - A simple GUI in C++/Qt for test and bench Argon2.
* [aws-cloudhsm-pkcs11-examples](https://github.com/aws-samples/aws-cloudhsm-pkcs11-examples) - Sample applications demonstrating how to use the CloudHSM PKCS#11 library.
* [Bcrypt](http://bcrypt.sourceforge.net/) - A cross platform file encryption utility. Encrypted files are portable across all supported operating systems and processors.
* [BeeCrypt](http://beecrypt.sourceforge.net/) - A portable and fast cryptography library.
* [BerEditor](https://github.com/jykim74/BerEditor) - Qt Gui tool is capable of editing and viewing for BER and DER data. BerEditor is ASN.1 viewer.
* [bfish](https://github.com/cjwagenius/bfish) - A single include C99 Blowfish ECB crypto library.
* [Botan](http://botan.randombit.net/) - A crypto library for C++.
* [CertMan](https://github.com/jykim74/CertMan) - Qt Gui tool is for making X.509 Cert, CRL, CSR and KeyPair(RSA and ECDSA)
* [cifra](https://github.com/ctz/cifra) - Cifra is a collection of cryptographic primitives targeted at embedded use.
* [compact25519](https://github.com/DavyLandman/compact25519) - A compact portable X25519 + Ed25519 implementation.
* [Crypto++](https://github.com/weidai11/cryptopp) - A free C++ class library of cryptographic schemes.
* [crypto3](https://github.com/NilFoundation/crypto3) - Modern Cryptography Suite in C++17.
* [CryptokiMan](https://github.com/jykim74/CryptokiMan) - Qt Gui tool is for testing and managing PKCS#11 library.
* [digestpp](https://github.com/kerukuro/digestpp) - C++11 header-only message digest (hash) library.
* [gost-engine](https://github.com/gost-engine/engine) - A reference implementation of the Russian GOST crypto algorithms for OpenSSL.
* [engine_pkcs11](https://github.com/OpenSC/engine_pkcs11) - OpenSSL engine for PKCS#11 modules.
* [GnuPG](https://www.gnupg.org/) - A complete and free implementation of the OpenPGP standard.
* [GnuTLS](http://www.gnutls.org/) - A secure communications library implementing the SSL, TLS and DTLS protocols.
* [eHSM](https://github.com/intel/ehsm) - An End-to-End Distributed and Scalable Cloud KMS (Key Management System) built on top of Intel SGX enclave-based HSM (Hardware Security Module), aka eHSM.
* [hash-library](https://github.com/stbrumme/hash-library) - Portable C++ hashing library.
* [HighwayHash](https://github.com/google/highwayhash) - Fast strong hash functions: SipHash/HighwayHash.
* [HEhub](https://github.com/primihub/HEhub) - A library for homomorphic encryption and its applications.
* [KOMIHASH](https://github.com/avaneev/komihash) - Very fast, high-quality hash function, discrete-incremental and streamed hashing-capable.
* [libgcrypt](http://www.gnu.org/software/libgcrypt/) - A general purpose cryptographic library originally based on code from GnuPG.
* [libhsm](https://github.com/bentonstark/libhsm) - simplified C-API for HSM PKCS#11 API.
* [libkmip](https://github.com/OpenKMIP/libkmip) - libkmip is an ISO C11 implementation of the Key Management Interoperability Protocol (KMIP).
* [liblithium](https://github.com/teslamotors/liblithium) - liblithium is a lightweight cryptography library that is portable by design.
* [libpki](https://github.com/openca/libpki) - Easy-to-use high-level library for PKI-enabled applications.
* [LibreSSL](http://www.libressl.org/) - A free version of the SSL/TLS protocol forked from OpenSSL in 2014.
* [libsodium](https://github.com/jedisct1/libsodium) - P(ortable|ackageable) NaCl-based crypto library, opinionated and easy to use.
* [libhydrogen](https://github.com/jedisct1/libhydrogen) - A lightweight, secure, easy-to-use crypto library suitable for constrained environments.
* [libp11](https://github.com/OpenSC/libp11) - PKCS#11 wrapper library.
* [libsecp256k1](https://github.com/bitcoin-core/secp256k1) - Optimized C library for ECDSA signatures and secret/public key operations on curve secp256k1.
* [libtomcrypt](https://github.com/libtom/libtomcrypt) - A fairly comprehensive, modular and portable cryptographic toolkit.
* [mbedTLS](https://github.com/ARMmbed/mbedtls) - An open source, portable, easy to use, readable and flexible SSL library, previously known as PolarSSL.
* [Nettle](http://www.lysator.liu.se/~nisse/nettle/) - A low-level cryptographic library.
* [openCryptoki](https://github.com/opencryptoki/opencryptoki) - PKCS#11 library and tools for Linux. Includes tokens supporting TPM and IBM crypto hardware as well as a software token.
* [OpenSC](https://github.com/OpenSC/OpenSC) - Open source smart card tools and middleware. PKCS#11/MiniDriver/Tokend.
* [OpenSSL](https://github.com/openssl/openssl) - A robust, commercial-grade, full-featured, and Open Source cryptography library.
	* [openssl-cmake](https://github.com/viaduck/openssl-cmake) - CMake script supplying OpenSSL libraries conveniently, encapsulating the OpenSSL build system on various platforms.
	* [openssl-example](https://github.com/yedf2/openssl-example) - Asynchronous examples for openssl using poll/epoll.
* [openssl-pkcs11-samples](https://github.com/tkil/openssl-pkcs11-samples) - Sample code for working with OpenSSL, LibP11, engine_pkcs11, and OpenSC.
* [Raspberry Pico HSM](https://github.com/polhenarejos/pico-hsm) - Hardware Security Module for Raspberry Pico.
* [p11-kit](https://github.com/p11-glue/p11-kit) - Provides a way to load and enumerate PKCS#11 modules.
* [p11perftest](https://github.com/Mastercard/p11perftest) - Utility to benchmark speed of operations of a PKCS#11 implementation.
* [pam_p11](https://github.com/OpenSC/pam_p11) - Authentication with PKCS#11 modules.
* [picotls](https://github.com/h2o/picotls) - Picotls is a TLS 1.3 (RFC 8446) protocol stack written in C.
* [PKCS11-SPECS](https://github.com/Pkcs11Interop/PKCS11-SPECS) - All versions of PKCS#11 specification in one place.
* [pkcs11js](https://github.com/PeculiarVentures/pkcs11js) - A Node.js implementation of the PKCS#11 2.3 interface. (Keywords: Javascript, PKCS#11, Crypto, Smart Card, HSM).
* [pkcs11-logger](https://github.com/Pkcs11Interop/pkcs11-logger) - PKCS#11 logging proxy module.
* [pkcs11-helper](https://github.com/OpenSC/pkcs11-helper) - Library that simplifies the interaction with PKCS#11 providers for end-user applications using a simple API and optional OpenSSL engine.
* [pkcs11-provider](https://github.com/latchset/pkcs11-provider) - pkcs#11 provider for OpenSSL 3.0+
* [pkcs11-tools](https://github.com/Mastercard/pkcs11-tools) - A set of tools to manage objects on PKCS#11 cryptographic tokens. Compatible with many PKCS#11 library, including major HSM brands, NSS and softoken.
* [pkcs11test](https://github.com/google/pkcs11test) - PKCS#11 Test Suite.
* [plusaes](https://github.com/kkAyataka/plusaes) - Header only C++ AES cipher library.
* [portable8439](https://github.com/DavyLandman/portable8439) - Portable 8439: ChaCha20-Poly1305 (RFC 8439) in portable & fast C99.
* [pvpkcs11](https://github.com/PeculiarVentures/pvpkcs11) - pvpkcs11 consists of a input validation library and a set of PKCS#11 implementations that wrap operating system and browser cryptographic implementations.
* [QCA](https://github.com/KDE/qca) - Qt Cryptographic Architecture — straightforward cross-platform crypto API.
* [retter](https://github.com/MaciejCzyzewski/retter) - A collection of hash functions, ciphers, tools, libraries, and materials related to cryptography.
* [rain](https://github.com/DOSAYGO-Research/rain) - The fastest 128-bit and 256-bit non-crypto hash, passes all tests, and under 140 source lines of code.
* [RHash](https://github.com/rhash/RHash) - RHash (Recursive Hasher) is a console utility for calculation and verification of magnet links and various message digests.
* [s2n](https://github.com/awslabs/s2n) - C99 implementation of the TLS/SSL protocols, designed to be simple, fast and with security as a priority.
* [seal](https://github.com/microsoft/seal) - Microsoft SEAL is an easy-to-use and powerful homomorphic encryption library.
* [Secure Memory Library](https://github.com/viaduck/secure_memory) - Provides general purpose data structures and utilities used by multiple ViaDuck projects. Implemented with a focus on memory security and safety.
* [sha1collisiondetection](https://github.com/cr-marcstevens/sha1collisiondetection) - Library and command line tool to detect SHA-1 collision in a file.
* [smhasher](https://github.com/rurban/smhasher) - Hash function quality and speed tests [rurban.github.io/smhasher](https://rurban.github.io/smhasher/).
* [t1ha](https://github.com/erthink/t1ha) - Fast Positive Hash, it is a portable non-cryptographic 64-bit hash function.
* [Tink](https://github.com/google/tink) - A multi-language, cross-platform library that provides cryptographic APIs that are secure, easy to use correctly, and hard(er) to misuse.
* [Tiny AES in C](https://github.com/kokke/tiny-AES-c) - Small portable AES128/192/256 in C.
* [tr31](https://github.com/openemv/tr31) - ASC X9 TR-31 library and tools.
* [trezor-crypto](https://github.com/trezor/trezor-firmware/tree/master/crypto) - Heavily optimized crypto algorithms for embedded devices.
* [tsip](https://github.com/dgryski/tsip) - Tiny sip-like hash. This repository has Go, amd64, C, and Rust implemtations of a small fast string
hashing function.
* [Themis](https://github.com/cossacklabs/themis) - crypto library for painless data security, providing symmetric and asymmetric encryption, secure sockets with forward secrecy, for mobile and server platforms.
* [xxHash](https://github.com/Cyan4973/xxHash) - Extremely fast hash algorithm.
* [xxhash_cpp](https://github.com/RedSpah/xxhash_cpp) - Port of the xxhash library to C++17.
* [x509ls](https://github.com/skip2/x509ls) - A text-based SSL certificate viewer. Shows an SSL server's certificate chain, and the validation path formed by OpenSSL.
* [zfs-crypto](https://github.com/zfsrogue/zfs-crypto) - ZFS On Linux with crypto patches.

## CSV
*Libraries for parsing Comma Separated Value (CSV) files*

* [commata](https://github.com/furfurylic/commata) - Just another header-only C++17 CSV parser.
* [csv2](https://github.com/p-ranav/csv2) - Fast CSV parser for modern C++.
* [csv_parser](https://github.com/tadasv/csv_parser) - This library provides a CSV parser to be used in an event loop for processing large amounts of streaming data.
* [csv_parser](https://github.com/Felipeasg/csv_parser) - C library for parsing CSV files.
* [Fast C++ CSV Parser](https://github.com/ben-strasser/fast-cpp-csv-parser) - Small, easy-to-use and fast header-only library for reading CSV files.
* [lazycsv](https://github.com/ashtum/lazycsv) - A fast, lightweight and single-header csv parser for modern C++.
* [libcsv](https://github.com/liu2guang/libcsv) - csv parser library.
* [minicsv](https://github.com/jedisct1/minicsv) - A tiny, fast, simple, single-file, BSD-licensed CSV parsing library in C.
* [ssp](https://github.com/red0124/ssp) - A header only "csv" parser which is fast and versatile with modern C++ api.
* [Vince's CSV Parser](https://github.com/vincentlaucsb/csv-parser) - A fast, self-contained, streaming C++17 CSV parser with optional type-casting and statistics.

## Database
*Database Libraries, SQL Servers, ODBC Drivers, and Tools*

* [Aerospike](https://github.com/aerospike/aerospike-server) - Aerospike is a distributed, scalable NoSQL database.
* [age](https://github.com/apache/age) - Graph database optimized for fast analysis and real-time data processing. It is provided as an extension to PostgreSQL.
* [AgensGraph](https://github.com/bitnine-oss/agensgraph) - AgensGraph, a transactional graph database based on PostgreSQL.
* [Akumuli](https://github.com/akumuli/Akumuli) - Akumuli is a time-series database for modern hardware.
* [ArangoDB](https://github.com/arangodb/arangodb) - ArangoDB is a multi-model, open-source database with flexible data models for documents, graphs, and key-values. Build high performance applications using a convenient SQL-like query language or JavaScript extensions. Use ACID transactions if you require them. Scale horizontally with a few mouse clicks.
* [ArcticDB](https://github.com/man-group/ArcticDB) - ArcticDB is a high performance, serverless DataFrame database built for the Python Data Science ecosystem. [arcticdb.io](http://arcticdb.io/)
* [ArDb](https://github.com/yinqiwen/ardb) - A redis protocol compatible nosql, it support multiple storage engines as backend like Google's LevelDB, Facebook's RocksDB, OpenLDAP's LMDB, PerconaFT, WiredTiger, ForestDB.
* [AtomSpace](https://github.com/opencog/atomspace) - The OpenCog (hyper-)graph database and graph rewriting system.
* [BaikalDB](https://github.com/baidu/BaikalDB) - BaikalDB, A Distributed HTAP Database.
* [Bedrock](https://github.com/Expensify/Bedrock) - Rock solid distributed database specializing in active/active automatic failover and WAN replication.
* [BerylDB](https://github.com/beryldb/beryldb) - BerylDB is a fully modular data structure data manager that can be used to store data as key-value entries. The server allows channel subscription and is optimized to be used as a cache repository. Supported structures include lists, sets, multimaps, and keys.
* [BerkeleyDB](https://www.oracle.com/database/berkeley-db/) - Library for a high-performance embedded database for key-value data.
* [Bluebox](https://github.com/tidwall/bluebox) - Bluebox is a tiny Redis clone for the purpose of displaying the capabilities of Neco, a network concurrency library for C. Supports SET, GET, DEL, and PING.
* [c-questdb-client](https://github.com/questdb/c-questdb-client) - Rust, C and C++ client for QuestDB InfluxDB Line Protocol over TCP.
* [CacheGrand](https://github.com/danielealbano/cachegrand) - cachegrand - a modern OSS Key-Value store built for today's hardware.
* [CalicoDB](https://github.com/andy-byers/CalicoDB) - A tiny embedded, transactional key-value database.
* [Ceph](https://github.com/ceph/ceph) - Ceph is a distributed object, block, and file storage platform. [WebSite](https://ceph.com/)
* [CacheLib](https://github.com/facebook/CacheLib) - Pluggable in-process caching engine to build and scale high performance services.
* [chDB](https://github.com/chdb-io/chdb) - chDB is an embedded OLAP SQL Engine powered by ClickHouse.
* [Citus](https://github.com/citusdata/citus) - Citus is a [PostgreSQL extension](https://www.citusdata.com/blog/2017/10/25/what-it-means-to-be-a-postgresql-extension/) that transforms Postgres into a distributed database—so you can achieve high performance at any scale.
* [ClickHouse](https://github.com/ClickHouse/clickhouse-cpp) - C++ client for ClickHouse DBMS.
* [CloudberryDb](https://github.com/cloudberrydb/cloudberrydb) - Cloudberry Database - Next generation unified database for Analytics and AI. [cloudberrydb.org](https://cloudberrydb.org/)
* [Comdb2](https://github.com/bloomberg/comdb2) - Comdb2 is a clustered RDBMS built on Optimistic Concurrency Control techniques. It provides multiple isolation levels, including Snapshot and Serializable Isolation. Read/Write transactions run on any node, with the client library transparently negotiating connections to lowest cost (latency) node which is available. The client library provides transparent reconnect.
* [Consus](https://github.com/rescrv/Consus) - Consus is a geo-replicated transactional key-value store.
* [corobase](https://github.com/sfu-dis/corobase) - Coroutine-Oriented Main-Memory Database Engine, VLDB 2021.
* [corvus](https://github.com/eleme/corvus) - Fast and lightweight Redis Cluster Proxy for Redis 3.0
* [cpp_redis](https://github.com/cpp-redis/cpp_redis) - C++11 Lightweight Redis client: async, thread-safe, no dependency, pipelining, multi-platform.
* [CrossDB](https://github.com/crossdb-org/crossdb) - Ultra High-performance Lightweight Embedded and Server OLTP RDBMS. [crossdb.org](https://crossdb.org/)
* [cruzdb](https://github.com/cruzdb/cruzdb) - Append-only key-value database on a distributed shared-log.
* [DBx1000](https://github.com/txaty/DBx1000) - DBx1000 is a single node OLTP database management system (DBMS). The goal of DBx1000 is to make DBMS scalable on future 1000-core processors.
* [doris](https://github.com/apache/doris) - Apache Doris is an easy-to-use, high performance and unified analytics database.
* [Dragonfly](https://github.com/dragonflydb/dragonfly) - A modern replacement for Redis and Memcached.
* [DuckDB](https://github.com/duckdb/duckdb) - [DuckDB](https://duckdb.org/) is an in-process SQL OLAP Database Management System.
* [Dynomite](https://github.com/Netflix/dynomite) - Inspired by [Dynamo whitepaper](http://www.allthingsdistributed.com/files/amazon-dynamo-sosp2007.pdf), is a thin, distributed dynamo layer for different storage engines and protocols. Currently these include Redis and Memcached. Dynomite supports multi-datacenter replication and is designed for high availability.
* [dqlite](https://github.com/canonical/dqlite) - Embeddable, replicated and fault-tolerant SQL engine.[dqlite.io](https://dqlite.io/)
* [ep-engine](https://github.com/couchbase/ep-engine) - Eventually Persistent Couchbase Data Layer.
* [ejdb](https://github.com/Softmotions/ejdb) - Embeddable JSON Database engine C library. Simple XPath like query language (JQL). Websockets / Android / iOS / React Native / Flutter / Java / Dart / Node.js bindings. Docker image.
* [Extensible-Storage-Engine](https://github.com/microsoft/Extensible-Storage-Engine) - ESE is an embedded / ISAM-based database engine, that provides rudimentary table and indexed access. However the library provides many other strongly layered and and thus reusable sub-facilities as well: A Synchronization / Locking library, a Data-structures / STL-like library, an OS-abstraction layer, and a Cache Manager.
* [FastoNoSQL](https://github.com/fastogt/fastonosql) - FastoNoSQL is a crossplatform Redis, Memcached, SSDB, LevelDB, RocksDB, UnQLite, LMDB, ForestDB, Pika, Dynomite, KeyDB GUI management tool. [fastonosql.com](https://fastonosql.com/)
* [FineDB](https://github.com/Amaury/FineDB) - High-performance key-value noSQL database. Written in C, multi-threaded, based on [LightningDB](http://symas.com/mdb/) and [nanomsg](http://nanomsg.org/). [Web site](finedb.org)
* [FoundationDB](https://github.com/apple/foundationdb) - FoundationDB is a distributed database designed to handle large volumes of structured data across clusters of commodity servers. It organizes data as an ordered key-value store and employs ACID transactions for all operations. It is especially well-suited for read/write workloads but also has excellent performance for write-intensive workloads.
	* [OpenTick](https://github.com/open-trade/opentick) - OpenTick is a fast tick database for financial timeseries data, built on FoundationDB with simplified SQL layer.
* [ForestDb](https://github.com/couchbase/forestdb) - A Fast Key-Value Storage Engine Based on Hierarchical B+-Tree Trie.
* [Greenplum Database (GPDB)](https://github.com/greenplum-db/gpdb) - Greenplum Database - Massively Parallel PostgreSQL for Analytics. An open-source massively parallel data platform for analytics, machine learning and AI.
* [GridDB](https://github.com/griddb/griddb) - GridDB is a next-generation open source database that makes time series IoT and big data fast,and easy.
* [Groonga](https://github.com/groonga/groonga) - Groonga is an open-source fulltext search engine and column store.
* [hiberlite](https://github.com/paulftw/hiberlite) - C++ Object-relational mapping for sqlite3.
* [Hiredis](https://github.com/redis/hiredis) - A minimalistic C client library for the Redis database.
* [hiredis-vip](https://github.com/vipshop/hiredis-vip) - Hiredis-vip is a C client library for the Redis database, supported redis cluster.
* [hse](https://github.com/hse-project/hse) - HSE is a fast embeddable key-value store designed for SSDs and persistent memory. HSE optimizes performance and endurance by orchestrating data placement across DRAM and multiple classes of solid-state storage.
* [Hyrise](https://github.com/hyrise/hyrise) - Hyrise is a research in-memory database.
* [Infinity](https://github.com/infiniflow/infinity) - The AI-native database built for LLM applications, providing incredibly fast vector and full-text search.
* [iowow](https://github.com/Softmotions/iowow) - The skiplist based persistent key/value storage engine.
* [IvorySQL](https://github.com/IvorySQL/IvorySQL) - Open Source Oracle Compatible PostgreSQL.
* [Jungle](https://github.com/eBay/Jungle) - An embedded key-value store library specialized for building state machine and log store.
* [KeyDB](https://github.com/Snapchat/KeyDB) - KeyDB is a high performance fork of Redis with a focus on multithreading, memory efficiency, and high throughput.
* [Kuzu](https://github.com/kuzudb/kuzu) - Embeddable property graph database management system built for query speed and scalability. Implements Cypher. [website](https://kuzudb.com/)
* [Kvrocks](https://github.com/apache/kvrocks) - Kvrocks is a distributed key value NoSQL database based on RocksDB and compatible with Redis protocol.
* [LevelDB](https://github.com/google/leveldb) - A fast key-value storage library written at Google that provides an ordered mapping from string keys to string values.
* [libpg_query](https://github.com/pganalyze/libpg_query) - C library for accessing the PostgreSQL parser outside of the server environment.
* [lf](https://github.com/zerotier/lf) - Fully Decentralized Fully Replicated Key/Value Store.
* [libmdbx](https://github.com/erthink/libmdbx) - One of the fastest embeddable key-value ACID database without WAL.
* [libfpta](https://github.com/erthink/libfpta) - Ultra fast, compact, Embedded Database for tabular and semistructured data.
* [libpqxx](https://github.com/jtv/libpqxx) - The official C++ client API for PostgreSQL.
* [librocksdb](https://github.com/holepunchto/librocksdb) - Asynchronous C bindings to RocksDB with support for batch operations.
* [LineairDB](https://github.com/LineairDB/LineairDB) - C++ fast transactional key-value storage.
* [LiteSQL](https://github.com/gittiver/litesql) - LiteSQL is a C++ ORM (object relational mapper) consisting of a codegenerator and C++ library that integrates C++ objects tightly to relational database and thus provides an object persistence layer. LiteSQL supports SQLite3, PostgreSQL, MySQL and oracle as backends.
* [LMDB](https://www.symas.com/lmdb/) - Very fast embedded key/value store with full ACID semantics.
* [LMDB++](https://github.com/bendiken/lmdbxx) - C++11 wrapper for the LMDB embedded database library.
* [LruClockCache](https://github.com/tugrul512bit/LruClockCache) - A low-latency LRU approximation cache in C++ using CLOCK second-chance algorithm. Multi level cache too. Up to 2.5 billion lookups per second.
* [ManticoreSearch](https://github.com/manticoresoftware/manticoresearch) - Easy to use open source fast database for search. Good alternative to Elasticsearch now, Drop-in replacement for E in the ELK soon. [manticoresearch.com](https://manticoresearch.com/)
* [MariaDB](https://mariadb.com/) - Robust, scalable and reliable SQL server, designed to be a drop-in replacement for MySQL.
* [matador](https://github.com/zussel/matador) - matador is an ORM and Web Application framework written in C++. It encapsulates all database backend communication and offers a simple (web) server.
* [memcache](https://github.com/memcached/memcached) - Free & open source, high-performance, distributed memory object caching system.
* [memgraph](https://github.com/memgraph/memgraph) - Open-source graph database, built for real-time streaming data, compatible with Neo4j.
* [mgclient](https://github.com/memgraph/mgclient) - C/C++ Memgraph Client.
* [MiniOB](https://github.com/oceanbase/miniob) - MiniOB is one mini database, helping developers to learn how database works.
* [MMKV](https://github.com/Tencent/MMKV) - MMKV is an efficient, small, easy-to-use mobile key-value storage framework used in the WeChat application. It's currently available on Android, iOS/macOS, Win32 and POSIX.
* [MonetDB](https://github.com/snaga/monetdb) - The MonetDB database system is a high-performance database kernel for query-intensive applications. The MonetDB kernel works together with an SQL frontend that is in a separate CVS module.
* [MongoDB C Driver](https://github.com/mongodb/mongo-c-driver) - MongoDB client library for C.
* [MongoDB C++ Driver](https://github.com/mongodb/mongo-cxx-driver) - C++ driver for MongoDB.
* [MongoDB Libbson](https://github.com/mongodb/libbson) - A BSON utility library.
* [MySQL++](http://www.tangentsoft.net/mysql++/) - A C++ wrapper for MySQL's C API.
* [mysql](https://github.com/boostorg/mysql) - MySQL C++ client based on Boost.Asio.
* [nanodbc](https://github.com/nanodbc/nanodbc) - A small C++ wrapper for the native C ODBC API.
* [Nebula Graph](https://github.com/vesoft-inc/nebula) - Nebula Graph is an open-source graph database capable of hosting super large-scale graphs with billions of vertices (nodes) and trillions of edges, with milliseconds of latency. It delivers enterprise-grade high performance to simplify the most complex data sets imaginable into meaningful and useful information.
* [NomaDB](https://github.com/DePizzottri/NomaDB) - Eventual consistent distributed key - data structure database based on CRDT.
* [OceanBase](https://github.com/oceanbase/oceanbase) - OceanBase Database is a native distributed relational database. It is developed entirely by Alibaba and Ant Group. OceanBase Database is built on a common server cluster. Based on the Paxos protocol and its distributed structure, OceanBase Database provides high availability and linear scalability. OceanBase Database is not dependent on specific hardware architectures.
* [ODB](https://www.codesynthesis.com/products/odb/) - An open-source, cross-platform, and cross-database object-relational mapping (ORM) system for C++.
* [Omnigres](https://github.com/omnigres/omnigres) - Omnigres makes Postgres a developer-first application platform. You can deploy a single database instance and it can host your entire application, scaling as needed.
* [OrioleDB](https://github.com/orioledb/orioledb) - building a modern cloud-native storage engine (... and solving some PostgreSQL wicked problems).
* [ormpp](https://github.com/qicosmos/ormpp) - modern C++ ORM, C++17, support mysql, postgresql, sqlite.
* [PaxosStore](https://github.com/Tencent/paxosstore) - PaxosStore has been deployed in WeChat production for more than two years, providing storage services for the core businesses of WeChat backend. Now PaxosStore is running on thousands of machines, and is able to afford billions of peak TPS.
* [Pedis](https://github.com/fastio/1store) - Pedis is the NoSQL data store using the SEASTAR framework, compatible with REDIS. The name of Pedis is an acronym of Parallel redis, which with high throughput and low latency.
* [Pegasus](https://github.com/apache/incubator-pegasus) - Pegasus is a distributed key-value storage system.
* [Percona Server](https://github.com/percona/percona-server) - Percona Server for MySQL is a free, fully compatible, enhanced, and open source drop-in replacement for any MySQL database. It provides superior performance, scalability, and instrumentation.
* [PerconaFT](https://github.com/Percona/PerconaFT) - PerconaFT is a high-performance, transactional key-value store, used in the TokuDB storage engine for Percona Server and MySQL, and in TokuMX, the high-performance MongoDB distribution. PerconaFT is provided as a shared library with an interface similar to Berkeley DB.
* [pgbouncer](https://github.com/pgbouncer/pgbouncer) - lightweight connection pooler for PostgreSQL.
* [pgvector](https://github.com/pgvector/pgvector) - Open-source vector similarity search for Postgres.
* [Pika](https://github.com/OpenAtomFoundation/pika) - Pika is a nosql compatible with redis, it is developed by Qihoo's DBA and infrastructure team.
* [PikiwiDB](https://github.com/OpenAtomFoundation/pikiwidb) - High-performance, large-capacity, multi-tenant, data-persistent, strong data consistency based on raft, Redis-compatible elastic KV data storage system based on RocksDB.
* [pgfe](https://github.com/dmitigr/pgfe) - PostgreSQL C++ driver.
* [PolarDB](https://github.com/alibaba/PolarDB-for-PostgreSQL) - PolarDB for PostgreSQL (PolarDB for short) is an open source database system based on PostgreSQL. It extends PostgreSQL to become a share-nothing distributed database, which supports global data consistency and ACID across database nodes, distributed SQL processing, and data redundancy and high availability through Paxos based replication.
* [Proton](https://github.com/timeplus-io/proton) - A streaming SQL engine, a fast and lightweight alternative to Apache Flink.
* [proxysql](https://github.com/sysown/proxysql) - High-performance MySQL proxy with a GPL license.
* [Qedis](https://github.com/loveyacper/Qedis) - A C++11 implementation of Redis Server, including cluster, use Leveldb for persist storage.
* [QxOrm](https://github.com/QxOrm/QxOrm) - QxOrm library - C++ Qt ORM (Object Relational Mapping) and ODM (Object Document Mapper) library - Official repository.
* [recutils](https://www.gnu.org/software/recutils/) - Set of tools and a C library for accessing human-editable, plaintext database files called recfiles.
* [Redcon](https://github.com/tidwall/redcon.c) - Redcon is a custom Redis server framework that is fast and simple to use. This is a C version of the [original Redcon](https://github.com/tidwall/redcon), and is built on top of [evio.c](https://github.com/tidwall/evio.c).
* [Redis](https://github.com/antirez/redis) - Redis is an in-memory database that persists on disk. The data model is key-value, but many different kind of values are supported: Strings, Lists, Sets, Sorted Sets, Hashes [website](http://redis.io/)
	* [docker-redis-cluster](https://github.com/sinbadxiii/docker-redis-cluster) - Docker Redis Cluster.
* [AsyncRedis client](https://github.com/boostorg/redis) - An async redis client designed for performance and scalability.
* [react-native-mmkv](https://github.com/mrousavy/react-native-mmkv) - The fastest key/value storage for React Native. ~30x faster than AsyncStorage.
* [RedisGraph](https://github.com/RedisGraph/RedisGraph) - A graph database as a Redis module.
* [Redpanda](https://github.com/redpanda-data/redpanda) - Redpanda is a streaming platform for mission critical workloads. Kafka® compatible, No Zookeeper, no JVM, and no code changes required. Use all your favorite open source tooling - 10x faster.
* [redrock](https://github.com/szstonelee/redrock) - Redis extend to disk by RocksDB.
* [RediSearch](https://github.com/RediSearch/RediSearch) - Full-Text search over Redis.
* [redis3m](https://github.com/luca3m/redis3m) - Wrapper of hiredis with clean C++ interface, supporting sentinel and ready to use patterns.
* [redis-cluster-tool](https://github.com/deep011/redis-cluster-tool) - Convenient and useful tool for redis cluster. It was primarily built to manage the redis cluster.
* [redis-cpp](https://github.com/tdv/redis-cpp) - redis-cpp is a header-only library in C++17 for Redis (and C++11 backport).
* [redis-nds](https://github.com/mpalmer/redis/tree/nds-2.6) - This is a version of Redis patched to implement NDS (the Naive Disk Store). Documentation on this feature is contained within README.nds.
* [redis-roaring](https://github.com/aviggiano/redis-roaring) - Roaring Bitmaps for Redis.
* [redis-plus-plus](https://github.com/sewenew/redis-plus-plus) - Redis client written in C++.
* [Reindexer](https://github.com/Restream/reindexer) - Embeddable, in-memory, document-oriented database with a high-level Query builder interface. [reindexer.io](https://reindexer.io/)
* [RethinkDB](https://github.com/rethinkdb/rethinkdb) - RethinkDB is the first open-source scalable database built for realtime applications. It exposes a new database access model, in which the developer can tell the database to continuously push updated query results to applications without polling for changes. RethinkDB allows developers to build scalable realtime apps in a fraction of the time with less effort.
* [RocksDB](https://github.com/facebook/rocksdb) - Embedded key-value store for fast storage from facebook.
* [RonDB](https://github.com/logicalclocks/rondb) - This is RonDB, a distribution of NDB Cluster developed and used by Hopsworks AB. It also contains development branches of RonDB.
* [rqlite](https://github.com/rqlite/rqlite) - The lightweight, distributed relational database built on SQLite.
* [SequoiaDB](https://github.com/SequoiaDB/SequoiaDB) - SequoiaDB is a MySQL/PostgreSQL-compatible distributed relational database. It supports ACID, horizontal scaling, HA/DR and Multi-model data storage engine.
* [Scylla](https://github.com/scylladb/scylla) - Scylla is the real-time big data database that is API-compatible with Apache Cassandra and Amazon DynamoDB.
* [sharedhashfile](https://github.com/simonhf/sharedhashfile) - Share Hash Tables With Stable Key Hints Stored In Memory Mapped Files Between Arbitrary Processes.
* [SimDB](https://github.com/LiveAsynchronousVisualizedArchitecture/simdb) - High performance, shared memory, lock free, cross platform, single file, minimal dependencies, C++11 key-value store.
* [SOCI](https://github.com/SOCI/soci) - A database abstraction layer for C++.
* [sophia](http://sophia.systems/) - Modern, embeddable key-value database.
* [sparkey](https://github.com/spotify/sparkey) - Sparkey is a simple constant key/value storage library. It is mostly suited for read heavy systems with infrequent large bulk inserts. It includes both a C library for working with sparkey index and log files (libsparkey), and a command line utility for getting info about and reading values from a sparkey index/log (sparkey).
* [Speedb](https://github.com/speedb-io/speedb) - Community-led project: A RocksDB compliant high performance scalable embedded key-value store.
* [SplinterDb](https://github.com/vmware/splinterdb) - High Performance Embedded Key-Value Store.
* [sqlean](https://github.com/nalgeon/sqlean) - The ultimate set of SQLite extensions.
* [sqlite-gui](https://github.com/little-brother/sqlite-gui) - Lightweight SQLite editor for Windows.
* [ssdb](https://github.com/ideawu/ssdb) - SSDB is a high performace key-value(key-string, key-zset, key-hashmap) NoSQL database, an alternative to Redis.
* [StarRocks](https://github.com/StarRocks/starrocks) - StarRocks is a next-gen sub-second MPP database for full analytics scenarios, including multi-dimensional analytics, real-time analytics and ad-hoc query.
* [SQLCipher](https://github.com/sqlcipher/sqlcipher) - SQLCipher is a standalone fork of SQLite that adds 256 bit AES encryption of database files and other security features.
* [SQLite](http://www.sqlite.org/) - A completely embedded, full-featured relational database in a few 100k that you can include right into your project.
	* [botansqlite3](https://github.com/OlivierJG/botansqlite3) - Sqlite3 encryption codec to allow full database encryption using the algorithms supported by Botan.
	* [crypto_sqlite](https://github.com/viaduck/crypto_sqlite) - SQLite3 encryption interface for full page encryption with custom crypto provider.
	* [sqlite-vec](https://github.com/asg017/sqlite-vec) - An extremely small, "fast enough" vector search SQLite extension that runs anywhere.
* [SQLiteC++](https://github.com/SRombauts/SQLiteCpp) - SQLiteC++ (SQLiteCpp) is a smart and easy to use C++ SQLite3 wrapper.
* [sqlite_modern_cpp](https://github.com/SqliteModernCpp/sqlite_modern_cpp) - Header only C++14 wrapper around sqlite library.
* [sqlite_orm](https://github.com/fnc12/sqlite_orm) - SQLite ORM light header only library for modern C++.
* [sqlpp11](https://github.com/rbock/sqlpp11) - A type safe embedded domain specific language for SQL queries and results in C++.
* [tair](https://github.com/alibaba/tair) - A distributed key-value storage system developed by Alibaba Group.
* [TDengine](https://github.com/taosdata/TDengine) - An open-source time-series database with high-performance, scalability and SQL support. It can be widely used in IoT, Connected Vehicles, DevOps, Energy, Finance and other fields.
* [Tendis](https://github.com/Tencent/Tendis) - Tendis is a high-performance distributed storage system fully compatible with the Redis protocol.
* [Tera](https://github.com/baidu/tera) - Tera is a high performance distributed NoSQL database, which is inspired by google's [BigTable](http://static.googleusercontent.com/media/research.google.com/zh-CN//archive/bigtable-osdi06.pdf) and designed for real-time applications.
* [TerarkDb](https://github.com/bytedance/terarkdb) - A RocksDB compatible KV storage engine with better performance.
* [TidesDB](https://github.com/tidesdb/tidesdb) - High-performance, durable, transactional embedded storage engine designed for flash and RAM optimization. [website](https://tidesdb.com/)
* [TigerBeetleDB C++ client (Community)](https://github.com/kassane/tigerbeetle-cpp) - [TigerBeetle][https://tigerbeetle.com/] is a financial accounting database designed for mission critical safety and performance to power the future of financial services.
* [TileDB](https://github.com/TileDB-Inc/TileDB) - Fast Dense and Sparse Multidimensional Array DBMS. [website](https://tiledb.io/)
* [TimescaleDB](https://github.com/timescale/timescaledb) - Database designed to make SQL scalable for time-series data. It is engineered up from PostgreSQL and packaged as a PostgreSQL extension, providing automatic partitioning across time and space (partitioning key), as well as full SQL support.
* [TinyRedis_RPC](https://github.com/zk1556/TinyRedis_RPC) - Lightweight Redis based on RPC framework.
* [TinyORM](https://github.com/silverqx/TinyORM) - Modern C++ ORM library. [website](https://www.tinyorm.org/).
* [Todis](https://github.com/topling/todis) - Todis is a massive, persistent Redis server developed by Topling Inc.
* [TokuDB](https://github.com/percona/tokudb-engine) - TokuDB is a high-performance, write optimized, transactional storage engine for Percona Server and MySQL. For more details, see our [product page](https://www.percona.com/software/percona-tokudb).
* [TokuMX](https://github.com/Tokutek/mongo) - TokuMX is a high-performance distribution of MongoDB, a document-oriented database with built-in sharding and replication, built on Tokutek's [Fractal Tree indexes](https://github.com/Tokutek/ft-index).
* [ToplingDB](https://github.com/topling/toplingdb) - ToplingDB is a cloud native LSM Key-Value Store with searchable compression algo and distributed compaction
* [Typesense](https://github.com/typesense/typesense) - Open Source alternative to Algolia and an Easier-to-Use alternative to ElasticSearch zap mag sparkles Fast, typo tolerant, in-memory fuzzy Search Engine for building delightful search experiences.
* [upscaledb](https://upscaledb.com) - An embedded "typed" key/value store with a built-in query interface.
* [UnQLite](https://github.com/symisc/unqlite) - Self-contained, serverless, zero-configuration, transactional NoSQL engine with a C interface.[unqlite.symisc.net](https://unqlite.symisc.net/)
* [urkel](https://github.com/chjj/liburkel) - An optimized and cryptographically provable key-value store. Written in C.
* [UStore](https://github.com/unum-cloud/ustore) - Multi-Modal database for BLOBs, JSON, and graphs.
* [valkey](https://github.com/valkey-io/valkey) - A flexible distributed key-value datastore that supports both caching and beyond caching workloads. [valkey.io](https://valkey.io/)
* [vedis](https://github.com/symisc/vedis) - An Embedded Implementation of Redis.
* [Velox](https://github.com/facebookincubator/velox) - A C++ vectorized database acceleration library aimed to optimizing query engines and data processing systems. [Website](https://velox-lib.io/)
* [VoltDB](https://github.com/VoltDB/voltdb/) - VoltDB is a horizontally-scalable, in-memory SQL RDBMS designed for applications that benefit from strong consistency, high throughput and low, predictable latency.
* [ybc](https://github.com/valyala/ybc) - YBC - Yet another Blob Cache library. This library implements fast in-process blob cache with persistence support.
* [YugabyteDB](https://github.com/yugabyte/yugabyte-db) - YugabyteDB is a high-performance, cloud-native distributed SQL database that aims to support all PostgreSQL features. It is best fit for cloud-native OLTP (i.e. real-time, business critical) applications that need absolute data correctness and require at least one of the following: scalability, high tolerance to failures, globally-distributed deployments.
* [YTsaurus](https://github.com/ytsaurus/ytsaurus) - YTsaurus is a distributed storage and processing platform for big data with support for MapReduce model, a distributed file system and a NoSQL key-value database. [Website](https://ytsaurus.tech/)
* [WCDB](https://github.com/Tencent/wcdb) - WCDB is a cross-platform database framework developed by WeChat.
* [WhiteDB](http://whitedb.org) - Lightweight database library, operating entirely in main memory.
* [webdis](https://github.com/nicolasff/webdis) - A very simple web server providing an HTTP interface to Redis. It uses [hiredis](https://github.com/antirez/hiredis), [jansson](https://github.com/akheron/jansson), [libevent](https://monkey.org/~provos/libevent/), and [http-parser](https://github.com/ry/http-parser/).

### Database Tools

* [sqlitestudio](https://github.com/pawelsalawa/sqlitestudio) - A free, open source, multi-platform SQLite database manager.

## Data Structures ##

This section has big libraries that provide data structures and other stuff you expect of a 'modern' standard library.

* [APR](http://apr.apache.org/) - Apache Portable Runtime; another library of cross-platform utility functions.
* [Algorithms](https://github.com/Kumar-laxmi/Algorithms) - A Repository for algorithms in C, C++, Python and Java.
* [avlmini](https://github.com/skywind3000/avlmini) - AVL implementation which is as fast/compact as linux's rbtree.
* [bplustree](https://github.com/begeekmyfriend/bplustree) - A minimal but extreme fast B+ tree indexing structure demo for billions of key-value storage.
* [BTree](https://github.com/frozenca/BTree) - A general-purpose high-performance lightweight STL-like modern C++ B-Tree.
* [btrees](https://github.com/wichtounet/btrees) - C++ implementation of concurrent Binary Search Trees.
* [c-hashmap](https://github.com/Mashpoe/c-hashmap) - A fast hash map/hash table (whatever you want to call it) for the C programming language. It can associate a key with a pointer or integer value in O(1) time.
* [C-Macro-Collections](https://github.com/LeoVen/C-Macro-Collections) - Generate simple and generic data structures using macros.
* [C Algorithms](https://fragglet.github.io/c-algorithms/) - Collection of common algorithms and data structures for C.
* [CLIST](https://github.com/AlexanderAgd/CLIST) - Simple and lightweight [dynamic array](https://en.wikipedia.org/wiki/Dynamic_array) implementation.
* [code-library](https://github.com/ShahjalalShohag/code-library) - Templates, algorithms and data structures implemented and collected for programming contests.
* [Collections-C](https://github.com/srdja/Collections-C) - Library of generic data structures.
* [CPL](http://www.eso.org/sci/software/cpl/) - The Common Pipeline Library; a set of libraries designed to be a comprehensive, efficient and robust software toolkit.
* [cpp-btree](https://github.com/enascimento/cpp-btree) - C++ B-tree is a template library that implements ordered in-memory containers based on a B-tree data structure.
* [cpp-btree](https://github.com/Kronuz/cpp-btree) - Modern C++ B-tree containers.
* [cstl](https://github.com/facil-io/cstl) - C STL - a Simple Template Library for C, including JSON processing, hash maps, dynamic arrays, binary strings and more.
* [ds](https://github.com/recp/ds) - Common Data Structures and Algorithms.
* [EFL](https://www.enlightenment.org/) - Large collection of useful data structures and functions.
* [GLib](https://wiki.gnome.org/Projects/GLib) - Library of utility functions and structures, designed to be portable, efficient and powerful.
* [GIO](https://developer.gnome.org/gio/) - Modern and easy-to-use VFS API.
* [GObject](https://developer.gnome.org/gobject/stable/) - Object-oriented system and object model for C.
* [hamt](https://github.com/mkirchner/hamt) - A hash array-mapped trie (HAMT) implementation in C99. A HAMT is a data structure that can be used to efficiently implement persistent associative arrays (aka maps, dicts) and sets.
* [hashmap.h](https://github.com/sheredom/hashmap.h) - A simple one header hashmap implementation for C/C++.
* [igraph](https://igraph.org/) - A graph processing library.
* [kdtree](https://github.com/jtsiomb/kdtree) - Simple library for working with KD-trees.
* [labynkyr](https://github.com/enascimento/labynkyr) - C++11 implementation of side-channel key rank estimation and parallel key search algorithms.
* [libavl](http://adtinfo.org/libavl.html/index.html) - Library containing a range of self-balancing binary trees.
* [libcdada](https://msune.github.io/libcdada/) - A small, portable, MACRO-less library for basic data structures (list, set, map, queue...) in C (C++ backend).
* [libkdtree](https://github.com/nvmd/libkdtree) - libkdtree++ is a C++ template container implementation of k-dimensional space sorting, using a kd-tree.
* [liblfds](https://liblfds.org/) - Portable lock-free data structure library. Public domain (more exactly, whatever license you want).
* [libsrt](https://faragon.github.io/libsrt.html) - Soft and hard real-time data structures.
* [list.h](https://github.com/nbulischeck/list.h) - Implementations for singly linked and doubly linked list functions.
* [lrucache11](https://github.com/mohaps/lrucache11) - A header only C++11 LRU Cache template class that allows you to define key, value and optionally the Map type. uses a double linked list and a std::unordered_map style container to provide fast insert, delete and update.
* [M*LIB](https://github.com/P-p-H-d/mlib) - Library for generic, but typesafe C containers. Implemented as header-only.
* [MyTinySTL](https://github.com/Alinshans/MyTinySTL) - Achieve a tiny STL in C++11.
* [offbrand](https://github.com/theck01/offbrand_lib) - Collection of generic, reference-counted data structures.
* [OpenVDB](https://github.com/AcademySoftwareFoundation/openvdb) - open source C++ library comprising a novel hierarchical data structure and a large suite of tools for the efficient storage and manipulation of sparse volumetric data discretized on three-dimensional grids.
* [ordered-map](https://github.com/Tessil/ordered-map) - C++ hash map and hash set which preserve the order of insertion.
* [PackedArray](https://github.com/gpakosz/PackedArray) - Random-access array of tightly packed unsigned integers of any desired width. Has a SIMD-optimized implementation.
* [Parallel Hashmap](https://github.com/greg7mdp/parallel-hashmap) - A family of header-only, very fast and memory-friendly hashmap and btree containers.
* [PBL](http://www.mission-base.com/peter/source/) - Large library of utilities, featuring data structures, among other things.
* [pqueue](https://github.com/tidwall/pqueue.c) - Priority queue for C.
* [range-v3](https://github.com/ThePhD/range-v3) - Range library for C++11/14/17. This code is the basis of a [formal proposal](https://ericniebler.github.io/std/wg21/D4128.html) to add range support to the C++ standard library.
* [rb3ptr](http://jstimpfle.de/projects/rb3ptr/rb3ptr.html) - Red-Black tree. Exposes almost all implementation primitives, so can be used for scenarios like augmentation, multiple compatible ordering functions, and more.
* [qlibc](http://wolkykim.github.io/qlibc/) - Simple and powerful C library, designed as a replacement for GLib while focusing on being small and light.
* [quadsort](https://github.com/scandum/quadsort) - Quadsort is a stable adaptive merge sort which is faster than quicksort.
* [tensorstore](https://github.com/google/tensorstore) - Library for reading and writing large multi-dimensional arrays.
* [uthash](http://troydhanson.github.io/uthash) - Single-file hash table implementation.
* [vector.h](https://github.com/swenson/vector.h) - C header library for typed lists (using macros and "template" C).
* [vla](https://github.com/cloudwu/vla) - Variable length array.

### Big Integer

* [wide-integer](https://github.com/ckormanyos/wide-integer) - Wide-Integer implements a generic C++ template for uint128_t, uint256_t, uint512_t, uint1024_t, etc.

### Graph

* [CXXGraph](https://github.com/ZigRazor/CXXGraph) - free C++(17) graph header-only library for representation and algorithms execution.
* [Graaf](https://github.com/bobluppes/graaf) - A general-purpose lightweight C++20 graph library. [website](https://bobluppes.github.io/graaf/)

### Interval Tree

* [AVL Tree Integer Intervals](https://github.com/alexandrucatana/AVL-Tree-for-Integer-Intervals) - This is an implementation of an AVL Tree having as node values closed integer intervals of the form [4, 16], where 4 is the left-bounded value and 16 is right-bounded value.
* [cgranges](https://github.com/lh3/cgranges) - A C/C++ library for fast interval overlap queries (with a "bedtools coverage" example).
* [cpp_trees](https://github.com/emin63/cpp_trees) - The cpp_trees repo provides various tree data structures in C++ along with some simple unit tests.
* [interval-tree](https://github.com/5cript/interval-tree) - A C++ header only interval tree implementation.
* [interval-tree](https://github.com/NicoG60/interval-tree) - Implementation of an augmented AVL interval tree in the STL fashion.
* [interval-tree](https://github.com/joshjo/interval-tree) - Interval tree implementation.
* [interval_tree](https://github.com/epaminondas/interval_tree) - An STL-like ordered tree data structure to hold intervals.
* [interval_tree_cpp](https://github.com/borzacchiello/interval_tree_cpp) - CLRS's Interval Tree implementation in C++.
* [IntervalTree](https://github.com/IvanPinezhaninov/IntervalTree) - A red-black self-balancing interval tree C++11 header-only implementation.
* [intervaltree](https://github.com/frank2/intervaltree) - An interval tree implementation in C++.
* [intervaltree](https://github.com/ekg/intervaltree) - This library provides a basic implementation of an interval tree using C++ templates, allowing the insertion of arbitrary types into the tree.
* [IntervalTreess](https://github.com/Macuh/IntervalTreess) 
* [iTree](https://github.com/juncongmoo/itree) - iTree - an Interval Tree library. In computer science, an interval tree is a tree data structure to hold intervals. Every node in itree has a start and an end value.
* [ITree](https://github.com/coolsoftware/ITree) - C++ Interval Tree implementation.
* [parallel-interval-tree](https://github.com/gkrupp/parallel-interval-tree) - Parallel interval tree.
* [range-tree](https://github.com/mocherson/range-tree) - Implementation of range tree, range tree with fractional cascading and interval tree.
* [rangetree](https://github.com/luoyuanlab/rangetree) - Implementation of the fastest interval tree querying algorithm based on range tree data structure. Work on genomic and time intervals etc. Paper in Scientific Reports.
* [Range trees and Interval Trees](https://github.com/meghanamreddy/Range-trees-and-Interval-Trees) - Implementation of range trees and interval trees for windowing queries.
* [range-v3](https://github.com/ericniebler/range-v3) - Range library for C++14/17/20, basis for C++20's std::ranges.
* [red-black-and-interval-trees](https://github.com/JanX2/red-black-and-interval-trees) - This is some code for red-black trees and interval trees in C++.
* [test-itree](https://github.com/bio-nim/test-itree) - Compare C++ interval-tree implementations.
* [UniqueIntervalTree](https://github.com/mewais/UniqueIntervalTree) - A C++ implementation for an IntervalTree that does not allow overlapping ranges.

### Miscellaneous Data Structures and Algorithms

* [aatree](https://github.com/tidwall/aatree) - An implementation of the (AA tree)[https://en.wikipedia.org/wiki/AA_tree] in C.
* [atree](https://github.com/tlack/atree) - Apter Trees are a simpler representation of trees using just two vectors: [nodevalues, parentindices].
* [cp-algorithms](https://github.com/cp-algorithms/cp-algorithms) - Algorithm and data structure articles for (https://cp-algorithms.com)[https://cp-algorithms.com] (based on (http://e-maxx.ru)[http://e-maxx.ru])
* [Consistent Hashing](https://github.com/phaistos-networks/ConsistentHashing) - Consistent Hashing data structures and algorithms for C++
* [Fast Algorithms in Data Structures](https://github.com/andy489/Fast_Algorithms_in_Data_Structures) - LA, LCA, Perfect Hash, RMQ, Segment Tree, Sparse Table, Treap, vEB Tree, x-fast Tree (Willard), Suffix Automaton (Blumer et al.), Suffix Tree (Ukkonen)
* [lager](https://github.com/arximboldi/lager) - C++ library for value-oriented design using the unidirectional data-flow architecture — Redux for C++ [sinusoid.es/lager](https://sinusoid.es/lager/)
* [mmmulti](https://github.com/ekg/mmmulti) - Memory mapped multimap, multiset, and implicit interval tree based on an in-place parallel sort.
* [Ring-Buffer](https://github.com/AndersKaloer/Ring-Buffer) - A simple ring buffer (circular buffer) designed for embedded systems.
* [Ring-Buffer](https://github.com/jnk0le/Ring-Buffer) - Simple C++11 ring buffer implementation, allocated and evaluated at compile time.
* [ring-buffer](https://github.com/jpcima/ring-buffer) - Ring buffer library for C++17.
* [sdsl-lite](https://github.com/simongog/sdsl-lite) - Succinct Data Structure Library 2.0
* [SPSCQueue](https://github.com/rigtorp/SPSCQueue) - A single producer single consumer wait-free and lock-free fixed size queue written in C++11.

### Queue

* [MPMCQueue](https://github.com/rigtorp/MPMCQueue) - A bounded multi-producer multi-consumer concurrent queue written in C++11.
* [work-stealing-queue](https://github.com/taskflow/work-stealing-queue) - Fast work-stealing queue template in C++.

### Segment Tree

* [ygg](https://github.com/tinloaf/ygg) - An intrusive C++17 implementation of a Red-Black-Tree, a Weight Balanced Tree, a Dynamic Segment Tree and much more.

## Data visualization
**Data visualization Libraries*

* [gplot++](https://github.com/ziotom78/gplotpp) - Cross-platform header-only C++ plotting library that interfaces with Gnuplot.
* [matplotplusplus](https://github.com/alandefreitas/matplotplusplus) - C++ Graphics Library for Data Visualization. [website](https://alandefreitas.github.io/matplotplusplus/)
* [morphologica](https://github.com/ABRG-Models/morphologica) - C++ header-only graphing and data visualization with modern OpenGL. [website](https://abrg-models.github.io/morphologica/)

## Debug
*Debugging Libraries, Memory Leak and Resource Leak Detection, Unit Testing*

* [backward-cpp](https://github.com/bombela/backward-cpp) - A beautiful stack trace pretty printer for C++.
* [benchmark](https://github.com/google/benchmark) - Google provided small microbenchmark support library.
* [Boost.Test](https://github.com/boostorg/test) - Boost Test Library.
* [bytehound](https://github.com/koute/bytehound) - A memory profiler for Linux.
* [check](https://github.com/libcheck/check) - Check is a unit testing framework for C. [website](https://libcheck.github.io/check/)
* [doctest](https://github.com/onqtam/doctest) - The lightest feature rich C++ single header testing framework.
* [Catch](https://github.com/philsquared/Catch) - A modern, C++-native, header-only, framework for unit-tests, TDD and BDD.
* [Catch2](https://github.com/catchorg/Catch2) - A modern, C++-native, test framework for unit-tests, TDD and BDD.
* [Celero](https://github.com/DigitalInBlue/Celero) - C++ Benchmarking Framework.
* [Coz](https://github.com/plasma-umass/coz) - Coz is a new kind of profiler that unlocks optimization opportunities missed by traditional profilers.
* [cpp-dump](https://github.com/philip82148/cpp-dump) - A C++ library for debugging purposes that can print any variable, even user-defined types.
* [CppUTest](https://github.com/cpputest/cpputest) - Unit testing and mocking framework for C/C++.
* [Criterion](https://github.com/Snaipe/Criterion) - A cross-platform C and C++ unit testing framework for the 21st century.
* [CUTE](http://cute-test.com) - C++ Unit Testing Easier.
* [CMocka](https://cmocka.org/) - unit testing framework for C with support for mock objects.
* [CppBenchmark](https://github.com/chronoxor/CppBenchmark) - Performance benchmark framework for C++ with nanoseconds measure precision.
* [Cpptrace](https://github.com/jeremy-rifkin/cpptrace) - A simple, portable, and self-contained C++ stacktrace library supporting C++11 and greater.
* [CppUnit](http://www.freedesktop.org/wiki/Software/cppunit/) - C++ port of JUnit. [LG
* [CTest](https://cmake.org/cmake/help/v2.8.8/ctest.html) - The CMake test driver program.
* [cwrap](https://cwrap.org) - Testing full software stack on a single machine.
* [dbg-macro](https://github.com/sharkdp/dbg-macro) - A dbg(…) macro for C++.
* [Deleaker](http://www.deleaker.com) - A tool for resource leak detection, including memory, GDI and handle leaks.
* [FakeIt](https://github.com/eranpeer/FakeIt) - Simple mocking framework for C++.
* [fff](https://github.com/meekrosoft/fff) - A micro-framework for creating fake C functions.
* [Google Mock](https://github.com/google/googletest/blob/master/googlemock/README.md) - A library for writing and using C++ mock classes.
* [Google Test](https://github.com/google/googletest) - Google C++ Testing Framework.
* [GWPSan](https://github.com/google/gwpsan) - Framework for low-overhead sampling-based dynamic binary instrumentation, designed for implementing various bug detectors (also called "sanitizers") suitable for production uses.
* [jaeger.logz.io](https://github.com/dawidborycki/jaeger.logz.io) - A sample source code showing how to instrument c++ Microservices for distributed tracing with Jaeger and Logz.io.
* [Hippomocks](https://github.com/dascandy/hippomocks) - Single-header mocking framework.
* [ig-debugheap](https://github.com/deplinenoise/ig-debugheap) - Multiplatform debug heap useful for tracking down memory errors.
* [libassert](https://github.com/jeremy-rifkin/libassert) - The most over-engineered C++ assertion library.
* [libbacktrace](https://github.com/ianlancetaylor/libbacktrace) - A C library that may be linked into a C/C++ program to produce symbolic backtraces.
* [libtap](https://github.com/zorgnax/libtap) - Write tests in C.
* [lnav](https://github.com/tstack/lnav) - Log file navigator.
* [magic-trace](https://github.com/janestreet/magic-trace) - magic-trace collects and displays high-resolution traces of what a process is doing.
* [MemTrack](http://www.almostinfinite.com/memtrack.html) - Tracking memory allocations in C++.
* [microprofile](https://github.com/jonasmr/microprofile) - Profiler with web-view for multiple platforms.
* [MinUnit](https://github.com/siu/minunit) - A minimal unit testing framework for C self-contained in a single header file.
* [Mockator](http://www.mockator.com) - Eclipse CDT plug-in for C++ Seams and Mock Objects.
* [nanobench](https://github.com/martinus/nanobench) - Simple, fast, accurate single-header microbenchmarking functionality for C++11/14/17/20.
* [Nanotimer](https://github.com/mattreecebentley/plf_nanotimer) - A simple low-overhead cross-platform timer class for benchmarking.
* [Nonius](https://github.com/libnonius/nonius) - A C++ micro-benchmarking framework.
* [otel.logz.io](https://github.com/dawidborycki/otel.logz.io) - A sample source code showing how to instrument c++ Microservices for distributed tracing with OpenTelemetry C++, Jaeger, Logz.io, and OpenTelemetry Collector.
* [palanteer](https://github.com/dfeneyrou/palanteer) - Visual Python and C++ nanosecond profiler, logger, tests enabler.
* [Remotery](https://github.com/Celtoys/Remotery) - Single C File Profiler with Web Viewer.
* [tcpkali](https://github.com/satori-com/tcpkali) - Fast multi-core TCP and WebSockets load generator.
* [trompeloeil](https://github.com/rollbear/trompeloeil) - Header only C++14 mocking framework.
* [snitch](https://github.com/cschreib/snitch) - Lightweight C++20 testing framework.
* [Touca](https://github.com/trytouca/trytouca) - Automated regression testing system for testing complex mission-critical workflows.
* [UnitTest++](https://github.com/unittest-cpp/unittest-cpp) - A lightweight unit testing framework for C++.
* [Unity](https://github.com/ThrowTheSwitch/Unity) - Simple Unit Testing for C.
* [utest.h](https://github.com/sheredom/utest.h) - Single header unit testing framework for C and C++.
* [μt](https://github.com/boost-ext/ut) - C++20 single header/single module, macro-free μ(micro)/Unit Testing Framework.
* [VLD](https://kinddragon.github.io/vld) - Visual Leak Detector. A free, robust, open-source memory leak detection system for Visual C++.

## Distributed Systems

* [3TS](https://github.com/Tencent/3TS) - Tencent Transaction Processing Testbed System.
* [Calvin](https://github.com/yaledb/calvin) - Calvin is a scalable transactional database system that leverages determinism to guarantee active replication and full ACID-compliance of distributed transactions without two-phase commit. 
* [cirrus](https://github.com/ucbrise/cirrus) - Serverless ML Framework.
* [Curve](https://github.com/opencurve/curve) - Curve is a high-performance, lightweight-operation, cloud-native open source distributed storage system. Curve can be applied to: 1) mainstream cloud-native infrastructure platforms OpenStack and Kubernetes; 2) high-performance storage for cloud-native databases; 3) cloud storage middleware using S3-compatible object storage as a data storage.
* [couchbase-transactions-cxx](https://github.com/couchbase/couchbase-transactions-cxx) - Distributed Transactions for Couchbase, C++ API.
* [cpp-minter](https://github.com/MinterTeam/cpp-minter) - Minter C++ SDK: build and sign any transaction, generate mnemonic with private and public key.
* [Dogee](https://github.com/Menooker/Dogee) - C++ distributed platform for shared memory programming.
* [draft](https://github.com/deepgrace/draft) - The distributed, reliable, replicated, redundant and fault tolerant system.
* [endurox](https://github.com/endurox-dev/endurox) - Enduro/X is Open Source Middleware Platform for Distributed Transaction Processing.
* [fibre](https://github.com/samuelsadok/fibre) - Abstraction layer for painlessly building object oriented distributed systems that just work.
* [fuurin](https://github.com/mdamiani/fuurin) - Simple and fast ZeroMQ-based communication library.
* [glusterfs](https://github.com/gluster/glusterfs) - Gluster Filesystem : Build your distributed storage in minutes.
* [htm-unordered-queue](https://github.com/zhzhang2012/htm-unordered-queue) - A parallel-friendly unordered queue based on Hardware Transactional Memory.
* [HySortK](https://github.com/CornellHPC/HySortK) - High Performance Sorting Based Distributed memory K-mer counter.
* [infinisql](https://github.com/infinisql/infinisql) - Extreme Scale Distributed Transaction Processing [www.infinisql.org](http://www.infinisql.org/)
* [libatapp](https://github.com/atframework/libatapp) - Server app framework based on libatbus.
* [LightCTR](https://github.com/cnkuangshi/LightCTR) - Lightweight and Scalable framework that combines mainstream algorithms of Click-Through-Rate prediction based computational DAG, philosophy of Parameter Server and Ring-AllReduce collective communication.
* [LizardFS](https://github.com/lizardfs/lizardfs) - LizardFS is a highly reliable, scalable and efficient distributed file system. It spreads data over a number of physical servers, making it visible to an end user as a single file system.
* [lstm](https://github.com/mtak-/lstm) - lstm is a header only C++14 take on Software Transactional Memory (STM).
* [NanoSQL](https://github.com/htfy96/NanoSQL) - Nano encapsulation of SQLite with transaction support.
* [Nebula](https://github.com/Bwar/Nebula) - Nebula is a powerful framework for building highly concurrent, distributed, and resilient message-driven applications for C++.
* [nebula](https://github.com/varchar-io/nebula) - A distributed block-based data storage and compute engine.
* [NuRaft](https://github.com/eBay/NuRaft) - C++ implementation of Raft core logic as a replication library.
* [Ouroboros](https://github.com/belyaev-ms/ouroboros) - Embedded NoSQL storage of data
* [Pixie](https://github.com/pixie-io/pixie) - Instant Kubernetes-Native Application Observability. [website](https://px.dev/)
* [polaris](https://github.com/chenhao-ye/polaris) - Source code for the SIGMOD '23 paper “Polaris: Enabling Transaction Priority in Optimistic Concurrency Control”.
* [redo-ptm](https://github.com/anonpmcoder/redo-ptm) - multiple wait-free Persistent Transactional Memories (PTM).
* [Service Fabric](https://github.com/microsoft/service-fabric) - Service Fabric is a distributed systems platform for packaging, deploying, and managing stateless and stateful distributed applications and containers at large scale.
* [shirakami](https://github.com/project-tsurugi/shirakami) - Transactional key-value store.
* [Skale Consensus](https://github.com/skalenetwork/skale-consensus) - Running the very core of SKL network, SKALE BFT consensus is universal, modern, modular, high-performance, asynchronous, provably-secure, agent-based Proof-of-Stake blockchain consensus engine in C++ 17. Includes provably secure embedded Oracle. Used by SKALE elastic blockchains. Easy and flexible enough to implement your own blockchain or smart.
* [trade.cpp](https://github.com/per-framework/trade.cpp) - A transactional locking implementation for C++.
* [TransactionalDataStructures](https://github.com/liadab/TransactionalDataStructures) - This library implement a transactional data structure library, which enables the use of transactions. These transactions ensures atomicity of a series of operations on different data sets.
* [transmem](https://github.com/mfs409/transmem) - Transactional Memory Algorithms and Benchmarks.
* [v6d](https://github.com/v6d-io/v6d) - vineyard (v6d): an in-memory immutable data manager. (Project under CNCF, TAG-Storage).
* [wukong](https://github.com/SJTU-IPADS/wukong) - A graph-based distributed in-memory store that leverages efficient graph exploration to provide highly concurrent and low-latency queries over big linked data.
* [zookeeper-cpp](https://github.com/tgockel/zookeeper-cpp) - A ZooKeeper client for C++.

## Documentation

* [Doxide](https://github.com/lawmurray/doxide) - Modern documentation for modern C++, configure with YAML, output Markdown. [website](https://doxide.org)
* [doxygen](https://github.com/doxygen/doxygen) - The de facto standard tool for generating documentation from annotated C++ sources. [website](https://www.doxygen.org)
* [doxyrest](https://github.com/vovkos/doxyrest) - A compiler from Doxygen XML to reStructuredText for Sphinx.
* [hdoc](https://github.com/hdoc/hdoc) - The modern documentation tool for C++. [website](https://hdoc.io)
* [Natural Docs](https://github.com/NaturalDocs/NaturalDocs) - Natural Docs is an open source documentation generator for multiple programming languages. [website](https://www.naturaldocs.org)
* [Sphinx](https://github.com/sphinx-doc/sphinx) - Sphinx makes it easy to create intelligent and beautiful documentation. [website](https://www.sphinx-doc.org)

## DSP
*Digital signal processing.*

* [DSPFilters](https://github.com/vinniefalco/DSPFilters) - A collection of useful C++ classes for digital signal processing.
* [FFTW](http://www.fftw.org/) - A C library for computing the DFT in one or more dimensions.
* [iir1](https://github.com/berndporr/iir1) - IIR Realtime C++ filter library.
* [kissfft](https://github.com/mborgerding/kissfft) - A Fast Fourier Transform (FFT) library that tries to Keep it Simple, Stupid.
* [pocketfft](https://github.com/mreineck/pocketfft) - FFT implementation based on FFTPack, but with several improvements.
* [wavelib](https://github.com/rafat/wavelib) - C implementation of 1D and 2D wavelet transforms.

## Email
*Libraries and tools that implement email creation and sending.*

* [eMail](https://github.com/deanproxy/eMail) - Program that will send email via the command line to remote smtp servers or use 'sendmail' internally, and fully interact with GNUPG to encrypt and sign your e-mails.
* [LibEtPan](http://www.etpan.org/) - Mail library providing an efficient network for IMAP, SMTP, POP and NNTP.
* [libquickmail](http://sourceforge.net/projects/libquickmail/) - Library intended to give developers a way to send email from their applications. Supports multiple To/Cc/Bcc recipients and attachments without size limits.
* [mailio](https://github.com/karastojko/mailio) - mailio is a cross platform C++ library for MIME format and SMTP, POP3 and IMAP protocols.

## Embeddable Scripting Languages

* [Duktape](https://github.com/svaarala/duktape) - An embeddable Javascript engine with compact footprint. [website](https://duktape.org)
* [MetaCall](https://github.com/metacall/core) - Cross-platform Polyglot Runtime which supports NodeJS, JavaScript, TypeScript, Python, Ruby, C#, Wasm, Java, Cobol and more.
* [MuJS](https://mujs.com/) - Lightweight Javascript interpreter designed for embedding in other software to extend them with scripting capabilities.
* [quickjs](https://bellard.org/quickjs) - QuickJS is a small and embeddable Javascript engine. It supports the ES2020 specification including modules, asynchronous generators and proxies.
* [WebAssembly Micro Runtime](https://github.com/bytecodealliance/wasm-micro-runtime) - WebAssembly Micro Runtime (WAMR) is a lightweight standalone WebAssembly (Wasm) runtime with small footprint, high performance and highly configurable features for applications cross from embedded, IoT, edge to Trusted Execution Environment (TEE), smart contract, cloud native and so on.

## Financial
*Packages for accounting and finance.*

* [aes256gcm-decrypt](https://github.com/clearhaus/aes256gcm_decrypt) - Decrypt AES256GCM-encrypted data in Apple Pay Payment Tokens.
* [applepay-php](https://github.com/etsy/applepay-php) - A PHP extension that verifies and decrypts Apple Pay payment tokens.
* [Currency-Recognition](https://github.com/carlosmccosta/Currency-Recognition) - This project focus on the detection and recognition of Euro banknotes.
* [credit-card-validator](https://github.com/karancodes/credit-card-validator)
* [GenChipTAN](https://github.com/lennyerik/GenChipTAN) - A generator utility for the chipTAN optical and chipTAN QR codes which are used by certain European banks.
* [Global Transaction Payment Solution](https://github.com/GtpsFinance/Global-Transaction-Payment-Solution) - Global Transaction Payment Solution to become the global official payment solution.
* [JcBank](https://github.com/pliantmeerkat/JcBank) - A surprisingly complex banking system, simulating accounts, transactions, authentication , encryption and payments.
* [Oscar-ISO8583](https://github.com/sabit/Oscar-ISO8583) - Copied from Oscar Sanderson's DL ISO-8583 C Library (v0.4) [https://oscarsanderson.com/iso-8583/](https://oscarsanderson.com/iso-8583/)
* [iso8583_cipher](https://github.com/hbakhshandeh/iso8583_cipher) - MAC and PIN block encryption in ISO-8583.
* [iso8583lib](https://github.com/ecampostrini/iso8583lib) - C++ library to work with Iso8583 messages.
* [ISO-8583](https://github.com/shreyasr89/ISO-8583) - A C++ implementation of the ISO-8583.
* [isomon](https://github.com/castedo/isomon) - C/C++ library for money and ISO 4217 currency codes
* [libcardcheck](https://github.com/rlan/libcardcheck) - A payment/credit card number checker. A C++ library.
* [magspoof](https://github.com/samyk/magspoof) - A portable device that can spoof/emulate any magnetic stripe, credit card or hotel card "wirelessly", even on standard magstripe (non-NFC/RFID) readers. It can disable Chip&PIN and predict AMEX card numbers with 100% accuracy.
* [moneycpp](https://github.com/mariusbancila/moneycpp) - A C++ 17 header-only, cross-platform library for handling monetary values, currencies, rounding and other related features.
* [nfc-frog](https://github.com/cuamckuu/nfc-frog) - Contactless EMV credit card reader.
* [ultimateCreditCard-SDK](https://github.com/DoubangoTelecom/ultimateCreditCard-SDK) - Bank credit card deep layout analysis, fields extraction and recognition/OCR (ScanToPay) using deep learning.
* [UPI Payment QRCode Generator](https://github.com/AgnelSelvan/upi_payment_qrcode_generator) - Generates the QRCode for UPI Payment Flutter.
* [xcard](https://github.com/jkotra/xcard) - Multi-threaded card generator and validator.

## Font
*Libraries for parsing and manipulating font files.*

* [Fontconfig](https://gitlab.freedesktop.org/fontconfig/fontconfig) - Font configuration and customization library. [website](https://www.freedesktop.org/wiki/Software/fontconfig/)
* [FreeType](https://www.freetype.org/) - FreeType is a freely available software library to render fonts.
* [otfcc](https://github.com/caryll/otfcc) - A C library and utility used for parsing and writing OpenType font files.
* [harfbuzz](https://github.com/harfbuzz/harfbuzz) - A text shaping engine.
* [libschrift](https://github.com/tomolt/libschrift) - A lightweight TrueType font rendering library.

## Game Engine

* [Acid](https://github.com/Equilibrium-Games/Acid) - A high speed C++17 Vulkan game engine.
* [Allegro](http://liballeg.org/) - A cross-platform library mainly aimed for video games and multimedia programming.
* [astera][https://github.com/tek256/astera] - C99 Cross Platform 2D Game Library.
* [Atomic Game Engine](https://github.com/AtomicGameEngine/AtomicGameEngine) - A multi-platform 2D and 3D engine with a consistent API in C++, C#, JavaScript, and TypeScript.
* [Axmol Engine](https://github.com/axmolengine/axmol) - A cross-platform game engine for desktop, mobile, and XBOX (UWP), derived from Cocos2d-x-4.0. [website](https://axmol.dev/)
* [Banshee 3D](https://github.com/BearishSun/BansheeEngine) - Modern C++14 game engine with Vulkan support, fully featured editor and C# scripting.
* [Cocos2d-x](http://www.cocos2d-x.org/) - A multi-platform framework for building 2d games, interactive books, demos and other graphical applications.
* [Corange](https://github.com/orangeduck/Corange) - A game engine written in pure C, SDL and OpenGL.
* [crown](https://github.com/dbartolini/crown) - Crown is a general purpose data-driven game engine, written from scratch in orthodox C++ with a minimalistic and data-oriented design philosophy in mind.
* [delta3d](http://sourceforge.net/projects/delta3d/) - A robust simulation platform.
* [EnTT](https://github.com/skypjack/entt) - Gaming meets modern C++.
* [ezEngine](https://github.com/ezEngine/ezEngine) - Is a free, open-source game engine written in C++. Its philosophy is to be modular and flexible, such that it can be adapted to many different use cases. [website](https://ezengine.net/)
* [GamePlay](https://github.com/gameplay3d/GamePlay) - A cross-platform native C++ game framework for creating 2D/3D mobile and desktop games.
* [Godot](https://github.com/godotengine/godot) - A fully featured, open source, MIT licensed, game engine.
* [Grit](https://github.com/grit-engine/grit-engine) - Community project to build a free game engine for implementing open world 3D games.
* [Halley](https://github.com/amzeratul/halley) - A lightweight game engine written in C++14 with a "true" entity-component system.
* [JNGL](https://github.com/jhasse/jngl/) - 2D library for Linux, Windows, macOS, Android, iOS, Xbox, the Nintendo Switch and the Web. [website](https://bixense.com/jngl/)
* [KlayGE](https://github.com/gongminmin/KlayGE) - a cross-platform open source game engine with plugin-based architecture.
* [OpenXRay](https://github.com/OpenXRay/xray-16) - a community-modified X-Ray engine used in S.T.A.L.K.E.R. game series.
* [Oxygine](http://oxygine.org/) - A cross-platform 2D C++ game engine.
* [Panda3D](https://github.com/panda3d/panda3d) - A game engine, a framework for 3D rendering and game development for Python and C++ programs.
* [PixelGameEngine](https://github.com/OneLoneCoder/olcPixelGameEngine) - The official distribution of olcPixelGameEngine, a tool used in javidx9's YouTube videos and projects.
* [Polycode](https://github.com/ivansafrin/Polycode) - A cross-platform framework for creative code in C++ (with Lua bindings).
* [quakeforge](https://github.com/quakeforge/quakeforge) - Actively maintained branch of the original Quake engine code with 20+ years of development.
* [raylib](https://github.com/raysan5/raylib) - A simple and easy-to-use library to enjoy videogames programming. [website](http://www.raylib.com/)
* [Spring](https://github.com/spring/spring) - A powerful free cross-platform RTS game engine.
* [Torque2D](https://github.com/GarageGames/Torque2D) - An open-source and cross-platform C++ engine built for 2D game development.
* [Torque3D](https://github.com/GarageGames/Torque3D) - An open-source C++ engine built for 3D game development.
* [toy engine](https://toyengine.io/) - toy is a thin and modular c++ game engine and offers simple expressive c++ idioms to design full featured 2D or 3D games in fast iterations.
* [Urho3D](https://urho3d.github.io/) - A free lightweight, cross-platform 2D and 3D game engine implemented in C++. Greatly inspired by OGRE and Horde3D.

## GUI
*Graphic User Interface*

* [Boden](https://github.com/AshampooSystems/boden) - Native, mobile, cross-platform GUI Framework.
* [CEGUI](http://cegui.org.uk/) - Flexible, cross-platform GUI library.
* [Clay](https://github.com/nicbarker/clay) - High performance UI layout library in C. [nicbarker.com/clay](https://nicbarker.com/clay)
* [Elements](https://github.com/cycfi/elements) - Lightweight, fine-grained, resolution independent, modular GUI library.
* [Expo](https://github.com/expo/expo) - An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
* [FLTK](http://www.fltk.org/index.php) - Fast, light, cross-platform C++ GUI toolkit.
* [FOX Toolkit](http://fox-toolkit.org) - an open-source, cross-platform widget toolkit.
* [GacUI](https://github.com/vczh-libraries/GacUI) - GPU Accelerated C++ User Interface, with WYSIWYG developing tools, XML supports, built-in data binding and MVVM features.
* [GTK+](http://www.gtk.org/) - A multi-platform toolkit for creating graphical user interfaces.
* [gtkmm](http://www.gtkmm.org/en/) - The official C++ interface for the popular GUI library GTK+.
* [JUCE](https://github.com/juce-framework/JUCE) - JUCE is an open-source cross-platform C++ application framework for desktop and mobile applications, including VST, VST3, AU, AUv3, RTAS and AAX audio plug-ins.
* [imgui](https://github.com/ocornut/imgui) - Immediate Mode Graphical User Interface with minimal dependencies.
* [implot](https://github.com/epezent/implot) - Immediate Mode Plotting widgets for imgui.
* [iup](https://www.tecgraf.puc-rio.br/iup) - Multi-platform toolkit for building graphical user interfaces.
* [libRocket](http://librocket.com/) - libRocket is a C++ HTML/CSS game interface middleware.
* [libui](https://github.com/andlabs/libui) - Simple and portable (but not inflexible) GUI library in C that uses the native GUI technologies of each platform it supports.
* [lvgl](https://github.com/lvgl/lvgl) - Embedded graphics library to create beautiful UIs for any MCU, MPU and display type. [lvgl.io](https://lvgl.io/)
* [MyGUI](https://github.com/MyGUI/mygui) - Fast, flexible and simple GUI.
* [nana](http://nanapro.org/en-us/) - Nana is a cross-platform library for GUI programming in modern C++ style.
* [NanoGui](https://github.com/wjakob/nanogui) - A minimalistic cross-platform widget library for OpenGL 3.x or higher.
* [nuklear](https://github.com/Immediate-Mode-UI/Nuklear) - A single-header ANSI C gui library.
* [PlotJuggler](https://github.com/facontidavide/PlotJuggler) - PlotJuggler is a tool to visualize time series that is fast, powerful and intuitive.
* [QCustomPlot](http://qcustomplot.com/) - Qt plotting widget without further dependencies.
* [qpwgraph](https://github.com/rncbc/qpwgraph) - qpwgraph - A PipeWire Graph Qt GUI Interface.
* [QWidgetDemo](https://github.com/feiyangqingyun/QWidgetDemo) - Some open source demos written in Qt (widgets) are expected to have more than 100.
* [Qwt](http://qwt.sourceforge.net/) - Qt Widgets for Technical Applications.
* [QwtPlot3D](http://qwtplot3d.sourceforge.net/) - A feature-rich Qt/OpenGL-based C++ programming library, providing essentially a bunch of 3D-widgets.
* [revery](https://github.com/revery-ui/revery) - Revery is kind of like super-fast, native code Electron - with bundled React-like/Redux-like libraries and a fast build system - all ready to go!
* [RmlUi](https://github.com/mikke89/RmlUi) - The HTML/CSS User Interface library evolved. Fork of libRocket.
* [Sciter](http://sciter.com/) - Sciter is an embeddable HTML/CSS/scripting engine aimed to be used as an UI layer of modern desktop applications.
* [Slint](https://github.com/slint-ui/slint) -  Lightweight GUI toolkit for desktop and embedded.[website](https://slint-ui.com/)
* [TGUI](https://github.com/texus/TGUI) - Cross-platform modern c++ GUI. [website](https://tgui.eu/)
* [WebUI](https://github.com/webui-dev/webui) - Use any web browser as GUI, with your preferred language in the backend and HTML5 in the frontend, all in a lightweight portable lib. [webui.me](https://webui.me/)
* [wxCharts](https://github.com/wxIshiko/wxCharts) - A library to create charts in wxWidgets applications. [website](https://www.wxishiko.com/wxCharts/)
* [wxWidgets](http://wxwidgets.org/) - A C++ library that lets developers create applications for Windows, Mac OS X, Linux and other platforms with a single code base.
* [Yue](https://github.com/yue/yue) - A library for creating native cross-platform GUI apps.
* [xtd](https://github.com/gammasoft71/xtd) - Modern C++17/20 framework to create console (CLI), forms (GUI like WinForms) and tunit (unit tests like Microsoft Unit Testing Framework) applications on Windows, macOS, Linux, iOS and android.

## Graphics

* [assimp](https://github.com/assimp/assimp) - Open Asset Import Library (assimp) is a cross-platform 3D model import library which aims to provide a common API for different 3D asset file formats. [website](http://www.assimp.org)
* [bgfx](https://github.com/bkaradzic/bgfx) - A cross-platform rendering library.
* [Blend2D](https://github.com/blend2d/blend2d) - 2D vector graphics engine powered by a JIT compiler. [website](https://blend2d.com/)
* [bs::framework](https://github.com/GameFoundry/bsf) - Modern C++14 library for the development of real-time graphical applications.
* [Cairo](http://cairographics.org/) - A 2D graphics library with support for multiple output devices.
* [C-Turtle](https://github.com/walkerje/C-Turtle) - A C++11 header-only turtle graphics library acting as a CImg wrapper.
* [Diligent Engine](https://github.com/DiligentGraphics/DiligentEngine) - A modern cross-platform low-level 3D graphics library.
* [DirectXTK](https://github.com/Microsoft/DirectXTK) - A collection of helper classes for writing DirectX 11.x code in C++.
* [graphene](http://ebassi.github.io/graphene) - Thin layer of graphical data types.
* [GLFW](https://github.com/glfw/glfw) - A simple, cross-platform OpenGL wrangling library.
* [GLFWPP](https://github.com/janekb04/glfwpp) - A thin modern C++17 header-only wrapper for GLFW.
* [Harfang 3D](https://github.com/harfang3d/harfang3d) - 3D visualization library usable in C++, Python, Lua and Go. Based on BGFX. [website](https://www.harfang3d.com)
* [heman](https://github.com/prideout/heman) - Tiny library of image utilities dealing with height maps, normal maps, distance fields and the like.
* [herebedragons](https://github.com/kosua20/herebedragons) - A basic 3D scene implemented with various engines, frameworks or APIs. [website](http://simonrodriguez.fr/dragon/)
* [Horde3D](https://github.com/horde3d/Horde3D) - A small 3D rendering and animation engine.
* [Ion](https://github.com/google/ion) - A small and efficient set of libraries for building cross-platform client or server applications that use 3D graphics. [website](https://google.github.io/ion/)
* [Irrlicht](http://irrlicht.sourceforge.net/) - A high performance realtime 3D engine written in C++.
* [libigl](https://github.com/libigl/libigl) - Simple C++ geometry processing library.
* [libcaca](https://github.com/cacalabs/libcaca) - ASCII renderer for terminal-based interfaces.
* [LLGL](https://github.com/LukasBanana/LLGL) - Low Level Graphics Library (LLGL) is a thin abstraction layer for the modern graphics APIs.
* [LunaSVG](https://github.com/sammycage/lunasvg) - a standalone SVG rendering library in C++.
* [magnum](https://github.com/mosra/magnum) - Lightweight and modular C++11/C++14 graphics middleware for games and data visualization. [website](http://magnum.graphics)
* [micro-gl](https://github.com/micro-gl/micro-gl) - Realtime, Embeddable, Headers Only C++11 CPU vector graphics. no STD lib, no FPU and no GPU required. [website](https://micro-gl.github.io/docs/microgl)
* [NanoVG](https://github.com/memononen/nanovg) - Antialiased 2D vector drawing library on top of OpenGL for UI and visualizations.
* [Ogre 3D](http://www.ogre3d.org/) :zap: - A scene-oriented, real-time, flexible 3D rendering engine (as opposed to a game engine) written in C++.
* [OpenSceneGraph](http://www.openscenegraph.org/) - An open source high performance 3D graphics toolkit.
* [OpenSubdiv](https://github.com/PixarAnimationStudios/OpenSubdiv) - Pixar's library for evaluating and rendering subdivision surfaces on CPU and GPU.
* [OpenVDB](http://www.openvdb.org/) - Library and tools for storing, editing, and rendering volumetric datasets.
* [Panda3D](http://www.panda3d.org/) - A framework for 3D rendering and game development for Python and C++.
* [Partio](https://github.com/wdas/partio) - Library for wrangling particle data, with support for most common file formats.
* [Perspective](https://github.com/finos/perspective) - A data visualization and analytics component, especially well-suited for large and/or streaming datasets. [perspective.finos.org](https://perspective.finos.org/)
* [PlutoSVG](https://github.com/sammycage/plutosvg) - Tiny SVG rendering library in C.
* [PlutoVG](https://github.com/sammycage/plutovg) - A standalone 2D vector graphics library in C.
* [Skia](https://github.com/google/skia) - A complete 2D graphic library for drawing Text, Geometries, and Images. [webpage](https://skia.org/)
* [ThorVG](https://github.com/thorvg/thorvg) - a platform-independent portable library that allows for drawing vector-based scenes and animations, including SVG and Lottie. [website](https://www.thorvg.org/)
* [TinySpline](https://github.com/msteinbeck/tinyspline) - A small, yet powerful ANSI C library for interpolating, transforming, and querying arbitrary NURBS, B-Splines, and Bézier curves.
* [urho3d](https://github.com/urho3d/Urho3D) - Cross-platform rendering and game engine.
* [Yocto/GL](https://github.com/xelatihy/yocto-gl) - Tiny C++ Libraries for Data-Driven Physically-based Graphics.

## Image Processing

* [Boost.GIL](https://github.com/boostorg/gil) - Generic Image Library.
* [BitmapPlusPLus](https://github.com/baderouaich/BitmapPlusPlus) - Simple and Fast header only Bitmap C++ library.
* [CImg](http://cimg.eu/) - A small, open source, C++ toolkit for image processing.
* [CxImage](https://www.codeproject.com/Articles/1300/CxImage) - An image processing and conversion library to load, save, display, transform BMP, JPEG, GIF, PNG, TIFF, MNG, ICO, PCX, TGA, WMF, WBMP, JBG, J2K images.
* [FaceLivenessDetection-SDK](https://github.com/DoubangoTelecom/FaceLivenessDetection-SDK) - 3D Passive Face Liveness Detection (Anti-Spoofing). A single image is needed to compute liveness score. 99,67% accuracy on our dataset and perfect scores on multiple public datasets (NUAA, CASIA FASD, MSU...).
* [FreeImage](http://freeimage.sourceforge.net/) - An open source library that supports popular graphics image formats and others as needed by today's multimedia applications. [GPL2 or GPL3]
* [GD](https://github.com/libgd/libgd) - GD Graphics Library, famously used in PHP for image loading/manipulation & thumbnail generation. [website](http://libgd.github.io/)
* [Darktable](https://github.com/darktable-org/darktable) - darktable is an open source photography workflow application and raw developer [website](https://www.darktable.org/)
* [DCMTK](http://dicom.offis.de/dcmtk.php.en) - DICOM Toolkit.
* [FlyCV](https://github.com/PaddlePaddle/FlyCV) - FlyCV is a high-performance library for processing computer visual tasks.
* [fpng](https://github.com/richgel999/fpng) - Super fast C++ .PNG writer/reader.
* [GDCM](http://gdcm.sourceforge.net/wiki/index.php/Main_Page) - Grassroots DICOM library.
* [giflib](https://sourceforge.net/projects/giflib) - Library for reading and writing gif images.
* [Jpegli][https://github.com/google/jpegli] - an improved JPEG encoder and decoder implementation.
* [ITK](http://www.itk.org/) - An open-source, cross-platform system for image analysis.
* [Leptonica](https://github.com/DanBloomberg/leptonica) - Leptonica is an open source library containing software that is broadly useful for image processing and image analysis applications. [website](http://leptonica.org/index.html)
* [libfacedetection](https://github.com/ShiqiYu/libfacedetection) - Open source library for face detection in images. The face detection speed can reach 1500FPS.
* [libavif](https://github.com/AOMediaCodec/libavif) - Library for encoding and decoding .avif files.
* [libjpeg-turbo](https://github.com/libjpeg-turbo/libjpeg-turbo) - A JPEG image codec that uses SIMD instructions to accelerate baseline JPEG encoding and decoding. [website](https://libjpeg-turbo.org/)
* [libjxl](https://github.com/libjxl/libjxl) - JPEG XL image format reference implementation.
* [libpag](https://github.com/Tencent/libpag) - libpag is a real-time rendering library for PAG (Portable Animated Graphics) files that renders both vector-based and raster-based animations across most platforms, such as iOS, Android, macOS, Windows, Linux, and Web.
* [libpng](http://www.libpng.org) - Official PNG reference library.
* [libpng](https://github.com/pnggroup/libpng) - the reference library for use in applications that read, create, and manipulate PNG (Portable Network Graphics) raster image files. [website](https://libpng.sourceforge.io/)
* [libspng](https://github.com/randy408/libspng) - Simple, modern libpng alternative. [website](https://libspng.org/)
* [libRSVG][https://wiki.gnome.org/action/show/Projects/LibRsvg?action=show&redirect=LibRsvg] - Library to render SVG files using Cairo.
* [libsixel](https://github.com/saitoha/libsixel) - Library implementing the SIXEL protocol, allowing beautiful graphics in your terminal.
* [libspng](https://libspng.org) - A simpler interface for reading and writing PNG files.
* [libvips](https://github.com/jcupitt/libvips) - A fast image processing library with low memory needs. [website](http://www.vips.ecs.soton.ac.uk/)
* [libxmi](https://gnu.org/software/libxmi) - Function library for rasterizing 2D vector graphics.
* [lightmapper](https://github.com/ands/lightmapper) - Single-file library for lightmap baking, using an existing OpenGL renderer.
* [little CMS][www.littlecms.com] - A Color Management System. It provides fast transforms between ICC profiles.
* [LodePNG](https://github.com/lvandeve/lodepng) - PNG encoder and decoder in C and C++.
* [Magick++](http://www.imagemagick.org/script/api.php) - ImageMagick program interfaces for C++.
* [MagickWnd](http://www.imagemagick.org/script/api.php) - ImageMagick program interfaces for C.
* [mozjpeg](https://github.com/mozilla/mozjpeg) - Improved JPEG encoder.
* [msf_gif](https://github.com/notnullnotvoid/msf_gif) - Single-header animated GIF exporter, suitable for recording gifs in realtime.
* [OpenCV](http://opencv.org/) - Open source computer vision.
* [OpenEXR](http://www.openexr.com/) - Cross-platform library for high dynamic range imaging.
* [OpenJPEG](https://github.com/uclouvain/openjpeg) - an open-source JPEG 2000 codec written in C language.
* [OpenImageIO](https://github.com/OpenImageIO/oiio) - Powerful image and texture wrangling library with support for a wide number of common lossy and RAW formats.
* [QOI](https://github.com/phoboslab/qoi) - The “Quite OK Image” format for fast, lossless image compression.
* [SAIL](https://github.com/smoked-herring/sail) - Small and fast image decoding library for humans (not for machines)
* [Simd](https://github.com/ermig1979/Simd) - C++ image processing library with using of SIMD: SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, AVX-512, VMX(Altivec) and VSX(Power7), NEON for ARM.
* [stb-image](https://github.com/nothings/stb/blob/master/stb_image.h) - STB single-header image loading library.
* [tesseract-ocr](https://github.com/tesseract-ocr) - An OCR engine.
* [TinyDNG](https://github.com/syoyo/tinydng) - Header-only Tiny DNG/TIFF loader and writer in C++.
* [TinyEXIF](https://github.com/cdcseacave/TinyEXIF) - Tiny ISO-compliant C++ EXIF and XMP parsing library for JPEG.
* [TinyTIFF](https://github.com/jkriege2/TinyTIFF) - lightweight TIFF reader/writer library.
* [ultimateCreditCard-SDK](https://github.com/DoubangoTelecom/ultimateCreditCard-SDK) - Bank credit card deep layout analysis, fields extraction and recognition/OCR (ScanToPay) using deep learning. [website](https://www.doubango.org/webapps/credit-card-ocr/)
* [Video++](https://github.com/matt-42/vpp) - A C++14 high performance video and image processing library.
* [VIGRA](https://github.com/ukoethe/vigra) - A generic C++ computer vision library for image analysis.
* [VTK](http://www.vtk.org/) - Open-source, freely available software system for 3D computer graphics, image processing and visualization.

## Industrial automation

* [open62541](https://github.com/open62541/open62541) - Open source implementation of OPC UA (OPC Unified Architecture) aka IEC 62541 licensed under Mozilla Public License v2.0
* [QSimpleScada](https://github.com/IndeemaSoftware/QSimpleScada) - Qt based simple SCADA framework, with dashboard, static and dynamic components

## Internationalization

* [gettext](http://www.gnu.org/software/gettext/) - GNU 'gettext'.
* [IBM ICU](http://site.icu-project.org/) - A set of C/C++ and Java libraries providing Unicode and Globalization support.
* [libiconv](http://www.gnu.org/software/libiconv/) - An encoding conversion library between different character encodings.
* [simdutf](https://github.com/simdutf/simdutf) - Unicode routines (UTF8, UTF16, UTF32): billions of characters per second using SSE2, AVX2, NEON, AVX-512.
* [uni-algo](https://github.com/uni-algo/uni-algo) - Unicode Algorithms Implementation for C/C++.
* [utf8.h](https://github.com/sheredom/utf8.h) - Single header utf8 string functions for C and C++.
* [utf8proc](https://github.com/JuliaStrings/utf8proc) - A clean C library for processing UTF-8 Unicode data.

## Inter-process communication

* [Apache Thrift](https://thrift.apache.org/) - Efficient cross-language IPC/RPC, works between C++, Java, Python, PHP, C#, and many more other languages. Originally developed by Facebook.
* [Boost.Interprocess](https://github.com/boostorg/interprocess) - Header-only Boost library that supports kernel-level shared memory and memory-mapped files, with in-built synchronization mechanisms (semaphores, mutexes, and more).
* [bRPC](https://github.com/apache/brpc) - brpc is an Industrial-grade RPC framework using C++ Language, which is often used in high performance system such as Search, Storage, Machine learning, Advertisement, Recommendation etc. "brpc" means "better RPC".
* [Cap'n Proto](http://kentonv.github.io/capnproto/) - Fast data interchange format and capability-based RPC system.
* [cppzmq](https://github.com/zeromq/cppzmq) - Header-only C++ binding for libzmq.
* [eCAL](https://github.com/eclipse-ecal/ecal) - A high performance publish-subscribe, client-server cross-plattform middleware. [ecal.io](https://ecal.io/)
* [FifoIPCBenchmark](https://github.com/AndreaBaretta/FifoIPCBenchmark) - A test to benchmark the latency of interprocess communication using a shared-memory FIFO object.
* [FleXR_ShmQ](https://github.com/gt-flexr/FleXR_ShmQ) - A simple and lightweight shared memory queue with some useful features.
* [Flow-IPC](https://github.com/Flow-IPC/ipc) - Modern C++ toolkit for high-speed inter-process communication (IPC).
* [gloo](https://github.com/facebookincubator/gloo) - Gloo is a collective communications library. It comes with a number of collective algorithms useful for machine learning applications. These include a barrier, broadcast, and allreduce.
* [gRPC](https://github.com/grpc/grpc) - A high performance, open source, general-purpose RPC framework.
* [grpcxx](https://github.com/uatuko/grpcxx) - Blazing fast gRPC server (C++).
* [Ice](https://github.com/zeroc-ice/ice) - Comprehensive RPC framework with support for C++, C#, Java, JavaScript, Python and more. 
* [iceoryx](https://github.com/eclipse-iceoryx/iceoryx) - True zero-copy inter-process communication framework for safety critical systems with bindings for C, Rust. Runs on Linux, QNX, Windows, Mac OS, FreeBSD. [website](https://iceoryx.io/)
* [incubator-brpc](https://github.com/apache/incubator-brpc) - Industrial-grade RPC framework used throughout Baidu, with 1,000,000+ instances and thousands kinds of services. "brpc" means "better RPC".
* [IPC](https://github.com/microsoft/IPC) - IPC is a C++ library that provides inter-process communication using shared memory on Windows. A .NET wrapper is available which allows interaction with C++ as well.
* [IPC-FDStream](https://github.com/eldruin/IPC-FDStream) - Inter-Process Communication with C++ STL-Like streams through file descriptors.
* [InterprocessMemPool](https://github.com/keithrausch/InterprocessMemPool) - c++ library for interprocess memory pools, communication, and automatic network device discovery. lightweight DDS alternative.
* [lib-datalane](https://github.com/stream-labs/lib-datalane) - An incredibly fast IPC solution for C and C++.
* [libjson-rpc-cpp](https://github.com/cinemast/libjson-rpc-cpp) - JSON-RPC framework for C++ servers and clients.
* [mage](https://github.com/domfarolino/mage) - A simple cross-platform1 interprocess communication (IPC) library written in C++. Mage is heavily inspired by [Chromium's Mojo IPC](https://chromium.googlesource.com/chromium/src/+/master/mojo/README.md) library
* [nanomsg](https://github.com/nanomsg/nanomsg) - A simple high-performance implementation of several "scalability protocols".
* [nng](https://github.com/nanomsg/nng) - nanomsg-next-generation, a light-weight brokerless messaging library.
* [packio](https://github.com/qchateau/packio) - An asynchronous msgpack-RPC and JSON-RPC library built on top of Boost.Asio.
* [Real-Time-Messaging](https://github.com/schevenin/Real-Time-Messaging) - Multi-threaded ridesharing and real-time messaging simulation with broker.
* [reproc](https://github.com/DaanDeMeyer/reproc) - reproc (Redirected Process) is a cross-platform C/C++ library that simplifies starting, stopping and communicating with external programs. The main use case is executing command line applications directly from C or C++ code and retrieving their output.
* [rest_rpc](https://github.com/qicosmos/rest_rpc) - modern C++(C++11), simple, easy to use rpc framework.
* [rpclib](https://github.com/rpclib/rpclib) - A modern C++ msgpack-RPC server and client library.
* [shadesmar](https://github.com/Squadrick/shadesmar) - Fast C++ IPC using shared memory.
* [simple-rpc-cpp](https://github.com/pearu/simple-rpc-cpp) - A simple RPC wrapper generator to C/C++ functions.
* [smf](https://github.com/smfrpc/smf) - Fastest RPC in the west.
* [SRPC](https://github.com/sogou/srpc) - A lightweight RPC system that supports multiple protocols and OpenTelemetry.
* [tRPC-Cpp](https://github.com/trpc-group/trpc-cpp) - A pluggable, high-performance RPC framework written in cpp.
* [ujrpc](https://github.com/unum-cloud/ujrpc) - Up to 100x Faster FastAPI. JSON-RPC with io_uring, SIMDJSON, and pure CPython bindings.
* [WAMP](http://wamp.ws/) - Provides RPC and pub/sub messaging patterns. (various implementations, various languages)
* [xmlrpc-c](http://xmlrpc-c.sourceforge.net/) - A lightweight RPC library based on XML and HTTP.

### Shared Memory

* [cpp-ipc](https://github.com/mutouyun/cpp-ipc) - C++ IPC Library: A high-performance inter-process communication using shared memory on Linux/Windows.
* [shm_container](https://github.com/mikewei/shm_container) - A collection of shared memory containers for high performance server.
* [shm_ring_buffer](https://github.com/bo-yang/shm_ring_buffer) - Shared-memory based ring buffer.
* [tcpshm](https://github.com/MengRao/tcpshm) - A connection-oriented persistent message queue framework based on TCP or SHM(shared memory).

## JSON

* [ArduinoJson](https://github.com/bblanchon/ArduinoJson) - JSON library for Arduino and embedded C++. Simple and efficient.
* [Boost.PropertyTree](https://github.com/boostorg/property_tree) - A property tree parser/generator that can be used to parse XML/JSON/INI/Info files.
* [cJSON](https://github.com/DaveGamble/cJSON) - Ultralightweight JSON parser in ANSI C.
* [DAW JSON Link](https://github.com/beached/daw_json_link) - Fast, convenient JSON serialization and parsing in C++.
* [fastgron](https://github.com/adamritter/fastgron) - High-performance JSON to GRON (greppable, flattened JSON) converter.
* [frozen](https://github.com/cesanta/frozen) - JSON parser and generator for C/C++.
* [Glaze](https://github.com/stephenberry/glaze) - Extremely fast, in memory, JSON and interface library for modern C++.
* [Jansson](https://github.com/akheron/jansson) - C library for encoding, decoding and manipulating JSON data.
* [jbson](https://github.com/chrismanning/jbson) - jbson is a library for building & iterating BSON data, and JSON documents in C++14.
* [JeayeSON](https://github.com/jeaye/jeayeson) - A very sane (header only) C++ JSON library.
* [Jsmn](https://github.com/zserge/jsmn) - A minimalistic JSON parser in C.
* [json](https://github.com/boostorg/json) - A C++11 library for parsing and serializing JSON to and from a DOM container in memory.
* [json](https://github.com/nlohmann/json) - JSON for Modern C++.
* [JSON++](https://github.com/hjiang/jsonxx) - A JSON parser in C++.
* [json.cpp](https://github.com/jart/json.cpp) - a baroque JSON parsing / serialization library for C++.
* [json.h](https://github.com/sheredom/json.h) - A simple one header/one source solution to parsing JSON in C and C++.
* [json-c](https://github.com/json-c/json-c) - A JSON implementation in C.
* [jsoncons](https://github.com/danielaparker/jsoncons) - A C++ header-only library for JSON and JSON-like binary formats with JSONPointer, JSONPatch, JSONPath and JMESPath.
* [JsonCpp](https://github.com/open-source-parsers/jsoncpp) - A C++ library for interacting with JSON.
* [Jsonifier](https://github.com/RealTimeChris/Jsonifier) - A few classes for parsing and serializing objects from/into JSON - very rapidly.
* [json-build](https://github.com/lcsmuller/json-build) - C89 tiny zero-allocation JSON serializer.
* [json-parser](https://github.com/udp/json-parser) - Very low footprint JSON parser written in portable ANSI C.
* [json_dto](https://github.com/Stiffstream/json_dto) - A small header-only library for converting data between json representation and c++ structs.
* [json_struct](https://github.com/jorgen/json_struct) - json_struct is a single header only C++ library for parsing JSON directly to C++ structs and vice versa.
* [jsonParse](https://github.com/liufeigit/jsonParse) - A simple JSON parser in ANSI C.
* [json11](https://github.com/dropbox/json11) - A tiny JSON library for C++11.
* [json-voorhees](https://github.com/tgockel/json-voorhees) - JSON library for C++. Support for C++11. No dependencies, fast and dev-friendly.
* [JSON Toolkit](https://github.com/sourcemeta/jsontoolkit) - JSON, JSON Pointer, JSON Schema, and JSONL library for C++20.
* [jute](https://github.com/amir-s/jute) - Very simple C++ JSON Parser.
* [libjson](https://github.com/vincenthz/libjson) - A JSON parser and printer library in C. easy to integrate with any model.
* [libjson](http://sourceforge.net/projects/libjson/) - Lightweight JSON library. [?]
* [LIBUCL](https://github.com/vstakhov/libucl) - Universal configuration library parser
* [PicoJSON](https://github.com/kazuho/picojson) - A header-file-only, JSON parser serializer in C++.
* [qt-json](https://github.com/gaudecker/qt-json) - A simple class for parsing JSON data into a QVariant hierarchy and vice versa.
* [QJson](https://github.com/flavio/qjson) - A qt-based library that maps JSON data to QVariant objects.
* [qajson4c](https://github.com/DeHecht/qajson4c) - A simple json library written in C. Optimized for low memory consumption and for good usability without dynamic memory allocation.
* [RapidJSON](https://github.com/Tencent/rapidjson) - A fast JSON parser/generator for C++ with both SAX/DOM style API.
* [sajson](https://github.com/chadaustin/sajson) - Lightweight, extremely high-performance JSON parser for C++11.
* [simdjson](https://github.com/simdjson/simdjson) - Parsing gigabytes of JSON per second : used by Facebook/Meta Velox, WatermelonDB, Apache Doris, StarRocks.
* [subjson](https://github.com/couchbase/subjson) - High performance JSON manipulation library.
* [Sonic-Cpp](https://github.com/bytedance/sonic-cpp) - A fast JSON serializing & deserializing library, accelerated by SIMD.
* [taoJSON](https://github.com/taocpp/json) - Zero-dependency C++ header-only JSON library.
* [ujson](https://bitbucket.org/awangk/ujson) - µjson is a a small, C++11, UTF-8, JSON library.
* [UltraJSON](https://github.com/ultrajson/ultrajson) - Ultra fast JSON decoder and encoder written in C.
* [YAJL](https://github.com/lloyd/yajl) - A fast streaming JSON parsing library in C.
* [json-c](https://github.com/json-c/json-c/wiki) - Library for working with JSON.
* [yyjson](https://github.com/ibireme/yyjson) - A high performance JSON library written in ANSI C.
* [parson](https://github.com/kgabis/parson) - Lightweight JSON library written in C.
* [WJElement](https://github.com/netmail-open/wjelement/wiki) - Advanced JSON manipulation library, with support for JSON Schema.

## Logging

* [aixlog](https://github.com/badaix/aixlog) - Header-only C++ logging library.
* [binary_log](https://github.com/p-ranav/binary_log) - Fast binary logger for C++.
* [binlog](https://github.com/morganstanley/binlog) - A high performance C++ log library, producing structured binary logs.
* [Blackhole](https://github.com/3Hren/blackhole) - Attribute-based logging framework, which is designed to be fast, modular and highly customizable.
* [Boost.Log](https://github.com/boostorg/log) - Designed to be very modular and extensible.
* [BqLog](https://github.com/Tencent/BqLog) -  A lightweight, high-performance logging system used in projects such as "Honor of Kings".
* [Chucho](https://github.com/mexicowilly/Chucho) - Chucho is a C++14 logging library that supports hierarchical loggers and many different output destinations.
* [CLP](https://github.com/y-scope/clp) - Compressed Log Processor (CLP) is a free tool capable of compressing text logs and searching the compressed logs without decompression.
* [clutchlog](https://github.com/nojhan/clutchlog) - C++ "spatial" logging system which targets versatile, (de)clutchable, _debugging_, in a single header.
* [CppLogging](https://github.com/chronoxor/CppLogging) - Ultra fast and low latency C++ Logging library with flexible configuration and high throughput in multithreaded environment.
* [data_tamer](https://github.com/PickNikRobotics/data_tamer) - C++ library for Fearless Timeseries Logging.
* [Easylogging++](https://github.com/abumq/easyloggingpp) - Extremely light-weight high performance logging library for C++11 (or higher) applications. [website](https://muflihun.github.io/easyloggingpp)
* [ErrLib](https://github.com/MSDN-WhiteKnight/ErrLib) - C/C++ exception handling and logging library.
* [fmtlog](https://github.com/MengRao/fmtlog) - A fmtlib-style logging library with latency in nanoseconds.
* [Fluent Bit](https://github.com/fluent/fluent-bit) - Fast Log Processor and Forwarder for Linux, Embedded Linux, MacOS and BSD family operating systems. It's part of the Fluentd Ecosystem and a CNCF sub-project.
	* [cmetrics](https://github.com/fluent/cmetrics) - A standalone library to create and manipulate metrics in C.
	* [fluentbit-examples](https://github.com/newrelic/fluentbit-examples) - Example Configurations for Fluent Bit.
* [G3log](https://github.com/KjellKod/g3log) - Asynchronous logger with Dynamic Sinks.
* [glog](https://github.com/google/glog) - C++ implementation of the Google logging module.
* [haclog](https://github.com/MuggleWei/haclog) - An extremely fast plain C logging library.
* [iLogtail](https://github.com/alibaba/ilogtail) - Fast and Lightweight Observability Data Collector.
* [Log4cpp](http://log4cpp.sourceforge.net/) - A library of C++ classes for flexible logging to files, syslog, IDSA and other destinations.
* [Log4CPP](https://github.com/Toni-Graphics/Log4CPP) - A simple one header cpp logging libary.
* [log4cplus](https://github.com/log4cplus/log4cplus) - A simple to use C++ logging API providing thread-safe, flexible, and arbitrarily granular control over log management and configuration.
* [Log4Qt](https://github.com/MEONMedical/Log4Qt) - Log4Qt - Logging for the Qt cross-platform application framework.
* [Logan](https://github.com/Meituan-Dianping/Logan) - Logan is a lightweight case logging system based on mobile platform.
* [log.c](https://github.com/rxi/log.c) - A simple logging library implemented in C99.
* [logging-log4cxx](https://github.com/apache/logging-log4cxx) - Apache Log4cxx is a C++ port of Apache Log4j.
* [logix](https://github.com/Naguales/logix) - Logix is a lightweight, thread-safe logging library designed for C++11.
* [loguru](https://github.com/emilk/loguru) - A lightweight C++ logging library.
* [logpp](https://github.com/oktal/logpp) - A modern, fast, structured logging framework in C++.
* [lwlog](https://github.com/ChristianPanov/lwlog) - Very fast synchronous and asynchronous C++17 logging library.
* [NanoLog](https://github.com/PlatformLab/NanoLog) - Nanolog is an extremely performant nanosecond scale logging system for C++ that exposes a simple printf-like API.
* [NanoLog](https://github.com/Iyengar111/NanoLog) - Low Latency C++11 Logging Library.
* [perr](https://github.com/az5112/perr) - A replacement for std::cerr that prints source points and STL containers.
* [plog](https://github.com/SergiusTheBest/plog) - Portable and simple log for C++ in less than 1000 lines of code.
* [reckless](https://github.com/mattiasflodin/reckless) - Low-latency, high-throughput, asynchronous logging library for C++.
* [Serenity_Logger](https://github.com/USAFrenzy/Serenity_Logger) - Yet another Fast and Efficient logging framework. The goal is to be nanosecond level fast with extensibility (inspired by loggers such as spdlog, nanolog, and fmtlog and heavily influenced by the formatting used in fmtlib and <format>).
* [slog](https://github.com/sashamakarenko/slog) - Structured C++ Logger with optimization opportunities for latency and/or disk.
* [struCtured-logger](https://github.com/briandowns/struCtured-logger) - A simple JSON structured logger for C applications.
* [stumpless](https://github.com/goatshriek/stumpless) - C logging library built for high performance and a rich feature set.
* [spdlog](https://github.com/gabime/spdlog) - Super fast, header only, C++ logging library.
	* [examples-spdlog](https://github.com/gusenov/examples-spdlog) - Примеры логирования с помощью C++ библиотеки spdlog.
	* [spdlog_setup](https://github.com/guangie88/spdlog_setup) - spdlog setup initialization via file configuration for convenience.
	* [spdlog-json-config](https://github.com/MuhaoSZ/spdlog-json-config) - Header-only spdlog config library which use json file as a configuration file.
	* [spdlog-mongodb-sink](https://github.com/mguludag/spdlog-mongodb-sink) - mongodb sink for spdlog library.
	* [spdlog-sinks](https://github.com/wangdiqi/spdlog-sinks) - Simple rsyslog sink for spdlog, send log message to rsyslog by udp.
	* [spdlog-sinks](https://github.com/julio77it/spdlog-sinks)
* [TANK](https://github.com/phaistos-networks/TANK) - A very high performance distributed log service.
* [templog](http://www.templog.org/) - A very small and lightweight C++ library which you can use to add logging to your C++ applications.
* [timemory](https://github.com/NERSC/timemory) - Modular C++ Toolkit for Performance Analysis and Logging. Profiling API and Tools for C, C++, CUDA, Fortran, and Python. The C++ template API is essentially a framework to creating tools: it is designed to provide a unifying interface for recording various performance measurements alongside data logging and interfaces to other tools.
* [tracetool](https://github.com/froglogic/tracetool) - An efficient and configurable logging framework for C++.
* [P7Baical](http://baical.net/p7.html) - An open source and cross-platform library for high-speed sending telemetry & trace data  with minimal usage of CPU and memory.
* [Quill](https://github.com/odygrd/quill) - Asynchronous cross platform low latency logging library.
* [xtr](https://github.com/choll/xtr) - A Fast and Convenient C++ Logging Library for Low-latency or Real-time Environments.
* [zlog](http://hardysimpson.github.io/zlog) - Reliable, pure C logging library.

## Machine Learning

* [Caffe](https://github.com/BVLC/caffe) - A fast framework for neural networks.
* [catboost](https://github.com/catboost/catboost) - A fast, scalable, high performance Gradient Boosting on Decision Trees library.
* [CCV](https://github.com/liuliu/ccv) - C-based/Cached/Core Computer Vision Library, A Modern Computer Vision Library.
* [cuML](https://github.com/rapidsai/cuml) - cuML - RAPIDS Machine Learning Library.
* [FAISS](https://github.com/facebookresearch/faiss) - A library for efficient similarity search and clustering of dense vectors.
* [Fido](https://github.com/FidoProject/Fido) - A highly-modular C++ machine learning library for embedded electronics and robotics.
* [flashlight](https://github.com/facebookresearch/flashlight) - A fast, flexible machine learning library from Facebook AI Research written entirely in C++ and based on the ArrayFire tensor library.
* [ggml](https://github.com/ggerganov/ggml) - Tensor library for machine learning with 16-bit and 4-bit quantization support.
* [liboai](https://github.com/D7EAD/liboai) - liboai is a simple, unofficial C++17 library for the OpenAI API. It allows developers to access OpenAI endpoints through a simple collection of methods and classes.
* [libsvm](https://github.com/cjlin1/libsvm) - A simple, easy-to-use, efficient library for Support Vector Machines.
* [LightGBM](https://github.com/microsoft/LightGBM) - A fast, distributed, high performance gradient boosting (GBT, GBDT, GBRT, GBM or MART) framework based on decision tree algorithms, used for ranking, classification and many other machine learning tasks.
* [MeTA](https://github.com/meta-toolkit/meta) - A modern C++ data sciences toolkit.
* [Minerva](https://github.com/dmlc/minerva) - A fast and flexible system for deep learning.
* [m2cgen](https://github.com/BayesWitnesses/m2cgen) - A CLI tool to transpile trained classic ML models into a native C code with zero dependencies.
* [mlpack](https://github.com/mlpack/mlpack) - A scalable c++ machine learning library.
* [ncnn](https://github.com/Tencent/ncnn) - A high-performance neural network inference computing framework optimized for mobile platforms.
* [oneDAL](https://github.com/oneapi-src/oneDAL) - A powerful machine learning library that helps speed up big data analysis.
* [ONNX runtime](https://github.com/microsoft/onnxruntime) - C and C++ library for training and inference ONNX models. ONNX is a standard that AI models can be converted into, regardless of the library they are trained with. [website](https://onnxruntime.ai/)
* [OpenVINO](https://github.com/openvinotoolkit/openvino) - OpenVINO is an open-source toolkit for optimizing and deploying AI inference.
* [Paddle](https://github.com/PaddlePaddle/Paddle) - PArallel Distributed Deep LEarning: Machine Learning Framework from Industrial Practice. [paddlepaddle.org](http://www.paddlepaddle.org/)
* [Recommender](https://github.com/GHamrouni/Recommender) - C library for product recommendations/suggestions using collaborative filtering (CF).
* [RNNLIB](https://github.com/szcom/rnnlib) - RNNLIB is a recurrent neural network library for sequence learning problems.
* [SHOGUN](https://github.com/shogun-toolbox/shogun) - The Shogun Machine Learning Toolbox.
* [USearch](https://github.com/unum-cloud/usearch) - Fast search and clustering library for vectors and strings.
* [sofia-ml](https://code.google.com/p/sofia-ml/) - The suite of fast incremental algorithms for machine learning.
* [VLFeat](https://github.com/vlfeat/vlfeat) - The VLFeat open source library implements popular computer vision algorithms specialising in image understanding and local featurexs extraction and matching.
* [whisper.cpp](https://github.com/ggerganov/whisper.cpp) - High-performance inference of [OpenAI's Whisper](https://github.com/openai/whisper) automatic speech recognition (ASR) model
* [xgboost](https://github.com/dmlc/xgboost) - Scalable, Portable and Distributed Gradient Boosting (GBDT, GBRT or GBM) Library, for Python, R, Java, Scala, C++ and more. Runs on single machine, Hadoop, Spark, Flink and DataFlow.

## Math

* [Apophenia](https://github.com/b-k/apophenia) - A C library for statistical and scientific computing.
* [Armadillo](https://gitlab.com/conradsnicta/armadillo-code) - Fast C++ library for linear algebra & scientific computing.
* [autodiff](https://github.com/autodiff/autodiff) - A modern, fast and expressive C++ library for automatic differentiation.
* [blaze](https://bitbucket.org/blaze-lib/blaze) - high-performance C++ math library for dense and sparse arithmetic.
* [Boost.Multiprecision](https://github.com/boostorg/multiprecision) - provides higher-range/precision integer, rational and floating-point types in C++, header-only or with GMP/MPFR/LibTomMath backends.
* [ceres-solver](http://ceres-solver.org/) - C++ library for modeling and solving large complicated nonlinear least squares problems from google.
* [CGAL](https://github.com/CGAL/cgal) - Collection of efficient and reliable geometric algorithms.
* [cml](http://cmldev.net/) - free C++ math library for games and graphics.
* [Decimal data type](https://github.com/vpiotr/decimal_for_cpp) - Decimal data type support, for COBOL-like fixed-point operations on currency/money values.
* [DirectXMath](https://github.com/microsoft/DirectXMath) - An all inline SIMD C++ linear algebra library for use in games and graphics apps.
* [Dlib](https://github.com/davisking/dlib) - A modern C++11 machine learning, computer vision, numerical optimization, and deep learning toolkit. [website](http://dlib.net/)
* [Eigen](http://eigen.tuxfamily.org/) - A high-level C++ library of template headers for linear algebra, matrix and vector operations, numerical solvers and related algorithms.
* [ExprTK](http://www.partow.net/programming/exprtk/) - The C++ Mathematical Expression Toolkit Library (ExprTk) is a simple to use, easy to integrate and extremely efficient run-time mathematical expression parser and evaluation engine.
* [Geometric Tools](https://www.geometrictools.com) - C++ library for computing in the fields of mathematics, graphics, image analysis and physics. [website](https://www.geometrictools.com)
* [GLM](https://github.com/g-truc/glm) - Header-only C++ math library that matches and inter-operates with OpenGL's GLSL math. [website](https://glm.g-truc.net/)
* [GMTL](http://ggt.sourceforge.net/) - Graphics Math Template Library is a collection of tools implementing Graphics primitives in generalized ways.
* [GMP](https://gmplib.org/) - A C library for arbitrary precision arithmetic, operating on signed integers, rational numbers, and floating-point numbers.
* [Klein](https://github.com/jeremyong/klein) - A fast, SIMD-optimized C++17 Geometric Algebra library for point, line, and plane projections, intersections, joins, rigid-body motion, and more.
* [linalg.h](https://github.com/sgorsten/linalg) - Single header, public domain, short vector math library for C++.
* [MATIO](https://github.com/tbeu/matio) -  MATLAB MAT File I/O Library. [website](https://sourceforge.net/projects/matio/)
* [MatX](https://github.com/NVIDIA/MatX) - A GPU-accelerated C++17 numerical computing library with a MATLAB/Python-like syntax.
* [MIRACL](https://github.com/CertiVox/MIRACL) - A Multiprecision Integer and Rational Arithmetic Cryptographic Library.
* [muparser](https://beltoforion.de/en/muparser) - muParser is an extensible high performance math expression parser library written in C++.
* [NT2](https://github.com/Mathieu-/nt2) - A SIMD-optimized numerical template library that provides an interface with MATLAB-like syntax.
* [LibTomMath](https://github.com/libtom/libtommath) - A free open source portable number theoretic multiple-precision integer library written entirely in C. [website](http://www.libtom.net/)
* [linmath.h](https://github.com/datenwolf/linmath.h) - A lean linear math library, aimed at graphics programming.
* [lp_solve](https://sourceforge.net/projects/lpsolve) - A library used to formulate and solve linear programming problems. [website](http://lpsolve.sourceforge.net)
* [OpenBLAS](https://github.com/xianyi/OpenBLAS) - An optimized BLAS library based on GotoBLAS2 1.13 BSD version. [website](http://www.openblas.net/)
* [PCG-rand](https://www.pcg-random.org/) - PCG is a family of simple fast space-efficient statistically good algorithms for random number generation. Unlike many general-purpose RNGs, they are also hard to predict.
* [QuantLib](https://github.com/lballabio/quantlib) - A free/open-source library for quantitative finance. [Modified BSD] [website](http://quantlib.org/)
* [Random](https://github.com/effolkronium/random) - Random for modern C++ with convenient API.
* [SimSIMD](https://github.com/ashvardanian/SimSIMD) - Vector distance functions for x86 AVX2, AVX-512, Arm NEON, and SVE.
* [StatsLib](https://github.com/kthohr/stats) - A C++ header-only library of statistical distribution functions. [website](https://www.kthohr.com/statslib.html)
* [SymEngine](https://github.com/symengine/symengine) - Fast symbolic manipulation library, a rewriting of SymPy's core in C++.
* [TinyExpr](https://github.com/codeplea/tinyexpr) - A C library for parsing and evaluating math expressions from strings.
* [Vc](https://github.com/VcDevel/Vc) - SIMD Vector Classes for C++.
* [Versor](http://versor.mat.ucsb.edu/) - A (fast) Generic C++ library for Geometric Algebras, including Euclidean, Projective, Conformal, Spacetime (etc).
* [Wagyu](https://github.com/mapbox/wagyu) - A general library for geometry operations of union, intersections, difference, and xor.
* [Wykobi](http://www.wykobi.com/) - A C++ library of efficient, robust and simple to use C++ 2D/3D oriented computational geometry routines.
* [xtensor](https://github.com/QuantStack/xtensor) - A C++14 library for numerical analysis with multi-dimensional array expressions, inspired by NumPy syntax. [website](http://quantstack.net/xtensor)
* [universal](https://github.com/stillwater-sc/universal) - A C++14 header-only library implementing arbitrary posit arithmetic. The posit number system is a tapered floating point that is more efficient than IEEE floating point. Posits enable reproducible computational science. [website](http://www.stillwater-sc.com/assets/content/stillwater-universal-sw.html)
* [utl::random](https://github.com/DmitriBogdanov/UTL/blob/master/docs/module_random.md) - A C++17 header-only library implementing fast random for Monte-Carlo simulations & gamedev.
* [XAD](https://github.com/auto-differentiation/xad) - Powerful Automatic Differentiation for C++. [website](https://auto-differentiation.github.io/)

## Memory Allocation

* [Boehm GC](https://github.com/ivmai/bdwgc) - Conservative garbage collector for C and C++. [website](http://www.hboehm.info/gc/)
* [buddy_alloc](https://github.com/spaskalev/buddy_alloc) - A single header buddy memory allocator for C with bounded allocation costs.
* [C Smart Pointers](https://github.com/Snaipe/libcsptr) - Smart pointers for the (GNU) C programming language.
* [gc](https://github.com/mkirchner/gc) - implementation of a conservative, thread-local, mark-and-sweep garbage collector.
* [Hoard](https://github.com/emeryberger/Hoard) - Fast, Scalable, and Memory-efficient Malloc for Linux, Windows, and Mac. [website](http://hoard.org/)
* [jemalloc](https://github.com/jemalloc/jemalloc) - General purpose malloc(3) implementation that emphasizes fragmentation avoidance and scalable concurrency support. [website](http://jemalloc.net/)
* [memory](https://github.com/foonathan/memory) - STL compatible C++ memory allocator library.
* [memory-allocators](https://github.com/mtrebi/memory-allocators) - Custom memory allocators to improve the performance of dynamic memory allocation.
* [Mesh](https://github.com/plasma-umass/Mesh) - A memory allocator that automatically reduces the memory footprint of C/C++ applications.
* [mimalloc](https://github.com/microsoft/mimalloc) - A compact general purpose allocator with excellent performance.
* [rpmalloc](https://github.com/mjansson/rpmalloc) - This library provides a public domain cross platform lock free thread caching 16-byte aligned memory allocator implemented in C.
* [sgcl](https://github.com/pebal/sgcl) - A real-time Garbage Collector for C++
* [sfl-pool-allocator](https://github.com/slavenf/sfl-pool-allocator) - C++11 memory allocator based on memory pools. It offers fast and efficient allocation of a large number of small-size objects.
* [snmalloc](https://github.com/microsoft/snmalloc) - Message passing based high-performance allocator.
* [STL Growing Allocator](https://github.com/LorienLV/stl-growing-allocator) - C++ stl memory allocator.
* [TCMalloc](https://github.com/google/tcmalloc) - Google's fast, multi-threaded malloc implementation [website](https://google.github.io/tcmalloc/).
* [tgc](https://github.com/orangeduck/tgc) - A tiny garbage collector for C written in \~500 LOC of code and based on the [Cello Garbage Collector](http://libcello.org/learn/garbage-collection).

### Memory Mapping

* [mio](https://github.com/vimpunk/mio) - Cross-platform C++11 header-only library for memory mapped file IO.
* [portable-memory-mapping](https://github.com/stbrumme/portable-memory-mapping) - Portable Memory Mapping C++ Class (Windows/Linux).

### Memory Pool

* [Allocator_MemoryPool](https://github.com/Cjkkkk/Allocator_MemoryPool) - MemoryPool based c++ STL Allocator.
* [AppShift-MemoryPool](https://github.com/DevShiftTeam/AppShift-MemoryPool) - A very fast cross-platform memory pool mechanism for C++ built using a data-oriented approach (3 to 24 times faster than regular new or delete, depending on operating system & compiler).
* [ConcurrentMemoryPool](https://github.com/gongzhaoxu/ConcurrentMemoryPool) - Based on tcmalloc, a highly concurrent memory pool with three layers of cache (ThreadCache, Centralcache, PageCache) is implemented.
* [ConcurrentMemoryPool](https://github.com/superwjfeng/ConcurrentMemoryPool) - 
* [cpp-mempool](https://github.com/hardikp/cpp-mempool) - C++ header-only mempool library.
* [cppmempool](https://github.com/benpm/cppmempool) - Very simple C++ heterogeneous memory pool with thread safety.
* [MemoryPool](https://github.com/Winteradio/MemoryPool) - MemoryPool for ECS.
* [MemoryPool](https://github.com/bertmelis/MemoryPool) - This is a simple memory pool that doesn't solve the fragmentation problem but contains it. EARLY VERSION. USE AT OWN RISK.
* [momo](https://github.com/morzhovets/momo) - C++ template containers with optimized memory consumption.

## Messaging

* [AMQP-CPP](https://github.com/CopernicaMarketingSoftware/AMQP-CPP) - C++ library for asynchronous non-blocking communication with RabbitMQ.
* [Beanstalk](https://github.com/beanstalkd/beanstalkd) - Beanstalk is a simple, fast work queue.
* [BlazingMQ](https://github.com/bloomberg/blazingmq) - A modern high-performance open source message queuing system.
* [iris](https://github.com/p-ranav/iris) - Lightweight Component Model and Messaging Framework based on ØMQ.
* [MPSC_Queue](https://github.com/MengRao/MPSC_Queue) - A multi-producer single consumer queue C++ template suitable for async logging with SHM IPC support.
* [OpenMAMA](https://github.com/finos/OpenMAMA) - OpenMAMA is an open source project that provides a high performance middleware agnostic messaging API that interfaces with a variety of proprietary and open source message oriented middleware systems. [openmama.org](https://openmama.org/)
* [PicoMsg](https://github.com/gamblevore/PicoMsg) - Miniature Message Passing IPC System (Single Header C++ File).
* [tcpshm](https://github.com/MengRao/tcpshm) - A connection-oriented persistent message queue framework based on TCP or SHM(shared memory).

### Kafka
* [cppkafka](https://github.com/mfontanini/cppkafka) - Modern C++ Apache Kafka client library (wrapper for librdkafka).
* [kafka_plugin](https://github.com/TP-Lab/kafka_plugin) - EOSIO Kafka Plugin is used for building real-time data pipelines and streaming apps. This plugin allows you to utilize all of Kafka’s rich real-time features utilizing the EOS blockchain.
* [kcat](https://github.com/edenhill/kcat) - Generic command line non-JVM Apache Kafka producer and consumer.
* [kspp](https://github.com/bitbouncer/kspp) - A high performance/ real-time C++ Kafka streams framework (C++17).
* [librdkafka](https://github.com/edenhill/librdkafka) - Apache Kafka client library for C and C++.
* [libkafka](https://github.com/adobe-research/libkafka) - A C++ client library for Apache Kafka v0.8+. Also includes C API.
* [logkafka](https://github.com/Qihoo360/logkafka) - Collect logs and send lines to Apache Kafka.
* [modern-cpp-kafka](https://github.com/morganstanley/modern-cpp-kafka) - A C++ API for Kafka clients (i.e. KafkaProducer, KafkaConsumer, AdminClient).
* [qbusbridge](https://github.com/Qihoo360/qbusbridge) - The Apache Kafka Client SDK.
* [plumber](https://github.com/RPG-18/plumber) - Simple desktop application for Apache Kafka.

## Microsoft Office

* [DuckX](https://github.com/amiremohamadi/DuckX) - C++ library for creating and modifying Microsoft Word (.docx) files.

### Microsoft Excel
*Libraries for working with Microsoft Excel.*

* [XLSX I/O](https://brechtsanders.github.io/xlsxio/) - Cross-platform library for reading and writing .xlsx
* [xlsx_drone](https://github.com/damian-m-g/xlsx_drone) - Fast Microsoft Excel's *.xlsx reader.

## Multimedia

* [gerbera](https://github.com/gerbera/gerbera) - UPnP media server which allows you to stream your digital media through your home network and consume it on a variety of UPnP compatible devices.
* [GStreamer](http://gstreamer.freedesktop.org/) - A library for constructing graphs of media-handling components.
* [libass](https://github.com/libass/libass) - Portable subtitle renderer for the ASS/SSA subtitle format.
* [libav](https://github.com/libav/libav) - A collection of libraries and tools to process multimedia content such as audio, video, subtitles and related metadata. [LGPL v2.1+ and others] [website](https://www.libav.org/)
* [LIVE555 Streaming Media](http://www.live555.com/liveMedia/) - Multimedia streaming library using open standard protocols (RTP/RTCP, RTSP, SIP).
* [libVLC](https://wiki.videolan.org/LibVLC) - libVLC (VLC SDK) media framework.
* [MediaInfoLib](https://github.com/MediaArea/MediaInfoLib) - Convenient unified display of the most relevant technical and tag data for video and audio files.
* [QtAv](https://github.com/wang-bin/QtAV) - A multimedia playback framework based on Qt and FFmpeg to write a player easily. [website](http://wang-bin.github.io/QtAV/)
* [SDL](http://www.libsdl.org/) - Simple DirectMedia Layer.
* [SFML](https://github.com/SFML/SFML) - Simple and Fast Multimedia Library. [website](http://www.sfml-dev.org/)
* [TagLib](https://github.com/taglib/taglib) - A library for reading and editing the metadata of several popular audio formats. [website](https://taglib.org/)

## Networking

* [ada](https://github.com/ada-url/ada) - WHATWG-compliant and fast URL parser written in modern C++.
* [ACE](http://www.dre.vanderbilt.edu/~schmidt/ACE.html) - An OO Network Programming Toolkit in C++.
* [asio2](https://github.com/zhllxt/asio2) - Header only c++ network library, based on asio,support tcp,udp,http,websocket,rpc,ssl,icmp,serial_port,socks5.
* [asio3](https://github.com/zhllxt/asio3) - Header only c++ network library, based on c++ 20 coroutine and asio.
* [async-sockets-cpp](https://github.com/eminfedar/async-sockets-cpp) - Simple thread-based asynchronous TCP & UDP Socket classes in C++.
* [Boost.Asio](https://github.com/boostorg/asio) - A cross-platform C++ library for network and low-level I/O programming.
* [Boost.Beast](https://github.com/boostorg/beast) - HTTP and WebSocket built on Boost.Asio in C++11. [website](www.boost.org/libs/beast)
* [Breep](https://github.com/Organic-Code/Breep) - Event based, high-level C++14 peer-to-peer library.
* [brynet](https://github.com/IronsDu/brynet) - A Header-Only cross-platform C++ TCP network library.
* [C++ REST SDK](https://github.com/Microsoft/cpprestsdk) - The C++ REST SDK is a Microsoft project for cloud-based client-server communication in native code using a modern asynchronous C++ API design. This project aims to help C++ developers connect to and interact with services. (previously named Casablanca).
* [draft-http-tunnel](https://github.com/cmello/draft-http-tunnel) - An experimental HTTP tunnel implementing the [HTTP CONNECT negotiation](https://en.wikipedia.org/wiki/HTTP_tunnel).
* [c-ares](https://github.com/c-ares/c-ares) - A C library for asynchronous DNS requests.
* [CHL](https://github.com/it4e/CHL) - C Hypertext Library - A library for writing web applications in C.
* [cobalt](https://github.com/boostorg/cobalt) - Coroutines for C++20 & asio.
* [coroio](https://github.com/resetius/coroio) - Networking library using C++20 coroutines.
* [corokafka](https://github.com/bloomberg/corokafka) - C++ Kafka coroutine library using Quantum dispatcher and wrapping CppKafka.
* [cpp-httplib](https://github.com/yhirose/cpp-httplib) - A single file C++11 header-only HTTP/HTTPS sever library.
* [cpp-netlib](http://cpp-netlib.org/) - A collection of open-source libraries for high level network programming.
* [cpp-netlib/uri](https://github.com/cpp-netlib/uri) - URI parser/builder library for C++, compatible with RFC 3986 and RFC 3987.
* [cpp-rest-api-core](https://github.com/systelab/cpp-rest-api-core) - C++ framework to facilitate the creation of REST API.
* [CppNet](https://github.com/caozhiyi/CppNet) - Cross platform network library with C++11.
* [CppServer](https://github.com/chronoxor/CppServer) - Ultra fast and low latency asynchronous socket server & client C++ library with support TCP, SSL, UDP, HTTP, HTTPS, WebSocket protocols and 10K connections problem solution.
* [cpr](https://github.com/libcpr/cpr) - A modern C++ HTTP requests library with a simple but powerful interface. Modeled after the Python Requests module. [website](https://docs.libcpr.org/)
* [curlcpp](https://github.com/JosephP91/curlcpp) - An object oriented C++ wrapper for CURL(libcurl).
* [curlpp](https://github.com/jpbarrette/curlpp) - C++ wrapper around libcURL.
* [DPDK](https://github.com/DPDK/dpdk) - Data Plane Development Kit, libraries and drivers for fast packet processing. [website](https://www.dpdk.org/)
* [Dyad.c](https://github.com/rxi/dyad) - Asynchronous networking for C.
* [ecapture](https://github.com/gojue/ecapture) - Capture SSL/TLS text content without a CA certificate using eBPF. This tool is compatible with Linux/Android x86_64/Aarch64. [website](https://ecapture.cc/)
* [ENet](https://github.com/lsalzman/enet) - Reliable UDP networking library. [website](http://enet.bespin.org/)
* [envoy](https://github.com/envoyproxy/envoy) - Cloud-native high-performance edge/middle/service proxy.
* [evpp](https://github.com/Qihoo360/evpp) - C++ high performance networking with TCP/UDP/HTTP protocols.
* [f-stack](https://github.com/F-Stack/f-stack) - F-Stack is an user space network development kit with high performance based on DPDK, FreeBSD TCP/IP stack and coroutine API. (http://www.f-stack.org)
* [Flare](https://github.com/Tencent/flare) - Modern C++ development framework widely used in Tencent's advertising background, including basic libraries, RPC, various clients, etc.The main features are strong ease of use and low long tail delay.
* [FTP client for C++](https://github.com/embeddedmz/ftpclient-cpp) - C++ client for making FTP requests.
* [gumbo-parser](https://github.com/google/gumbo-parser) - HTML5 parsing library in C99.
* [gsocket](https://github.com/hackerschoice/gsocket) - The Global Socket Tookit allows two users behind NAT/Firewall to establish a TCP connection with each other.
* [haproxy](https://github.com/haproxy/haproxy) - HAProxy Load Balancer's.
* [H2O](https://github.com/h2o/h2o) - An optimized HTTP server with support for HTTP/1.x and HTTP/2. It can also be used as a library.
* [nghttp3](https://github.com/ngtcp2/nghttp3) - HTTP/3 library written in C.
* [HTTP Parser](https://github.com/nodejs/http-parser) - A http request/response parser for C.
* [http-server](https://github.com/trungams/http-server) - A simple HTTP/1.1 server implementation in C++.
* [httpserver.h](https://github.com/jeremycw/httpserver.h) - single header C library for building event driven non-blocking HTTP servers. Supports Linux with epoll and BSD/Mac with kqueue.
* [HP-Socket](https://github.com/ldcsaa/HP-Socket) - High Performance TCP/UDP/HTTP Communication Component https://www.oschina.net/p/hp-socket
* [i2pd](https://github.com/PurpleI2P/i2pd) - i2pd (I2P Daemon) is a full-featured C++ implementation of I2P client.
* [Katran](https://github.com/facebookincubator/katran) - A high performance layer 4 load balancer.
* [KCP](https://github.com/skywind3000/kcp/blob/master/README.en.md) - A fast and reliable ARQ protocol that helps applications to reduce network latency.
* [libasyncd](https://github.com/wolkykim/libasyncd) - Embeddable event-based asynchronous HTTP server library for C.
* [libevbuffsock](https://github.com/mreiferson/libevbuffsock) - libevbuffsock is a minimal port of the evbuffer suite of functions from libevent to libev.
* [libcurl](http://curl.haxx.se/libcurl/) - Multiprotocol file transfer library.
* [libfv](https://github.com/fawdlstty/libfv) - C++20 header-only network library, support TCP/SSL/HTTP/websocket server and client.
* [libhttp](https://github.com/lammertb/libhttp) - The official home of LibHTTP is [www.libhttp.org](www.libhttp.org).
* [libhttpserver](https://github.com/etr/libhttpserver) - C++ library for creating an embedded Rest HTTP server (and more).
* [libio](https://github.com/wxggg/libio) - A high performance multithread C++ network library for linux.
* [libjingle](https://code.google.com/p/libjingle/) - Google talk voice and P2P interoperability library.
* [libip](https://github.com/nort-cnc-control/libip) - This is small and simple event-based UPD/IP library. TCP is not supported and not planned (now, at least). It implements ONLY network, and you don't need to write any extra code, such as program state, or smth else. Just network events.
* [Libmicrohttpd](http://www.gnu.org/software/libmicrohttpd/) - GNU libmicrohttpd is a small C library that is supposed to make it easy to run an HTTP server as part of another application.
* [libnl](https://www.infradead.org/~tgr/libnl/) - `libnl` is a collection of libraries to provie APIs to the Netlink protocol (replacement for ioctl). It's primary use is to communicate with the Linux kernel, to modify networking state (interfaces, routing etc...).
* [libpcap](https://github.com/the-tcpdump-group/libpcap) - A portable C/C++ library for network traffic capture. [website](https://www.tcpdump.org/)
* [libquic](https://github.com/devsisters/libquic) - A QUIC protocol library extracted from Chromium's QUIC Implementation.
* [libserver](https://github.com/littledan/libserver) - Coroutine-based web servers.
* [libwebsock](https://github.com/JonnyWhatshisface/libwebsock) - Easy-to-use and powerful web socket library.
* [libwebsockets](https://github.com/warmcat/libwebsockets) - A lightweight pure C WebSocket implementation that provides both client and server libraries. [website](https://libwebsockets.org/)
* [lwIP](http://savannah.nongnu.org/projects/lwip/) - A lightweight TCP/IP stack.
* [libsagui](https://risoflora.github.io/libsagui/) - Sagui is a cross-platform C library which helps to develop web servers or frameworks.
* [libvldmail](https://github.com/dertuxmalwieder/libvldmail) - Your friendly e-mail address validation library.
* [ltio](https://github.com/echoface/ltio) - LigthingIO is a lightweight network IO framework with some infrastructure code for better coding experience.
* [mongols](https://github.com/webcpp/mongols) - C++ high performance networking with TCP/UDP/RESP/HTTP/WebSocket protocols.
* [Mongoose](https://github.com/cesanta/mongoose) - Extremely lightweight webserver.
* [MQTT-C](https://github.com/LiamBindle/MQTT-C) - A portable MQTT C client for embedded systems and PCs alike. [website](https://liambindle.ca/MQTT-C)
* [mTCP](https://github.com/mtcp-stack/mtcp) - Highly scalable user-level TCP stack for multicore systems.
* [Mumble](https://github.com/mumble-voip/mumble) - Mumble is an open-source, low-latency, high quality voice chat software. [www.mumble.info](https://www.mumble.info/)
* [Muduo](https://github.com/chenshuo/muduo) - A C++ non-blocking network library for multi-threaded server in Linux.
* [MultiThreadedProxyServerClient](https://github.com/Lovepreet-Singh-LPSK/MultiThreadedProxyServerClient) - This project is implemented using C and Parsing of HTTP referred from [Proxy Server](https://github.com/vaibhavnaagar/proxy-server)
* [n2n](https://github.com/ntop/n2n) - Peer-to-peer VPN.
* [naiveproxy](https://github.com/klzgrad/naiveproxy) - NaïveProxy uses Chromium's network stack to camouflage traffic with strong censorship resistence and low detectablility. Reusing Chrome's stack also ensures best practices in performance and security.
* [nanomsg](https://github.com/nanomsg/nanomsg) - C-based implementation of ZeroMQ.
* [nchan](https://github.com/slact/nchan) - Fast, horizontally scalable, multiprocess pub/sub queuing server and proxy for HTTP, long-polling, Websockets and EventSource (SSE), powered by Nginx.
* [nekoray](https://github.com/MatsuriDayo/nekoray) - Qt based cross-platform GUI proxy configuration manager (backend: v2ray / sing-box).
* [neon](https://github.com/notroj/neon) - neon - an HTTP/1.1 and WebDAV client library with a C API.
* [network-driver](https://github.com/jakobod/network-driver) - An asynchronous network abstraction framework for applications written C++20.
* [nghttp2](https://github.com/nghttp2/nghttp2) - HTTP/2 C Library. [website](https://nghttp2.org/)
* [nghttp3](https://github.com/ngtcp2/nghttp3) - HTTP/3 library written in C. [website](https://nghttp2.org/nghttp3/)
* [ngrest](https://github.com/loentar/ngrest) - Fast and easy C++ RESTful WebServices framework.
* [NNG](https://nanomsg.github.io/nng/) - nanomsg-next-generation - lightweight brokerless messaging.
* [ntf-core](https://github.com/bloomberg/ntf-core) - Sockets, timers, resolvers, events, reactors, proactors, and thread pools for asynchronous network programming.
* [odhcpd](https://github.com/openwrt/odhcpd) - odhcpd - Embedded DHCP/DHCPv6/RA Server & Relay.
* [Onion](https://github.com/davidmoreno/onion) - HTTP server library in C designed to be lightweight and easy to use.
* [PF_RING™](https://github.com/ntop/PF_RING) - High-speed packet processing framework. [website](https://www.ntop.org/products/packet-capture/pf_ring/)
* [picoev](https://github.com/kazuho/picoev) - A tiny, lightning fast event loop for network applications.
* [PicoHTTPParser](https://github.com/h2o/picohttpparser) - A tiny, primitive, fast HTTP request/response parser.
* [POCO](https://github.com/pocoproject) - C++ class libraries and frameworks for building network- and internet-based applications that run on desktop, server, mobile and embedded systems. [website](http://pocoproject.org/)
* [PowerDNS](https://github.com/PowerDNS/pdns) - PowerDNS Authoritative, PowerDNS Recursor, dnsdist.
* [Proxygen](https://github.com/facebook/proxygen) - Facebook's collection of C++ HTTP libraries including an easy to use HTTP server.
* [RakNet](https://github.com/OculusVR/RakNet) - A cross platform, open source, C++ networking engine for game programmers.
* [Restbed](https://github.com/corvusoft/restbed) - Corvusoft's Restbed framework brings asynchronous RESTful functionality to C++14 applications.
* [restc-cpp](https://github.com/jgaa/restc-cpp) - Modern C++ REST Client library.
* [Restinio](https://github.com/Stiffstream/restinio) - A header-only C++14 library that gives you an embedded HTTP/Websocket server.
* [restclient-cpp](https://github.com/mrtazz/restclient-cpp) - Simple REST client for C++. It wraps libcurl for HTTP requests.
* [Seasocks](https://github.com/mattgodbolt/seasocks) - Simple, small, C++ embeddable webserver with WebSockets support.
* [sion](https://github.com/zanllp/sion) - A lightweight cross-platform C++ HTTP client. supports asynchronous, https.
* [silgy](https://github.com/silgy/silgy) - Asynchronous HTTP(S) engine for C/C++ projects.
* [Silicon](http://siliconframework.org) - A high performance, middleware oriented C++14 http web framework.
* [SimpleAmqpClient](https://github.com/alanxz/SimpleAmqpClient) - Simple C++ Interface to rabbitmq-c.
* [socket99](https://github.com/silentbicycle/socket99) - C99 wrapper for the BSD sockets API.
* [socket.io-clientpp](https://github.com/ebshimizu/socket.io-clientpp) - A websocket++ and rapidjson based C++ Socket.IO client.
* [socket.io-client-cpp](https://github.com/socketio/socket.io-client-cpp) - C++11 implementation of Socket.IO client.
* [sockpp](https://github.com/fpagliughi/sockpp) - Modern C++ socket library.
* [srt](https://github.com/Haivision/srt) - Secure Reliable Transport (SRT) is a transport protocol for ultra low (sub-second) latency live video and audio streaming, as well as for generic bulk data transfer.
* [Taco Pie](https://github.com/cylix/tacopie) - multi-platform TCP Client & Server C++11 library.
* [taotu](https://github.com/InfraSail/taotu) - A lightweight multithreading C++ network library based on Reactor.
* [tinyproxy](https://github.com/tinyproxy/tinyproxy) - A light-weight HTTP/HTTPS proxy daemon for POSIX operating systems.
* [tlse](https://github.com/eduardsui/tlse) - Single C file TLS 1.2/1.3 implementation, using tomcrypt as crypto library.
* [TQUIC](https://github.com/tencent/tquic) - A high-performance, lightweight, and cross-platform QUIC library, exposed to C and C++.
* [Tufão](https://github.com/vinipsmaker/tufao) - An asynchronous web framework for C++ built on top of Qt.
* [toxcore](https://github.com/TokTok/c-toxcore) - Tox is a peer to peer (serverless) instant messenger aimed at making security and privacy easy to obtain for regular users. It uses [NaCl](https://nacl.cr.yp.to/) for its encryption and authentication.
* [trantor](https://github.com/an-tao/trantor) - A non-blocking I/O cross-platform TCP network library, using C++14.
* [ULib](https://github.com/stefanocasazza/ULib) - C++ application development framework, to help developers create and deploy applications quickly and simply.
* [uriparser](https://github.com/uriparser/uriparser) - Strictly RFC 3986 compliant URI parsing and handling library.
* [uSockets](https://github.com/uNetworking/uSockets) - µSockets - miniscule networking & eventing. This is the cross-platform async networking and eventing foundation library.
* [UCall](https://github.com/unum-cloud/ucall) - A high-performance SIMD-accelerated RPC library on io_uring.
* [uWebSockets](https://github.com/uNetworking/uWebSockets) - µWS is one of the most lightweight, efficient & scalable WebSocket & HTTP server implementations available.
* [WAFer](https://github.com/riolet/WAFer) - A C language-based ultra-light software platform for scalable server-side and networking applications. Think node.js for C programmers.
* [Wangle](https://github.com/facebook/wangle) - A client/server application framework to build asynchronous, event-driven modern C++ services.
* [wdt](https://github.com/facebook/wdt) - An embeddedable library (and command line tool) aiming to transfer data between 2 systems as fast as possible over multiple TCP paths.
* [WebSocket++](https://github.com/zaphoyd/websocketpp) - WebSocket++ is a header only C++ library that implements RFC6455 The WebSocket Protocol. It allows integrating WebSocket client and server functionality into C++ programs.
* [wslay](https://tatsuhiro-t.github.io/wslay) - The WebSocket library in C.
* [PcapPlusPlus](https://github.com/seladb/PcapPlusPlus) - a multiplatform C++ network sniffing and packet parsing and crafting framework.
* [ZeroMQ](https://github.com/zeromq/libzmq) - High-speed, modular asynchronous communication library.
* [zproto](https://github.com/zeromq/zproto) - Protocol framework for ZeroMQ.

## Node.js

* [nodecpp-demo](https://github.com/freezer333/nodecpp-demo) - This repository contains full code listings that help guide you through developing Node.js addons.
* [streaming-worker](https://github.com/freezer333/streaming-worker) - streaming-worker is designed to give you a simple interface for sending and receiving events/messages from a long running asynchronous C++ Node.js Addon.

## PDF
*Libraries for parsing and manipulating PDF documents.*

* [libharu](https://github.com/libharu/libharu) - A free, cross platform, open-sourced software library for generating PDF.
* [litePDF](https://litepdf.sourceforge.io) - Library to create and edit PDF documents that uses GDI functions through a device context to draw the page content.
* [MuPDF](https://mupdf.com/) - A lightweight PDF, XPS, and E-book viewer.
* [PDF-Writer](https://github.com/galkahana/PDF-Writer) - High performance library for creating, modiyfing and parsing PDF files in C++. [website](https://www.pdfhummus.com/)
* [PDFium](https://pdfium.googlesource.com/pdfium/) - PDF generation and rendering library.
* [pdfio](https://github.com/michaelrsweet/pdfio) - PDFio is a simple C library for reading and writing PDF files.
* [PoDoFo](http://podofo.sourceforge.net/) - A library to work with the PDF file format.
* [Poppler](https://poppler.freedesktop.org/) - Open-source multi-backend PDF rendering library based on the xpdf-3.0 code base.
* [DynaPDF](http://www.dynaforms.com/) - An easy-to-use PDF generation library.
* [VersyPDF](https://github.com/sybrexsys/VersyPDF) - High-quality, industry-strength PDF library for C/C++ programming languages meeting the requirements of the most demanding and diverse applications. Using VersyPDF library you can write stand-alone, cross-platform and reliable applications that can read, write, and edit PDF documents.
* [Xpdf](https://www.xpdfreader.com/) - Xpdf is a free PDF viewer and toolkit, including a text extractor, image converter, HTML converter, and more.

## Performance

* [prometheus-cpp-lite](https://github.com/biaks/prometheus-cpp-lite) - C++ header-only prometheus client library for quickly and easily adding metric (and profiling) functionality to C++ projects.
* [tracy](https://github.com/wolfpld/tracy) - A real time, nanosecond resolution, remote telemetry, hybrid frame and sampling profiler for games and other applications.
* [OpenTelemetry](https://github.com/open-telemetry/opentelemetry-cpp) - The OpenTelemetry C++ Client. [website](https://opentelemetry.io/)

## Physics
*Dynamics simulation engines*

* [Box2D](https://github.com/erincatto/Box2D) - A 2D physics engine for games.
* [Bullet](https://github.com/bulletphysics/bullet3) - A 3D physics engine for games.
* [Chipmunk](https://github.com/slembcke/Chipmunk2D) - A fast and lightweight 2D game physics library.
* [Jolt Physics](https://github.com/jrouwe/JoltPhysics) - A multi core friendly rigid body physics and collision detection library.
* [Kratos](https://github.com/KratosMultiphysics/Kratos) - framework for building parallel, multi-disciplinary simulation software, aiming at modularity, extensibility, and high performance. [website](http://www.cimne.com/kratos/)
* [LiquidFun](https://github.com/google/liquidfun) - A 2D physics engine for games.
* [Newton Dynamics](https://github.com/MADEAPPS/newton-dynamics) - An integrated solution for real time simulation of physics environments.
* [ODE](http://www.ode.org/) - Open Dynamics Engine - An open source, high performance library for simulating rigid body dynamics.
* [ofxBox2d](https://github.com/vanderlin/ofxBox2d) - openFrameworks wrapper for Box2D.
* [PhysX](https://github.com/NVIDIAGameWorks/PhysX-3.4) - An open-source realtime physics engine middleware SDK developed by Nvidia as a part of Nvidia GameWorks software suite.
* [PlayRho](https://github.com/louis-langholtz/PlayRho) - An interactive physics engine & library.
* [Project Chrono](https://github.com/projectchrono/chrono) - An open source multi-physics simulation engine.
* [Quantum++](https://github.com/vsoftco/qpp) - A modern C++11 quantum computing library.
* [QuarkPhysics](https://github.com/erayzesen/QuarkPhysics) - A 2D Soft Body and Rigid Body Physics Engine.
* [Simbody](https://github.com/simbody/simbody) - High-performance C++ multibody dynamics/physics library for simulating articulated biomechanical and mechanical systems like vehicles, robots, and the human skeleton.
* [SOFA](https://github.com/sofa-framework/sofa) - SOFA is an open-source framework targeting at real-time simulation, with an emphasis on medical simulation.

## QrCode

*Libraries for manipulating with QrCodes.*

* [ZXing-C++](https://github.com/zxing-cpp/zxing-cpp) - ZXing-C++ ("zebra crossing") is an open-source, multi-format linear/matrix barcode image processing library implemented in C++.

## Reflection

* [Better Enums](https://github.com/aantron/better-enums) - Reflective enums (to string, iteration). Single header. [website](http://aantron.github.io/better-enums/)
* [Boost.PFR](https://github.com/boostorg/pfr) - std::tuple like methods for user defined types without any macro or boilerplate code.
* [clReflect](https://bitbucket.org/dwilliamson/clreflect) - C++ Reflection using clang.
* [CPFG](https://github.com/cpgf/cpgf) - A C++03 library for reflection, callback and script binding. [website](http://www.cpgf.org/)
* [CPP-Reflection](https://github.com/AustinBrunkhorst/CPP-Reflection) - C++ Reflection using clang.
* [Easy Reflection](https://github.com/chocolacula/easy_reflection_cpp) - Easy and fast reflection + serialization solution like in Rust, Java or Go.
* [Magic Enum](https://github.com/Neargye/magic_enum) - Header-only C++17 library provides static reflection for enums (to string, from string, iteration), work with any enum type without any macro or boilerplate code.
* [magic_get](https://github.com/apolukhin/magic_get) - std::tuple like methods for user defined types without any macro or boilerplate code.
* [meta](https://github.com/skypjack/meta) - Header-only, non-intrusive and macro-free runtime reflection system in C++.
* [Nameof](https://github.com/Neargye/nameof) - Header-only C++17 library provides nameof macros and functions to obtain the simple name of variable, type, function, macro, and enum.
* [Ponder](https://github.com/billyquith/ponder) - A C++11 library for reflection.
* [refl-cpp](https://github.com/veselink1/refl-cpp) - Static reflection for C++17 (compile-time enumeration, attributes, proxies, overloads, template functions, metaprogramming).
* [REFLECT](https://github.com/qlibs/reflect) - C++20 Static Reflection library.
* [reflect-cpp](https://github.com/getml/reflect-cpp) - Serialization through reflection, including automatic field name retrieval from structs.
* [RTTR](https://github.com/rttrorg/rttr) - A C++11 library for reflection. [website](http://www.rttr.org)
* [simple_enum](https://github.com/arturbac/simple_enum) - An Fast, Intuitive and Type-Safe C++ Enumeration Support Library. [website](https://arturbac.github.io/simple_enum/)
* [TSMP](https://github.com/fabian-jung/tsmp) - An intrusion and macro-free C++20 library for static reflection. It uses libclang to extract reflection data from your source code and makes it usable via template specialization.
* [visit_struct](https://github.com/cbeck88/visit_struct) - A miniature library for struct-field reflection in C++.

## Regular Expression

* [CppVerbalExpressions](https://github.com/VerbalExpressions/CppVerbalExpressions) - C++ regular expressions made easy.
* [CTRE](https://github.com/hanickadot/compile-time-regular-expressions) - A Compile time PCRE (almost) compatible regular expression matcher.
* [Hyperscan](https://github.com/intel/hyperscan) - Hyperscan is a high-performance multiple regex matching library by Intel. Provides simultaneous matching of large numbers of regexps (up to tens of thousands). Typically used in a DPI library stack.
* [Onigmo](https://github.com/k-takata/Onigmo) - Fork of Oniguruma, supporting more advanced regexps.
* [Oniguruma](https://github.com/kkos/oniguruma) - A modern and flexible regular expressions library that supports a variety of character encodings.
* [PCRE](http://pcre.org/) - A regular expression C library inspired by the regular expression capabilities in Perl.
* [PCRE2](https://github.com/PCRE/pcre2) - PCRE2 is a re-working of the original PCRE1 library to provide an entirely new API.
* [PIRE](https://github.com/yandex/pire) - Perl Incompatible Regular Expressions library by Yandex. Can be really fast (more than 400 MB/s). [LPGL v3.0]
* [R3](https://github.com/c9s/r3) - R3 is an URL router library with high performance, thus, it's implemented in C. It compiles your R3Route paths into a prefix trie. By using the prefix tree constructed in the start-up time, you can dispatch the path to the controller with high efficiency.
* [RE2](https://github.com/google/re2) - A software library for regular expressions via a finite-state machine using automata theory.
* [regexp4](https://github.com/nasciiboy/recursiveregexpraptor-4) - Simple and complete implementation of regular expressions with its own sintax.
* [SLRE](https://github.com/cesanta/slre) - Super Light Regexp engine for C/C++.
* [sregex](https://github.com/openresty/sregex) - A non-backtracking NFA/DFA-based Perl-compatible regex engine library for matching on large data streams.
* [SRELL](https://www.akenotsuki.com/misc/srell/en/) - Unicode-aware regular expression template library for C++.
* [toy-regex](https://github.com/loveyacper/toy-regex) - A toy-regex use follow set and DFA.
* [TRE](https://github.com/laurikari/tre/) - POSIX-compliant, feature-full regex library.
* [Vectorscan](https://github.com/VectorCamp/vectorscan) - A portable fork of the high-performance regular expression matching library. [website](https://www.vectorcamp.gr/project/vectorscan/)

## Robotics

* [MOOS-IvP](http://moos-ivp.org) - A set of open source C++ modules for providing autonomy on robotic platforms, in particular autonomous marine vehicles.
* [MRPT](http://www.mrpt.org/) - Mobile Robot Programming Toolkit.
* [PCL](https://github.com/PointCloudLibrary/pcl) - Point Cloud Library is a standalone, large scale, open project for 2D/3D image and point cloud processing [website](http://www.pointclouds.org/)
* [Robotics Library (RL)](http://www.roboticslibrary.org/) - A self-contained C++ library for robot kinematics, motion planning and control.
* [RobWork](http://www.robwork.dk/apidoc/nightly/rw/index.html) - A collection of C++ libraries for simulation and control of robot systems.
* [ROS](http://wiki.ros.org/) - Robot Operating System provides libraries and tools to help software developers create robot applications.

## Scientific Computing

* [AMGCL](https://github.com/ddemidov/amgcl) - a header-only C++ library for solving large sparse linear systems with algebraic multigrid.
* [Au](https://github.com/aurora-opensource/au) - A C++14-compatible physical units library with no dependencies and a single-file delivery option. Emphasis on safety, accessibility, performance, and developer experience. [website](https://aurora-opensource.github.io/au/main/)
* [GSL](http://www.gnu.org/software/gsl/) - GNU scientific library.
* [Trilinos](https://github.com/trilinos/Trilinos) - High performance PDE solvers.
* [Torch](https://github.com/torch/torch7) - A scientific computing framework with wide support for machine learning algorithms that puts GPUs first. [website](http://torch.ch/)

## Scripting

* [AngelScript](https://www.angelcode.com/angelscript/) - AngelScript is a game-oriented interpreted/compiled scripting language.
* [Boost.Python](https://github.com/boostorg/python) - A C++ library which enables seamless interoperability between C++ and the Python programming language.
* [caccessor](https://github.com/cloudwu/caccessor) - Let lua access C types.
* [cffi-lua](https://github.com/q66/cffi-lua) - A portable C FFI for Lua 5.1+
* [cpp-peglib](https://github.com/yhirose/cpp-peglib) - A single file C++ header-only PEG (Parsing Expression Grammars) library.
* [cppimport](https://github.com/tbenthompson/cppimport) - Import C++ files directly from Python!
* [CppSharp](https://github.com/mono/CppSharp) - Tools and libraries to glue C/C++ APIs to high-level languages.
* [ChaiScript](https://github.com/ChaiScript/ChaiScript/) - An easy to use embedded scripting language for C++. [website](https://chaiscript.com/)
* [ctypes.sh](https://github.com/taviso/ctypes.sh) - A foreign function interface for bash.
* [Cython](https://github.com/cython/cython) - Cython is an optimising static compiler for both the Python programming language and the extended Cython programming language (based on Pyrex). It makes writing C extensions for Python as easy as Python itself. [website](http://cython.org/)
* [datatree-lua](https://github.com/cloudwu/datatree) - DataTree structure for Lua.
* [djinni](https://github.com/dropbox/djinni) - A tool for generating cross-language type declarations and interface bindings.
* [JavaCpp](https://github.com/bytedeco/javacpp) - The missing bridge between Java and native C++.
* [JerryScript](https://github.com/jerryscript-project/jerryscript) - Lightweight JavaScript engine for resource-constrained devices such as microcontrollers. It can run on devices with less than 64 KB of RAM and less than 200 KB of flash memory.
* [hermes](https://github.com/facebook/hermes) - A JavaScript engine optimized for running React Native.
* [LEM](https://github.com/esmil/lem) - A Lua Event Machine.
* [libffi](https://github.com/libffi/libffi) - A portable foreign-function interface library. [website](https://sourceware.org/libffi/)
* [Lua](http://www.lua.org/) - A minimal and fast scripting engine for configuration files and basic application scripting.
* [LuaBridge](https://github.com/vinniefalco/LuaBridge) - A lightweight, dependency-free library for binding Lua to C++.
* [LuaBridge3](https://github.com/kunitoki/LuaBridge3) - A lightweight, dependency-free library for binding Lua, LuaJIT, Luau and Ravi to C++.
* [luacstruct](https://github.com/yasuoka/luacstruct) - A simple C source code which provides a lua metatable representing a C struct, can be embedded within your application. Also it enables controlling access to the fields of the struct.
* [luacxx](https://github.com/dafrito/luacxx) - C++11 API for creating Lua bindings.
* [Luau](https://github.com/luau-lang/luau) - A fast, small, safe, gradually typed embeddable scripting language derived from Lua. [website](https://luau.org/)
* [lua-eco](https://github.com/zhaojh329/lua-eco) - Lua-eco is a Lua coroutine library which was implemented based on IO event.
* [luau](https://github.com/Roblox/luau) - A fast, small, safe, gradually typed embeddable scripting language derived from Lua. [website](https://luau-lang.org/)
* [MiniScript](https://miniscript.org/) - scripting language modern, elegant, easy to learn, and easy to embed in your own C# or C++ projects.
* [nanobind](https://github.com/wjakob/nanobind) - tiny and efficient C++/Python bindings.
* [nbind](https://github.com/charto/nbind) - Magical headers that make your C++ library accessible from JavaScript.
* [PHP-CPP](https://github.com/CopernicaMarketingSoftware/PHP-CPP) - A library to build PHP extensions with C++. [website](http://www.php-cpp.com/)
* [pocketpy](https://github.com/blueloveTH/pocketpy) - C++17 header-only Python interpreter for game scripting.
* [preCICE](https://github.com/precice/precice) - Coupling library for partitioned multi-physics simulations (FSI, CHT, and more).
* [pybind11](https://github.com/pybind/pybind11) - Seamless operability between C++11 and Python.
* [SIP](https://riverbankcomputing.com/software/sip/intro) - C or C++ Bindings Generator for Python v2 and v3.
* [sol2](https://github.com/ThePhD/sol2) - Sol3 (sol2 v3.0) - a C++ <-> Lua API wrapper with advanced features and top notch performance. [sol2.rtfd.io](http://sol2.rtfd.io/)
* [SWIG](https://github.com/swig/swig) - A wrapper/interface Generator that let you link your c++ code to Javascript, Perl, PHP, Python, Tcl and Ruby. [website](http://www.swig.org/)
* [txiki.js](https://github.com/saghul/txiki.js) - A tiny JavaScript runtime.
* [V7](https://github.com/cesanta/v7) - An embedded JavaScript engine.
* [V8](https://v8.dev) - A fast JavaScript engine by Google that can be embedded into any C++ application.
* [v8pp](https://github.com/pmed/v8pp) - Header-only library to expose C++ classes and functions into V8 to use them in JavaScript code. [website](https://pmed.github.io/v8pp/)
* [ChakraCore](https://github.com/Microsoft/ChakraCore) -A JavaScript engine by Microsoft that can be embedded into nodejs.
* [Wren](https://github.com/wren-lang/wren) - Wren is a small, fast, class-based concurrent scripting language.

## Security

*Tools and libraries that are used to help make your application more secure.*

* [Falco](https://github.com/falcosecurity/falco) - [Falco](https://falco.org/) is a cloud native runtime security tool for Linux operating systems. It is designed to detect and alert on abnormal behavior and potential security threats in real-time.
* [KMS](https://github.com/phaistos-networks/KMS) - Keys and Secrets Management Service.
* [SoftHSM](https://github.com/opendnssec/SoftHSMv2) - OpenDNSSEC handles and stores its cryptographic keys via the PKCS#11 interface.
* [wazuh](https://github.com/wazuh/wazuh) - The Open Source Security Platform.
* [xca](https://github.com/chris2511/xca) - XCA -- X Certificate and Key Management.
* [zkp-hmac-communication-cpp](https://github.com/zk-Call/zkp-hmac-communication-cpp) - "Zero-Knowledge" Proof Implementation with HMAC Communication in C++

## Serialization

* [alpaca](https://github.com/p-ranav/alpaca) - Serialization library written in C++17 - Pack C++ structs into a compact byte-array without any macros or boilerplate code.
* [bencode](https://github.com/fbdtemme/bencode) - A header-only C++20 bencode serialization/deserialization library.
* [binn](https://github.com/liteserver/binn) - Binary serialization format, meant to be compact, fast and easy-to-use.
* [BitSerializer](https://github.com/PavelKisliak/BitSerializer) - Multi-format serialization library (JSON, XML, YAML, CSV, MsgPack).
* [Bitsery](https://github.com/fraillt/bitsery) - Header only C++ binary serialization library.
* [Bond](https://github.com/Microsoft/bond) - An open source, cross-platform framework for working with schematized data.
* [Boost.Serialization](https://github.com/boostorg/serialization) - Boost Serialization Library.
* [capnproto](https://github.com/capnproto/capnproto) - Cap'n Proto serialization/RPC system - core tools and C++ library [capnproto.org](https://capnproto.org/)
* [c-capnproto](https://github.com/jmckaskill/c-capnproto) - Implementation of the Cap'n Proto serialization protocol.
* [cb0r](https://github.com/quartzjer/cb0r) - Minimal Zero-Footprint CBOR Decoder in C.
* [cereal](https://github.com/USCiLab/cereal) - A C++11 library for serialization.
* [cista](https://github.com/felixguendling/cista) - Cista is a simple, high-performance, zero-copy C++ serialization & reflection library.
* [cmp](https://github.com/camgunz/cmp) - Implementation of the [MessagePack](https://msgpack.org/) serialization protocol.
* [cppcodec](https://github.com/tplgy/cppcodec) - Header-only C++11 library to encode/decode base64, base32 and hex with consistent, flexible API.
* [cppack](https://github.com/mikeloomisgg/cppack) - A modern c++ implementation of the msgpack spec. - [msgpack.org](msgpack.org) (C++17)
* [EMV-tag-parser](https://github.com/misuher/EMV-tag-parser) - Generate a readable output from a string of tags EMV for payment systems.
* [fastbase64](https://github.com/lemire/fastbase64) - SIMD-accelerated base64 codecs.
* [FastBinaryEncoding](https://github.com/chronoxor/FastBinaryEncoding) - Fast Binary Encoding is ultra fast and universal serialization solution for C++, C#, Go, Java, JavaScript, Kotlin, Python, Ruby, Swift.
* [FlatBuffers](https://github.com/google/flatbuffers) - A Memory efficient serialization library.
* [Fleece](https://github.com/couchbase/fleece) - A super-fast, compact, JSON-equivalent binary data format.
* [HDF5](https://github.com/HDFGroup/hdf5) - HDF5 (Hierarchical Data Format version 5) is a data model, file format, and software library designed
for efficient storage and management of large and complex datasets.
	* [h5pp](https://github.com/DavidAce/h5pp) - A C++17 interface for HDF5.
* [hps](https://github.com/jl2922/hps) - High Performance C++11 Serialization Library.
* [iguana](https://github.com/qicosmos/iguana) - Universal serialization engine.
* [Kaitai Struct](http://kaitai.io) - A declarative language to describe various binary data structures and a compiler to generate C++ parser code.
* [libavro](http://avro.apache.org/docs/current/api/c/index.html#_introduction_to_avro_c) - Implementation of the Avro data serialization system.
* [libbaseencode](https://github.com/paolostivanin/libbaseencode) - Library written in C for encoding and decoding data using base32 or base64.
* [libcbor](https://github.com/PJK/libcbor) - CBOR protocol implementation for C and others.
* [libtlv](https://github.com/christianhujer/libtlv) - A TLV (Tag Length Value) library inspired by (but not the same) BER-TLV and UTF-8.
* [libxo](https://github.com/Juniper/libxo) - Allows an application to generate plain text, XML, JSON and HTML output using a common set of function calls. The application decides at runtime what output style should be produced.
* [libzim](https://github.com/openzim/libzim) - The Libzim is the reference implementation for the [ZIM file format](https://wiki.openzim.org/wiki/ZIM_file_format).
* [MessagePack](https://github.com/msgpack/msgpack-c) - Efficient binary serialization format "like JSON" for C and C++. [website](http://msgpack.org/)
* [mpack](https://github.com/ludocode/mpack) - Another implementation of the [MessagePack](https://msgpack.org) serialization protocol. 
* [msgpuck](https://github.com/rtsisyk/msgpuck) - MsgPuck is a compact and efficient [MessagePack](https://msgpack.org) serialization library.
* [mrpt-serialization](https://github.com/mrpt/mrpt/) - Versioned serialization to binary or text formats.
* [nanopb](https://github.com/nanopb/nanopb) - Small code-size Protocol Buffers implementation in ANSI C.
* [OPIC](http://opic.rocks/) - - Object Persistence in C, a revolutionary serialization framework, with matching on-disk and in-memory representations.
* [pboctlv](https://github.com/redxu/pboctlv) - EMV TLV Data parse.
* [protobuf](https://github.com/protocolbuffers/protobuf) - Protocol Buffers - Google's data interchange format.
* [protobuf-c](https://github.com/protobuf-c/protobuf-c) - Protocol Buffers implementation in C.
* [Protocol Puffers](https://github.com/PragmaTwice/protopuf) - A little, highly templated, and protobuf-compatible serialization/deserialization header-only library written in C++20.
* [SimpleBinaryEncoding](https://github.com/real-logic/simple-binary-encoding) - encoding and decoding application messages in binary format for low-latency applications.
* [tpl](https://github.com/troydhanson/tpl) - Small binary serialization library.
* [upb](https://github.com/protocolbuffers/upb) - A small protobuf implementation in C.
* [varint.c](https://github.com/tidwall/varint.c) - Read and write variable sized integers in C. Compatible with Protobuf and Go varints.
* [Wirehair](https://github.com/catid/wirehair) - O(N) Fountain Code for Large Data.
* [YAS](https://github.com/niXman/yas) - Very fast **Y**et **A**nother **S**erialization library supports the binary/text/json formats.
* [xpack](https://github.com/xyz347/xpack) - convert json/xml/bson to c++ struct.
* [zpp_bits](https://github.com/eyalz800/zpp_bits) - Actually, the fastest modern **S**erialization library. Just check [this video](https://www.youtube.com/watch?v=G7-GQhCw8eE&ab_channel=CppCon).

### BER TLV

* [BER-TLV](https://github.com/huckor/BER-TLV) - BER-TLV, Basic Encoding Rules library.
* [BER_TLV](https://github.com/Vedrov/BER_TLV) - Программа декодирует EMV теги записанные в TLV формате.
* [BER TLV Encoder and Decoder](https://github.com/AliceOh/Constructed-BER-TLV-Encoder-and-Decoder) - Constructed BER-TLV Encoder and Decoder in C.
* [BerTlvParser](https://github.com/mkaikkonen/BerTlvParser) - BER TLV Library and QT UI for handling BER TLV data.
* [bertlv](https://github.com/iceignatius/bertlv) - This is a data processor of Basic Encoding Rules (BER) format of TLV data.
* [ber-tlv](https://github.com/sandropjacobsen/ber-tlv) - BER TLV parser.
* [Ber-TLV](https://github.com/HosseinAssaran/Ber-TLV) - TLV Parser and Builder With C.
* [libtlv](https://github.com/toumilov/libtlv) - X.690 BER TLV library for C++.

## Serial Port

* [Asio](https://github.com/chriskohlhoff/asio/) - Asio includes classes for creating and manipulating serial ports in a portable manner. [website](http://think-async.com/)
* [Boost.Asio](https://github.com/boostorg/asio) - Boost.Asio includes classes for creating and manipulating serial ports in a portable manner. [website](https://boost.org/libs/asio)
* [CSerialPort](https://github.com/itas109/CSerialPort) - lightweight cross-platform serial port library.
* [Libserial](https://github.com/crayzeewulf/libserial) - Serial Port Programming in C++.
* [Serial Communication Library](https://github.com/wjwwood/serial) - Cross-platform, Serial Port library written in C++.

## Server

* [cloud-server](https://github.com/tacr-iotcloud/cloud-server) - Server for IoT gateways management via HTTP REST API.

## Sorting

+ [cpp-sort](https://github.com/Morwenn/cpp-sort) - Sorting algorithms & related tools for C++14.
* [pdqsort](https://github.com/orlp/pdqsort) - Pattern-defeating quicksort.
* [Timsort](https://github.com/gfx/cpp-TimSort) - A templated stable sorting function which outperforms quicksort-based algorithms including std::sort, for reversed or semi-sorted data.
* [Indiesort](https://github.com/mattreecebentley/plf_indiesort) - A sort wrapper which enables the use of std::sort (& other random-access sort functions) with non-random-access containers, and also improves sort performance for large/non-trivially-copyable types in random-access containers and arrays. [website](https://plflib.org/indiesort.htm)

## String Manipulation ##

* [Better String](http://bstring.sourceforge.net) - An alternative to the string library for C which is more functional and does not have buffer overflow overrun problems. Also includes a C++ wrapper.
* [dragonbox](https://github.com/jk-jeon/dragonbox) - Dragonbox is a float-to-string conversion algorithm based on a beautiful algorithm [Schubfach](https://drive.google.com/open?id=1luHhyQF9zKlM8yJ1nebU0OgVYhfC6CBN), developed by Raffaello Giulietti in 2017-2018. Dragonbox is further inspired by [Grisu](https://www.cs.tufts.edu/~nr/cs257/archive/florian-loitsch/printf.pdf) and [Grisu-Exact](https://github.com/jk-jeon/Grisu-Exact).
* [fast_double_parser](https://github.com/lemire/fast_double_parser) - Fast function to parse strings into double (binary64) floating-point values, enforces the RFC 7159 (JSON standard) grammar: 4x faster than strtod.
* [fast_float](https://github.com/fastfloat/fast_float) - Fast and exact implementation of the C++ from_chars functions for float and double types: 4x faster than strtod, part of GCC 12.
* [houdini](https://github.com/vmg/houdini) - Houdini is a simple API for escaping text for the web. And unescaping it. But that kind of breaks the joke in the name so nevermind.
* [ICU](http://site.icu-project.org/) - International Components for Unicode; a library for Unicode support.
* [itoa](https://github.com/jeaiii/itoa) - Fast integer to ascii / integer to string conversion.
* [libunistring](https://www.gnu.org/software/libunistring/) - Library for manipulating Unicode strings in C.
* [libgiconv](https://www.gnu.org/software/libiconv/) - Text conversion library.
* [match.c](https://github.com/tidwall/match.c) - A very simple pattern matcher where '*' matches on any number characters and '?' matches on any one character.
* [nanoizepp](https://github.com/marty1885/nanoizepp) - Dependency-free C++ 20 library to minimize HTML.
* [ryu](https://github.com/tidwall/ryu) - Convert floating-point numbers to strings in their shortest, most accurate representation.
* [SDS](https://github.com/antirez/sds) - Simple Dynamic Strings; a library for handling C strings in a simpler way, but one that is compatible with normal C string functions. Available via [clib](https://github.com/clibs/clib).
* [shoco](http://ed-von-schleck.github.io/shoco/) - Compressor for small text strings.
* [smaz](https://github.com/antirez/smaz) - Efficient string compression library.
* [StringZilla](https://github.com/ashvardanian/StringZilla) - 10x faster string search, split, sort, and shuffle for long strings and multi-gigabyte files in Python and C, leveraging SIMD with Arm Neon and x86 AVX2 & AVX-512 intrinsics.
* [String](https://github.com/KaisenAmin/String) - CStringEnhancer is a small C library that provides additional string manipulation functions.
* [tstr](https://github.com/tidwall/tstr) - A safe immutable string format for C.
* [utf8.h](https://github.com/sheredom/utf8.h) - Single header utf8 string functions.
* [utf8proc](https://github.com/JuliaStrings/utf8proc) - Small, clean library for processing UTF-8 Unicode data.

## Template Engines

*Libraries and tools for templating and lexing.*

* [amps](https://github.com/jrziviani/amps) - Amps - Simple Template Engine for C++.
* [bustache](https://github.com/jamboree/bustache) - C++20 implementation of {{ mustache }}
* [boostache](https://github.com/cierelabs/boostache) - Boostache is a text template processing engine. The initial release uses the [Mustache](http://mustache.github.io/) template language.
* [cJinja](https://github.com/HuangHongkai/cJinja) - A lightweight C++ HTML template engine.
* [complate-cpp](https://github.com/tmehnert/complate-cpp) - Rendering of JSX based views in C++ [complate](https://complate.org/)
* [cpp-mustache](https://github.com/henryr/cpp-mustache) - C++ implementation of mustache template engine with support for RapidJSON.
* [cpptl](https://github.com/nekipelov/cpptl) - C++ Template Library.
* [ginger](https://github.com/melpon/ginger) - C++ Template Engine for HTML/CSS/JavaScript.
* [hypertextcpp](https://github.com/kamchatka-volcano/hypertextcpp) - Fast HTML template engine using C++ code generation.
* [inja](https://github.com/pantor/inja) - A Template Engine for Modern C++.
* [Jinja2С++](https://github.com/jinja2cpp/Jinja2Cpp) - C++ implementation of the Jinja2 Python template engine. This library brings support of powerful Jinja2 template features into the C++ world, reports dynamic HTML pages and source code generation. [website](https://jinja2cpp.github.io/)
* [libmustache](https://github.com/jbboehr/libmustache) - C++ implementation of Mustache.
* [minja.hpp](https://github.com/google/minja) - A minimalistic C++ Jinja templating engine for LLM chat templates.
* [Mustache](https://github.com/kainjow/Mustache) - Mustache text templates for modern C++.
* [mustache](https://github.com/xelia-it/mustache) - Mustache-like template engine for C++11.
* [mustache](https://github.com/funlibs/mustache) - C mustache library with focus on portability and performances.
* [mustache-janet](https://github.com/beleon/mustache-janet) - Mustache module for janet via [mustach](https://gitlab.com/jobol/mustach) and [cJSON](https://github.com/DaveGamble/cJSON), based on [pymustach](https://github.com/RekGRpth/pymustach) mustach bindings.
* [Mold](https://github.com/duzy/mold) - Mold is a C++ Text Engine. It's a header-only library for generating text from a template and a set of data (feed).
* [mstch](https://github.com/no1msd/mstch) - Complete implementation of {{mustache}} templates using modern C++.
* [RedZone](https://github.com/jcfromsiberia/RedZone) - Lightweight C++ template engine with Jinja2-like syntax.
* [see-phit](https://github.com/rep-movsd/see-phit) - A C++ HTML template engine that uses compile time HTML parsing.
* [string_template](https://github.com/lexxmark/string_template) - Simple and small string template engine in C++17.
* [templ](https://github.com/Urquelle/templ) - Jinja2 C++ implementation.
* [TENG](https://github.com/seznam/teng) - TEng is a general purpose templating engine for C++ and Python. Especially useful for C++ apps that need to render dynamic HTML content.
* [tinja](https://github.com/mincequi/tinja) - Very fast and memory efficient template engine using C++17, (also) designed for microcontrollers (MCUs).
* [ucode](https://github.com/jow-/ucode) - Jinja-like micro templating.
* [Qentem Engine](https://github.com/HaniAmmar/Qentem-Engine) - About Fast JSON, templating engine, Arithmetic and logic evaluation. Pure C++.

### HTML template

* [puikinsh](https://github.com/puikinsh)
* [mazer](https://github.com/zuramai/mazer) - Free and Open-source Bootstrap 5 Admin Dashboard Template and Landing Page.
* [voler](https://github.com/zuramai/voler) - The first Bootstrap 5 admin dashboard template.
* [](https://github.com/frontendfunn/bootstrap-5-admin-dashboard-template)
* [Focus Admin Dashboard Template](https://github.com/themefisher/focus-bootstrap) - Focus - Open Source Admin Dashboard Template.
* [Tailwind Admin Template](https://github.com/davidgrzyb/tailwind-admin-template) - An admin dashboard template built with Tailwind and Alpine.js
* [kwd-dashboard](https://github.com/Kamona-WD/kwd-dashboard) - Fully responsive dashboard template built with tailwindcss & alpinejs
* [Responsive Admin Dashboard](https://github.com/BobsProgrammingAcademy/responsive-admin-dashboard)
* [qbadminui](https://github.com/qbytesoft-com/qbadminui)
* [adminify](https://github.com/swapan730/adminify)
* [sveltekit-vali-admin](https://github.com/suryamsj/sveltekit-vali-admin)
* [Admin Dashboards](https://github.com/admin-dashboards/open-source)
* [nion-admin](https://github.com/rajibchandrakarmaker/nion-admin)
* [admin-one-tailwind]https://github.com/justboil/admin-one-tailwind
* [soft-ui-dashboard-tailwind]https://github.com/creativetimofficial/soft-ui-dashboard-tailwind
* [Hotel-Website-Template](https://github.com/LjoveX/Hotel-Website-Template) - Free to use website template,created from scratch using HTML and CSS
* [E-Commerce Store](https://github.com/hannesxc/ecommerce-store)
* [Responsive web sites](https://github.com/bedimcode)
* [MDBootstrap](https://github.com/mdbootstrap)
* [Glowing - eCommerce Website](https://github.com/codewithsadee/glowing)
* [Ecommerce Website Template B4](https://github.com/MehedilslamRipon/Ecommerce-Website-Template-B4) - This is an E-commerce Website Template created with bootstrap 4
* [Grandmas Sweets Project B4](https://github.com/MehedilslamRipon/Grandmas-Sweets-Project-B4) - A stylish, one page, Bootstrap Sweets shop theme featuring off awesome navigation and amazing js cart
* [E-Commerce-Template](https://github.com/bhaumikpatel/E-Commerce-Template) - E-Commerce Template Built with React JS, Bootstrap 5
* [Phone Website](https://github.com/wpcodevo/phone_website) - An eCommerce website template built with HTML, CSS, and JAVASCRIPT.
* [Furniture Website](https://github.com/withaarzoo/Furniture-Website) - This is a multipage furniture website built using HTML, CSS, and JavaScript. It features a clean and modern design, with multiple pages to showcase different types of furniture.
* [Kimi](https://github.com/philipherlambang/kimi) - Kimi Theme Based on Bootstrap v 3.x
* [Groover Responsive E-commerce Template](https://github.com/ahmadhuss/groover-free-premium-ecommerce-template) - Groover Responsive E-commerce Template - 26+ Pages, Blog Module.
* [Ludus ecommerce template](https://github.com/ahmadhuss/ludus-free-premium-ecommerce-template)
* [Responsive-Ecommerce-Website](https://github.com/anubhav199/Responsive-Ecommerce-Website)
* [Hotel](https://github.com/sumitmangela/hotel)
* [oldskool-html-bootstrap](https://github.com/PixelRocket-Shop/oldskool-html-bootstrap) - OldSkool is a clean and minimal responsive HTML Bootstrap 5 Ecommerce fashion store template.
* [ui](https://github.com/atherosai/ui) - Simple UI examples from my social media.

## UUID

*Libraries for working with UUIDs.*

* [crossguid](https://github.com/graeme-hill/crossguid) - Lightweight cross platform C++ GUID/UUID library.
* [libstud-uuid](https://github.com/libstud/libstud-uuid) - Portable UUID generation library for C++.
* [libxid](https://github.com/uatuko/libxid) - A globally unique id generator.
* [sole](https://github.com/r-lyeh-archived/sole) - Sole is a lightweight C++11 library to generate universally unique identificators (UUID), both v1 and v4.
* [sqids-cpp](https://github.com/sqids/sqids-cpp) - Official C++ port of Sqids. Generate short unique IDs from numbers. [sqids.org/cpp](https://sqids.org/cpp)
* [stduuid](https://github.com/mariusbancila/stduuid) - C++17 cross-platform single-header library implementation for universally unique identifiers, simply know as either UUID or GUID (mostly on Windows).
* [uuid](https://github.com/boostorg/uuid) - Boost.org uuid module.
* [uuid](https://github.com/rtlayzell/uuid) - A small header only C++ library for handling Universally Unique ID's (UUID).
* [uuid_v4](https://github.com/crashoz/uuid_v4) - Super fast C++ library to generate and parse UUIDv4.


## Validation

* [cpp-validator](https://github.com/evgeniums/cpp-validator) - C++ header-only library for generic data validation.
* [json-schema](https://github.com/hotkit/json-schema) - JSON schema validation.
* [json-schema-validator](https://github.com/pboettch/json-schema-validator) - JSON schema validator for JSON for Modern C++.
* [protovalidate-cc](https://github.com/bufbuild/protovalidate-cc) - Protocol Buffer Validation for C++.
* [valijson](https://github.com/tristanpenman/valijson) - Header-only C++ library for JSON Schema validation, with support for many popular parsers.
* [yavl-cpp](https://github.com/psranga/yavl-cpp) - YAML Validation Data Binding Library in C++.

## Video

* [avcpp](https://github.com/h4tr3d/avcpp) - Modern C++ wrapper around FFmpeg.
* [libvpx](http://www.webmproject.org/code/) - VP8/VP9 Codec SDK.
* [FFmpeg](https://www.ffmpeg.org/) - A complete, cross-platform solution to record, convert and stream audio and video.
* [libde265](https://github.com/strukturag/libde265) - Open h.265 video codec implementation. [website](http://www.libde265.org/)
* [x265](https://bitbucket.org/multicoreware/x265/wiki/Home) - Open h.265 video codec implementation. [website](https://x265.readthedocs.io/en/default/)
* [OpenH264](https://github.com/cisco/openh264) - Open Source H.264 Codec. [website](http://www.openh264.org/)
* [SRS](https://github.com/ossrs/srs) - SRS is a simple, high-efficiency, real-time video server supporting RTMP, WebRTC, HLS, HTTP-FLV, SRT, MPEG-DASH, and GB28181.
* [Theora](http://www.theora.org/) - A free and open video compression format.
* [Vireo](https://github.com/twitter/vireo/) - A lightweight and versatile video processing library by Twitter.

## Virtual Machines

* [CarpVM](https://github.com/tekknolagi/carp) - "interesting" VM in C. Let's see how this goes.
* [MicroPython](https://github.com/micropython/micropython) - Aims to put an implementation of Python 3.x on a microcontroller.
* [TinyVM](https://github.com/jakogut/tinyvm) - A small, fast, lightweight virtual machine written in pure ANSI C.

## VPN

* [amnezia-client](https://github.com/amnezia-vpn/amnezia-client) - Amnezia VPN Client (Desktop+Mobile)


## Web Application Framework

* [acl](https://github.com/acl-dev/acl) - Server framework and network components written by C/C++ for Linux, Mac, FreeBSD, Solaris(x86), Windows, Android, IOS.
* [Agoo-C](https://github.com/ohler55/agoo-c) - Agoo webserver and GraphQL server in C with the goal of being the best performing webserver and GraphQL server.
* [cepresso](https://github.com/thechampagne/cepresso) - A mini web framework for C. It's written on top of [httpserver.h](https://github.com/jeremycw/httpserver.h).
* [cerveur](https://github.com/infraredCoding/cerveur) - A HTTP Web Server Framework Written In C (Just For Fun).
* [cfadmin](https://github.com/cfadmin-cn/cfadmin) - A lua web network framework.
* [Cherokee](https://github.com/cherokee/webserver) - Cherokee Web Server.
* [cinatra](https://github.com/qicosmos/cinatra) - Modern C++ (C++20), cross-platform, header-only, easy to use http framework.
* [Civetweb](https://github.com/civetweb/civetweb) - Provides easy to use, powerful, C/C++ embeddable web server with optional CGI, SSL and Lua support.
* [C++ REST SDK](https://github.com/Microsoft/cpprestsdk) - Microsoft project for cloud-based client-server communication in native code using a modern asynchronous C++ API design.
* [C++ wfrest](https://github.com/wfrest/wfrest) - C++ Web Framework REST API.
* [CppCMS](http://cppcms.com/) - A Free High Performance Web Development Framework (not a CMS).
* [CPPWebFramework](https://github.com/HerikLyma/CPPWebFramework) - The C++ Web Framework (CWF) is a MVC web framework, Open Source, under MIT License, using C++ with Qt to be used in the development of web applications.
* [cpv-framework](https://github.com/cpv-project/cpv-framework) - cpv framework is a web framework written in c++ based on [seastar framework](https://github.com/scylladb/seastar), it focus on high performance and modular design.
* [Crow](https://github.com/CrowCpp/Crow) - Crow is C++ micro web framework (inspired by Python Flask).
* [cuehttp](https://github.com/xcyl/cuehttp) - Modern C++ middleware framework for http(http/https)/websocket(ws/wss).
* [Cutelyst](https://github.com/cutelyst/cutelyst) - A C++ Web Framework built on top of Qt, using the simple approach of Catalyst (Perl) framework.
* [darkhttpd](https://github.com/emikulic/darkhttpd) - When you need a web server in a hurry.
* [duda](http://duda.io/) - Duda I/O is an event-driven and high performant web services framework written in C.
* [Drogon](https://github.com/an-tao/drogon) - A C++14/17 based, high-performance HTTP application framework.
    * [AMQP RabbitMQ Drogon Plugin](https://github.com/timulations/amqp_drogon) - A Drogon Plugin that integrates AMQP-CPP into the main Drogon event loop, utilising Trantor for TCP communications with AMQP service.
    * [anya-cxx](https://github.com/rishiagl/anya-cxx) - C++ Web Backend.
	* [backend_cpp](https://github.com/asman1337/backend_cpp) - Prototype v1 backend for CodingVeda project.
	* [bs-drogon](https://github.com/lauset/bs-drogon) - Drogon WebSocket Application.
	* [cpp-backend-login-example](https://github.com/xstackman/cpp-backend-login-example) - Backend login in C++ with Drogon Framework.
	* [cmd-terminal](https://github.com/klc407073648/cmd-terminal)
	* [demo_orm_drogon](https://github.com/waleko/demo_orm_drogon) - ORM demo.
	* [DevBankWithDrogonFramework](https://github.com/Lipe1994/DevBankWithDrogonFramework)
	* [dg_stream](https://github.com/fyuuki0jp/dg_stream) - drogon + openmaxil used h264 streamer
    * [Dremini](https://github.com/marty1885/dremini) - Highly parallel and concurrent Gemini server and client library for the Drogon web application framework.
	* [drogonframework.phaser](https://github.com/prothegee/drogonframework.phaser) - A simple example of drogonframework with [phaser](https://www.phaser.io/) & [svelte](https://svelte.dev/) integration.
	* [DroCoMS](https://github.com/xyious/DroCoMS) - Blog/website using drogon framework.
	* [drogon-api](https://github.com/ccezar25/drogon-api)
	* [drogon-boilerplate](https://github.com/alfishe/drogon-boilerplate) - C++ Boilerplate starter for Drogon framework-based websevice.
	* [drogon-chat-api](https://github.com/xD8A/drogon-chat-api)
	* [Drogon-demo](https://github.com/textcunma/Drogon-demo)
	* [drogon-echo-ddos](https://github.com/myks790/drogon-echo-ddos)
	* [drogon-http](https://github.com/AmdRyZen/drogon-http) - Cpp20 based HTTP web application.
	* [drogon_jsonrpc](https://github.com/hwc0919/drogon_jsonrpc) - jsonrpc in drogon with both http and websocket.
	* [Drogon-JWT-Filter-example](https://github.com/0rangeFox/Drogon-JWT-Filter-example) - A simple project to serve as an example of how to implement an JWT filter on Drogon framework.
	* [drogon_orm_prj](https://github.com/d9magai/drogon_orm_prj)
	* [drogon-realworld-example-app](https://github.com/arslan2012/drogon-realworld-example-app) - Example Drogon codebase containing real world examples (CRUD, auth, advanced patterns, etc) that adheres to the RealWorld API spec.
	* [drogon-redis-async-demo](https://github.com/goreycn/drogon-redis-async-demo)
	* [drogon-rest-api](https://github.com/matheusdearaujo/drogon-rest-api) - A project template to Drogon C++ Framework.
	* [drogon-svelte-todo-app](https://github.com/deltine/drogon-svelte-todo-app)
	* [Drogon-torch-serve](https://github.com/SABER-labs/Drogon-torch-serve) - Serve pytorch / torch models using Drogon.
	* [drogon_user_service](https://github.com/sartim/drogon_user_service) - User service running on Drogon Framework which handles RBAC management.
	* [drogon-user](https://github.com/Mis1eader-dev/drogon-user) - User extension on top of drogon web framework.
	* [drogon-web](https://github.com/minhhoai1001/drogon-web) - Example with Json validation.
	* [drogon_blog](https://github.com/vodoanminhhieuvn/drogon_blog) - The Blog Backend Drogon Project is a RESTful API that provides the backend for the Client App.
	* [drogon_channel](https://github.com/hwc0919/drogon_channel) - Experiment on channel for drogon.
	* [drogon-orm-stack-overflow](https://github.com/ivanka2012/drogon-orm-stack-overflow)
	* [drogon-client-test](https://github.com/marty1885/drogon-client-test) - A quick stress test tool to test drogon's HTTP server and client working together.
	* [DrogonOnlineStore](https://github.com/Anikievich/DrogonOnlineStore)
	* [DrogonRest](https://github.com/cppshizoidS/DrogonRest)
	* [drogonS](https://github.com/hezronkimutai/drogonS) - Drogon Examples.
	* [DrogonSQL](https://github.com/prothegee/DrogonSQL) - Drogon framework SQL example.
	* [DrogonWebApi](https://github.com/k-nero/DrogonWebApi) - Simple drogoncpp web api server with builtin entity manager.
	* [HDA_with_Cpp](https://github.com/brakmic/HDA_with_Cpp) - Hypermedia-driven application based on htmx and Drogon C++ web framework.
	* [eCommerceWeb](https://github.com/HadesD/eCommerceWeb) - E-Commerce website written by Drogon C++ framework and vuejs.
	* [eureka_drogon](https://github.com/timulations/eureka_drogon) - A minimal Drogon plugin for supporting service discovery via Eureka.
	* [ElasticSearchClient](https://github.com/tanglong3bf/ElasticSearchClient) - ElasticSearchClient is a Drogon plugin that can interact with the ElasticSearch server in a more concise way.
	* [ForDrogon](https://github.com/1111mp/ForDrogon) - A project for learn Drogon.
	* [helloworld-rest-cpp](https://github.com/scheca/helloworld-rest-cpp) - Example of a REST service using Drogon library in C++. (Dockerized Hello World with Drogon)
	* [HEVoting](https://github.com/S-Ptr/he-voting) - Online voting system that uses homomorphic encryption.
	* [hydenjekyll.org](https://github.com/oDinZu/hydenjekyll.org) - A hyde n' jekyll static admin dashboard or frontend for a Drogon backend. This frontend admin dashboard works in alignment with Jekyll's static site generator.
	* [koinonia](https://github.com/urlordjames/koinonia) - Screen sharing app using WebRTC with a signalling layer using the drogon web framework.
	* [Libreism](https://github.com/organization/Libreism) - Open source server time viewer written in C++.
	* [login_session](https://github.com/albaropereyra22/login_session) - Drogon example of login session.
	* [Muelsyse](https://github.com/tanglong3bf/Muelsyse) - Drogon plugin for simplifying RPC operations.
	* [MVC](https://github.com/smyfrank/MVC) - An example of Model–View–Controller pattern using Drogon Framework.
	* [MVC_DrogonHtmx](https://github.com/fullzer4/MVC_DrogonHtmx) - Speedy Web Development with Drogon C++ and Htmx in an MVC Architecture.
	* [orgChartApi](https://github.com/maikeulb/orgChartApi) - RESTful API written in C++ using Drogon HTTP application framework.
	* [opendelivery](https://github.com/promotedai/opendelivery) - Promoted.ai Open and Source Available search and ranking service in C++
	* [ProtobuffedDrogon](https://github.com/MuriloChianfa/ProtobuffedDrogon) - PROTOBUF implementation with DROGON framework.
	* [restapi-boilerplate](https://github.com/sirasjad/restapi-boilerplate) - REST API boilerplate code using Drogon C++ HTTP Framework, MongoDB and OpenID Connect.
	* [Share-the-Sky](https://github.com/bridge71/Share-the-Sky) - File-sharing system created by drogon.
	* [simple-finance-backend](https://github.com/N0I0C0K/simple-finance-backend) - 
	* [SMTPMail-drogon](https://github.com/an-tao/SMTPMail-drogon) - Simple Mail plugin for the Drogon framework.
	* [Spartoi](https://github.com/marty1885/spartoi) - Spartoi - Highly parallel and concurrent Spartan server and client library for the Drogon web application framework.
	* [rinha-crebito-cpp](https://github.com/MuriloChianfa/rinha-crebito-cpp)
	* [teatro-rewrite](https://github.com/Gelbpunkt/teatro-rewrite) - Rewrite of teatro in C++ using drogon.
	* [tegra](https://github.com/genyleap/tegra) - Tegra is an open source super high-end performance, user-friendly, multi-lingual and multi-purpose modern system based on C++20 which enables you to build websites and powerful online applications.	
	* [tlgs](https://github.com/marty1885/tlgs) - "Totally Legit" Gemini Search - Open source search engine for the Gemini protocol.
	* [traq-client-drogoncpp](https://github.com/n-kegra/traq-client-drogoncpp)
	* [rng](https://github.com/eranik/rng) - A simple service to generate random numbers using modern Drogon C++14/17 microservices framework.
	* [rvs](https://github.com/mjaehaer/rvs) - drogon + redis example.
	* [web_server](https://github.com/f0rmul/web_server) - Modern RESTful web-server based on Drogon Http Framework. Parse incoming form values.
* [facil.io](https://github.com/boazsegev/facil.io) - Evented, high performance C web framework supporting HTTP, WebSockets, SSE and more. [website](http://facil.io)
* [fcgiwrap](https://github.com/gnosek/fcgiwrap) - Simple FastCGI wrapper for CGI scripts.
* [Guacamole Server](https://github.com/apache/guacamole-server) - Mirror of Apache Guacamole Server.
* [janus-gateway](https://github.com/meetecho/janus-gateway) - Janus is an open source, general purpose, WebRTC server.
* [hinsightd](https://github.com/tiotags1/hin9) - hinsightd is a HTTP/1.1 webserver designed to be light on resources but not light on features. It uses the new linux async API io_uring for network and file access.
* [http-server](https://github.com/mattn/http-server) - A fast http server written in C.
* [ivykis-http](https://github.com/bazsi/ivykis-http) - ivykis http server.
* [kcgi](https://kristaps.bsd.lv/kcgi) - CGI and FastCGI library for C.
* [KLone](http://www.koanlogic.com/klone) - Fully featured, multi-platform, web application development framework, targeted especially at embedded systems and appliances.
* [Kore](https://kore.io/) - ultra fast and flexible web server / framework for web applications developed in C.
	* [Kore mustach](https://github.com/cavalo-mORTO/kore_mustach) - Integration of mustache templates with kore.
	* [Telize](https://github.com/fcambus/telize) - Telize is a REST API built in C with Kore allowing to get a visitor IP address and to query location information from any IP address. It outputs JSON-encoded IP geolocation data, and supports both JSON and JSONP. Geolocation operations are performed using libmaxminddb which caches the database in RAM. Therefore, Telize has very minimal overhead and should be blazing fast.
* [libasyncd](https://github.com/wolkykim/libasyncd) - Embeddable Event-based Asynchronous Message/HTTP Server library for C/C++.
* [libOnion](http://www.coralbits.com/libonion/) - lightweight library to help you create webservers in C programming language.
* [libuhttpd](https://github.com/zhaojh329/libuhttpd) - A very flexible, lightweight and fully asynchronous HTTP server library based on [libev](http://software.schmorp.de/pkg/libev.html) and [http-parser](https://github.com/nodejs/http-parser) for Embedded Linux.
* [libsniper](https://github.com/rtbtech/libsniper) - Super fast HTTP client/server C++ framework.
* [lithium](https://github.com/matt-42/lithium) - Easy to use C++17 HTTP Server with no compromise on performances.
* [lwan](https://github.com/lpereira/lwan) - Experimental, scalable, high performance HTTP server.
* [libzmq](https://github.com/zeromq/libzmq) - Core ZeroMQ library, a high-performance asynchronous messaging library, aimed at use in distributed or concurrent applications. C API (backend C++)
* [Naxsi](https://github.com/nbs-system/naxsi) - NAXSI means Nginx Anti [XSS](https://www.owasp.org/index.php/Cross-site_Scripting_%28XSS%29) & [SQL Injection](https://www.owasp.org/index.php/SQL_injection).
* [neco-http-server](https://github.com/mattn/neco-http-server) - Simple HTTP server using [neco](https://github.com/tidwall/neco)
* [oat++](https://github.com/oatpp/oatpp) - Light, zero-dependency framework to create high-performance web-services. [website](https://oatpp.io/)
* [Phorklift](https://github.com/Phorklift/phorklift) - Phorklift is an HTTP server and proxy daemon, with clear, powerful and dynamic configuration.
* [Pistache](http://pistache.io/) - Pistache is a C++ REST framework written in pure C++11 with no external dependency.
* [paozhu](https://github.com/hggq/paozhu) - paozhu C++ Web Framework,Support HTTP1 HTTP2, Rapid development of CRUD web applications, Include modules HttpServer HttpClient WebSocket ORM.
* [RabbitServer](https://github.com/Super-long/RabbitServer) - A high-performance web server based on HTTP request implemented in C++.
* [served](https://github.com/meltwater/served) - Served is a C++ library for building high performance RESTful web servers.
* [suil](https://github.com/dccarter/suil) - Suil C++ Framework - a mordern C++ micro framework packaged in various libraries that can be used to develop micro service components.
* [suika](https://github.com/ChyauAng/suika) - High Performance C++ Reactor Web Server from Scratch.
* [QDjango](https://github.com/jlaine/qdjango/) - A web framework written in C++ and built on top of the Qt library. Where possible it tries to follow django's API, hence its name.
* [Tinyhttpd](https://github.com/EZLippi/Tinyhttpd) - Tinyhttpd is J. David Blackstone wrote an ultra-lightweight Http Server with less than 500 lines in 1999. It is very good for learning and can help us truly understand the nature of server programs. [website](http://tinyhttpd.sourceforge.net)
* [TinyWebServer](https://github.com/qinguoyi/TinyWebServer) - A lightweight C++ Web server under Linux helps beginners quickly practice network programming and build their own server.
* [TreeFrog Framework](https://github.com/treefrogframework/treefrog-framework) - High-speed and full-stack web application framework based on C++ and Qt, which supports HTTP and WebSocket protocol (with O/R mapping).
* [ulfius](https://github.com/babelouest/ulfius) - HTTP Framework for REST Applications in C.
* [userver](https://github.com/userver-framework/userver) - Asynchronous C++17 framework with a rich set of abstractions and database drivers for fast and comfortable creation of efficient microservices, services and utilities.
	* [battleships-online](https://github.com/alexa0o/battleships-online) - The game based on userver framework.
	* [booking_cpp_userver](https://github.com/trivagoDuo/booking_cpp_userver) - Booking service for hotels, rooms and private homes like booking.com, trivago, etc.
	* [miet-video-backend](https://github.com/dmitriy-korotov/miet-video-backend) - C++ MIET Video backend with using userver framework.
	* [uauth-service](https://github.com/f0rmul/uauth-service) - Simple service that implements JWT-based authentication. Also its based on userver framework.
	* [userver-astra](https://github.com/root-kidik/userver-astra) - build example in Astra Linux docker container.
	* [tinkoff_api_userver](https://github.com/MikhailFDone/tinkoff_api_userver)
	* [Serialization-userver_formats](https://github.com/linuxnyasha/Serialization-userver_formats)
	* [userver-openapi-extension](https://github.com/sabudilovskiy/userver-openapi-extension)
	* [userver-logger](https://github.com/ferdn4ndo/userver-logger) - A lightweight ELK stack using Grafana (instead of Kibana), Loki (instead of ElasticSearch), Promtail (instead of Logstash), and a custom container to monitor the docker environment (acting as a beat).
	* [userverApp](https://github.com/apachaihop/userverApp)
	* [TrivilBiMap](https://github.com/olokreaz/TrivilBiMap) - BiMap implementation from userver-triks.
	* [yet-another-wordle](https://github.com/Repin-Daniil/yet-another-wordle) - API для игры 5 букв, созданное на базе userver framework с PostgreSQL.
	* [unetwork](https://github.com/szvyagin-gj/unetwork) - Network library based on userver.
	* [sessions_management_service](https://github.com/Marilyn-Monroe/sessions_management_service) - 
* [WebServer](https://github.com/linyacool/WebServer) - A C++ High Performance Web Server.
* [Wt](http://www.webtoolkit.eu/wt) - A C++ library for developing web applications.
* [yahttp](https://github.com/PowerDNS/pdns/tree/master/ext/yahttp) - YaHTTP aims to be a pure http request/response parser that has no IO ties. It is intended to be used on small-footprint applications and other utilities that want to use HTTP over something else than network IO.

## WebAssembly

* [duckdb-wasm](https://github.com/duckdb/duckdb-wasm) - WebAssembly version of DuckDB.
* [Emscripten](https://github.com/emscripten-core/emscripten) - An LLVM-to-WebAssembly Compiler.
* [ngx_wasm_module](https://github.com/Kong/ngx_wasm_module) - Nginx + WebAssembly.
* [proxy-wasm-cpp-sdk](https://github.com/proxy-wasm/proxy-wasm-cpp-sdk) - WebAssembly for Proxies (C++ SDK)
* [smkflow](https://github.com/ArthurSonzogni/smkflow) - A C++ dataflow node editor -- Compatible with WebAssembly.
* [wabt](https://github.com/WebAssembly/wabt) - The WebAssembly Binary Toolkit.
* [wasi-sdk](https://github.com/WebAssembly/wasi-sdk) - WASI-enabled WebAssembly C/C++ toolchain.

## XML
*XML is crap. Really. There are no excuses. XML is nasty to parse for humans, and it's a disaster to parse even for computers. There's just no reason for that horrible crap to exist. - Linus Torvalds*

* [Boost.PropertyTree](https://github.com/boostorg/property_tree) - A property tree parser/generator that can be used to parse XML/JSON/INI/Info files.
* [Expat](http://www.libexpat.org/) - An XML parser library written in C.
* [iksemel](https://github.com/meduketto/iksemel) - This is an XML parser library mainly designed for Jabber applications.
It provides SAX, DOM, and special Jabber stream APIs.
* [libexpatpp](https://github.com/gittiver/libexpatpp) - Modern C++ xml parser toolkit using expat.
* [Libxml2](http://xmlsoft.org/) - The XML C parser and toolkit of Gnome.
* [libxml++](http://libxmlplusplus.sourceforge.net/) - An XML Parser for C++.
* [Mini-XML](https://github.com/michaelrsweet/mxml) - A small XML parsing library written in ANSI C. [LGPL2 with exceptions]
* [PugiXML](https://github.com/zeux/pugixml) - A light-weight, simple and fast XML parser for C++ with XPath support. [pugixml.org](http://pugixml.org/)
* [RapidXml](http://rapidxml.sourceforge.net/) - An attempt to create the fastest XML parser possible, while retaining useability, portability and reasonable W3C compatibility.
* [TinyXML](http://sourceforge.net/projects/tinyxml/) - A simple, small, minimal, C++ XML parser that can be easily integrating into other programs.
* [TinyXML2](https://github.com/leethomason/tinyxml2) - A simple, small, efficient, C++ XML parser that can be easily integrating into other programs.
* [TinyXML++](https://github.com/rjpcomputing/ticpp) - A completely new interface to TinyXML that uses MANY of the C++ strengths. Templates, exceptions, and much better error handling.
* [Xalan C](https://github.com/apache/xalan-c) - A library and a command line program to transform XML documents using a stylesheet that conforms to XSLT 1.0 standards. [website](https://xalan.apache.org/)
* [Xerces-C++](http://xerces.apache.org/xerces-c/) - A validating XML parser written in a portable subset of C++.

## Yaml

* [fkYAML](https://github.com/fktn-k/fkYAML) - A C++ header-only YAML library.
* [LibCYAML](https://github.com/tlsa/libcyaml) - C library for reading and writing YAML.
* [libfyaml](https://github.com/pantoniou/libfyaml) - A fancy 1.2 YAML and JSON parser/writer.
* [LibYAML](https://github.com/yaml/libyaml) - A C library for parsing and emitting YAML. [website](https://pyyaml.org/wiki/LibYAML)
* [mini-yaml](https://github.com/jimmiebergmann/mini-yaml) - Single header YAML 1.0 C++11 serializer/deserializer.
* [yaml-cpp](https://github.com/jbeder/yaml-cpp) - is a [YAML](http://www.yaml.org) parser and emitter in C++ matching the [YAML 1.2 spec](http://www.yaml.org/spec/1.2/spec.html)
* [rapidyaml](https://github.com/biojppm/rapidyaml) - Rapid YAML is a C++ library to parse and emit YAML.

## Miscellaneous
*Useful libraries or tools that don't fit in the categories above or maybe just not categorised yet*

* [access_profiler](https://github.com/arvidn/access_profiler) - A tool to count accesses to member variables in c++ programs.
* [American fuzzy lop](http://lcamtuf.coredump.cx/afl/) a.k.a. afl-fuzz - Crazy fuzzing tool that automatically discovers bugs given time and minimal example input.
* [BehaviorTree.CPP](https://github.com/BehaviorTree/BehaviorTree.CPP) - Behavior Trees Library in C++. Batteries included. Even if our main use-case is robotics, you can use this library to build AI for games, or to replace Finite State Machines. [website](https://www.behaviortree.dev/)
* [Boost.Signals2](https://github.com/boostorg/signals2) - An implementation of a managed signals and slots system.
* [btop](https://github.com/aristocratos/btop) - Monitor of resources.
* [casacore](https://code.google.com/p/casacore/) - A set of c++ core libraries derived from aips++.
* [calendar](https://github.com/cassioneri/calendar) - Fast Gregorian calendar algorithms.
* [CCFinder](https://github.com/ind3p3nd3nt/CCFinder) - Recursive Credit Card Searcher For Windows C++ and Linux.
* [CCTZ](https://github.com/google/cctz) - a C++ library for translating between absolute and civil times using the rules of a time zone.
* [Cheat Sheets of HackingCPP](https://hackingcpp.com/cpp/cheat_sheets.html) - Cool cheat sheets and infographics for algorithms, views, containers, randomness, etc.
* [Concord](https://github.com/Cogmasters/concord) - A Discord API wrapper library written in C.
* [cosmopolitan](https://github.com/jart/cosmopolitan) - fast portable static native textmode containers (build C programs for Linux\Mac\Windows in one go).
* [C-Plus-Plus](https://github.com/TheAlgorithms/C-Plus-Plus) - Collection of various algorithms in mathematics, machine learning, computer science and physics implemented in C++ for educational purposes.
* [cpp-util](https://github.com/Stiffstream/cpp-util) - A collection of various helpers for C++. Some of them could be obsolete due to the evolution of C++.
* [cpp-lazy](https://github.com/MarcDirven/cpp-lazy) - a fast and easy lazy evaluation library for C++11/14/17/20.
* [CPPItertools](https://github.com/ryanhaining/cppitertools) - Range-based for loop add-ons inspired by the Python builtins and itertools library.
* [cpu_features](https://github.com/google/cpu_features) - Get CPU features at runtime.
* [CRCpp](https://github.com/d-bahr/CRCpp) - Easy to use and fast C++ CRC library.
* [cxx-prettyprint](https://github.com/louisdx/cxx-prettyprint) - A pretty printing library for C++ containers.
* [D++ (DPP)](https://github.com/brainboxdotcc/DPP) - A lightweight, high performance and scalable C++ library for creating Discord Bots. [website](https://dpp.dev/)
* [DAPHNE](https://github.com/daphne-eu/daphne) - An Open and Extensible System Infrastructure for Integrated Data Analysis Pipelines.
* [Datatype99](https://github.com/Hirrolot/datatype99) - Algebraic data types for C99.
* [date](https://github.com/HowardHinnant/date) - A date and time library based on the C++11/14/17 <chrono> header.
* [DynaMix](https://github.com/iboB/dynamix) - A library which allows you to compose and modify objects at run time.
* [emio](https://github.com/Viatorus/emio) - A safe and fast high-level and low-level character input/output library.
* [ezd](https://github.com/edenhill/ezd) - Making daemonizations easy - single .c+.h files providing the basic tools for daemons.
* [faker-cxx](https://github.com/cieslarmichal/faker-cxx) - C++20 Faker library for generating fake (but realistic) data for testing and development.
* [fast_io](https://github.com/cppfastio/fast_io) - Significantly faster input/output for C++20.
* [FastFormat](http://www.fastformat.org) - Fast, Safe C++ Formatting inspired by log4j and Pantheios.
* [fccf](https://github.com/p-ranav/fccf) - A command-line tool that recursively searches a directory to find C/C++ source code matching a search string.
* [fastfetch](https://github.com/fastfetch-cli/fastfetch) - Like neofetch, but much faster because written mostly in C.
* [{fmt}](https://github.com/fmtlib/fmt) - Small, safe and fast formatting library for C++.
* [QtVerbalExpressions](https://github.com/VerbalExpressions/QtVerbalExpressions) - This Qt lib is based off of the C++ VerbalExpressions library.
* [icecream-cpp](https://github.com/renatoGarcia/icecream-cpp) - A small printing library for debugging purposes.
* [gcc-poison](https://github.com/leafsr/gcc-poison) - A simple header file for developers to ban unsafe C/C++ functions from applications.
* [h3](https://github.com/uber/h3) - H3 is a geospatial indexing system using a hexagonal grid that can be (approximately) subdivided into finer and finer hexagonal grids, combining the benefits of a hexagonal grid with [S2](https://code.google.com/archive/p/s2-geometry-library/)'s hierarchical subdivisions.
* [happly](https://github.com/nmwsharp/happly) - A C++ header-only parser for the PLY file format. Parse .ply happily.
* [hedley](https://github.com/nemequ/hedley) - A C/C++ header file designed to smooth over some platform-specific annoyances. [website](https://nemequ.github.io/hedley/)
* [higress](https://github.com/alibaba/higress) - Next-generation Cloud Native Gateway.
* [JniHelpers](https://github.com/spotify/JniHelpers) - JniHelpers is a library designed to facilitate writing JNI code with C++.
* [io_uring_LPE-CVE-2024-0582](https://github.com/ysanatomic/io_uring_LPE-CVE-2024-0582) - LPE exploit for CVE-2024-0582 (io_uring)
* [incbin][https://github.com/graphitemaster/incbin] - Include binary files in your C/C++ applications with ease.
* [ip2region](https://github.com/lionsoul2014/ip2region) - Ip2region (2.0 - xdb) is a offline IP address manager framework and locator, support billions of data segments, ten microsecond searching performance. xdb engine implementation for many programming languages.
* [Kangaru](https://github.com/gracicot/kangaru) - A dependency injection container for C++11 and C++14.
* [Klib](https://github.com/attractivechaos/klib) - Small and lightweight implementations of common algorithms and data structures.
* [lexbor](https://github.com/lexbor/lexbor) - Lexbor is development of an open source HTML Renderer library. [lexbor.com](https://lexbor.com)
* [libCello](http://libcello.org/) - Library introducing higher-level programming to C.
* [libgraphqlparser](https://github.com/graphql/libgraphqlparser) - A GraphQL query parser in C++ with C and C++ APIs.
* [libsigc++](http://libsigc.sourceforge.net/) - A typesafe callback system for standard C++.
* [libsigcplusplus](https://github.com/libsigcplusplus/libsigcplusplus) - implements a typesafe callback system for standard C++. It allows you to define signals and to connect those signals to any callback function, either global or a member function, regardless of whether it is static or virtual.
* [libcpuid](https://github.com/anrieff/libcpuid) - A small C library for x86 CPU detection and feature extraction.
* [licensepp](https://github.com/abumq/licensepp) - Software licensing using digital signatures.
* [libevil](https://github.com/avati/libevil) - The Evil License Manager.
* [libenvpp](https://github.com/ph3at/libenvpp) - A modern C++ library for type-safe environment variable parsing.
* [libnfc](https://github.com/nfc-tools/libnfc) - Platform-independent Near-Field Communication library.
* [libnih](https://github.com/keybuk/libnih) - A lightweight library of C functions and structures.
* [libONVIF](https://github.com/Privatehive/libONVIF) - Yet another ONVIF library.
* [libphonenumber](https://github.com/google/libphonenumber) - Google's common Java, C++ and JavaScript library for parsing, formatting, and validating international phone numbers.
* [libpopcnt](https://github.com/kimwalisch/libpopcnt) - Fast C/C++ bit population count library.
* [libpostal](https://github.com/openvenues/libpostal) - library for parsing/normalizing street addresses around the world using statistical NLP and open data.
* [librd](https://github.com/edenhill/librd) - Rapid Development C utility library.
* [liburing](https://github.com/axboe/liburing) - This is the io_uring library, liburing. liburing provides helpers to setup and teardown io_uring instances, and also a simplified interface for applications that don't need (or want) to deal with the full kernel side implementation.
* [libusb](https://libusb.info/) - A universal USB library which allows for portable access to USB devices.
* [libusual](https://github.com/greenplum-db/libusual) - Collection of various code useful for writing server code.
* [llfio](https://github.com/ned14/llfio) - P1031 low level file i/o and filesystem library for the C++ standard [ned14.github.io/llfio](https://ned14.github.io/llfio/)
* [Mach7](https://github.com/solodon4/Mach7) - A Pattern-matching library for C++.
* [Metalang99](https://github.com/Hirrolot/metalang99) - Full-blown preprocessor metaprogramming.
* [minilibs](https://github.com/ccxvii/minilibs) - Miniature libraries that consist of a single header and source file.
* [mio](https://github.com/mandreyel/mio) - Cross-platform C++11 header-only library for memory mapped file IO.
* [MPH](https://github.com/qlibs/mph) - C++20 [Minimal] Static Perfect Hash library.
* [oui](https://github.com/zhaojh329/oui) - A modern web interface for OpenWrt implemented in vue.js and Lua.
* [PEGTL](https://github.com/taocpp/PEGTL) - The Parsing Expression Grammar Template Library.
* [Pipes](https://github.com/joboccara/pipes) - Pipelines for expressive code on collections in C++.
* [pprint](https://github.com/p-ranav/pprint) - Pretty Printer for Modern C++.
* [pspsdk](https://github.com/pspdev/pspsdk) - An open-source SDK for PSP homebrew development.
* [RapidFuzz](https://github.com/rapidfuzz/rapidfuzz-cpp) - Rapid fuzzy string matching in C++ using the Levenshtein Distance. [website](https://rapidfuzz.github.io/rapidfuzz-cpp/)
* [rapidhash](https://github.com/Nicoshev/rapidhash) - Very fast, high quality, platform-independent hashing algorithm.
* [Remote Call Framework](http://www.deltavsoft.com/) - Inter-process communication framework for C++.
* [repr](https://github.com/p-ranav/repr) - repr(value) returns a printable string representation of value.
* [rtty](https://github.com/zhaojh329/rtty) - Access your terminal from anywhere via the web.
* [SafetyHook](https://github.com/cursey/safetyhook) - C++23 procedure hooking library.
* [scnlib](https://github.com/eliaskosunen/scnlib) - scanf for modern C++. [website](https://v1.scnlib.dev/)
* [Scintilla](http://scintilla.org/) - A free source code editing component.
* [SDS](https://github.com/antirez/sds) - Simple Dynamic Strings library for C.
* [semver.c](https://github.com/h2non/semver.c) - A semver parser and render in ANSI C.
* [sigslot](http://sigslot.sourceforge.net/) - C++ Signal/Slot Library.
* [sigslot](https://github.com/palacaze/sigslot) - A simple, header only, C++14 signal-slots implementation.
* [simdzone](https://github.com/NLnetLabs/simdzone) - Fast and standards compliant DNS zone parser.
* [SimpleSignal](https://github.com/larspensjo/SimpleSignal) - High performance C++11 signals.
* [sslscan](https://github.com/rbsec/sslscan) - sslscan tests SSL/TLS enabled services to discover supported cipher suites.
* [Stage](https://github.com/rtv/Stage) - Mobile robot simulator.
* [stdman](https://github.com/jeaye/stdman) - A tool that parses archived HTML files from [cppreference](https://cppreference.com) and generates groff-formatted manual pages for Unix-based systems.
* [stb](https://github.com/nothings/stb) - A range of single-file libraries for C/C++.
* [strong_type](https://github.com/rollbear/strong_type) - An additive strong typedef library for C++14/17/20.
* [StrTk](http://www.partow.net/programming/strtk/index.html) - A C++ library consisting of high performance string processing routines.
* [subprocess.h](https://github.com/sheredom/subprocess.h) - Simple one header solution to launching processes and interacting with them for C/C++.
* [tidy-html5](https://github.com/htacg/tidy-html5) - The granddaddy of HTML tools, with support for modern standards.
* [timers](https://github.com/recp/tm) - This library provides an easy way to set timers and timeouts. As initial version all timers run in single runloop at seperate thread.
* [tiny::optional](https://github.com/Sedeniono/tiny-optional/) - Replacement for std::optional that does not waste memory unnecessarily.
* [tinyxxd](https://github.com/xyproto/tinyxxd) - Standalone version of the little hex dump utility that comes with ViM.
* [tpotce](https://github.com/telekom-security/tpotce) - T-Pot - The All In One Honeypot Platform.
* [trade-frame](https://github.com/rburkholder/trade-frame) - C++ 17 based library (with sample applications) for testing equities, futures, etfs & options based automated trading ideas using DTN IQFeed real time data feed and Interactive Brokers (IB TWS API) for trade execution. Some support for Alpaca & Phemex.
* [tsm](https://github.com/tinverse/tsm) - A Hierarchical State Machine Framework in C++.
* [Tulip Indicators](https://tulipindicators.org) - A C library of over 100 financial technical analysis indicators.
* [uap-cpp](https://github.com/ua-parser/uap-cpp) - C++ implementation of ua-parser (User Agents)
* [ub-canaries](https://github.com/regehr/ub-canaries) - A collection of C/C++ programs that try to get compilers to exploit undefined behavior.
* [units](https://github.com/nholthaus/units) - Compile-time, header-only, dimensional analysis and unit conversion library built on c++14 with no dependencies.
* [uri_parser](https://github.com/ZewoGraveyard/uri_parser) - uri_parser is an URI parser. It is a modularized excerpt of [node.js's http-parser](https://github.com/nodejs/http-parser) source.
* [util-linux](https://github.com/karelzak/util-linux) - util-linux is a random collection of Linux utilities.
* [utillib](https://github.com/ihedvall/utillib) - Common functions when developing applications.
* [value-category-cheatsheet](https://github.com/jeaye/value-category-cheatsheet) A PDF cheatsheet for lvalues, rvalues, and the like.
* [Valhalla](https://github.com/valhalla/valhalla) - Valhalla is an open source routing engine and accompanying libraries for use with OpenStreetMap data. Valhalla also includes tools like time+distance matrix computation, isochrones, elevation sampling, map matching and tour optimization (Travelling Salesman).
* [VarTypes](https://github.com/szi/vartypes) - A feature-rich, object-oriented framework for managing variables in C++ / Qt4.
* [yaLanTingLibs](https://github.com/alibaba/yalantinglibs) - A Collection of C++20 libraries, include struct_pack, struct_json, coro_rpc and async_simple.
* [Wildcards](https://github.com/zemasoft/wildcards/) - A simple C++ header-only template library implementing matching using wildcards.
* [ZBar](http://zbar.sourceforge.net/) - A barcode scanner library, which allows to scan photos/images/video streams for barcodes and return their value.
* [ZXing](https://github.com/zxing/zxing/) - An open-source, multi-format 1D/2D barcode image processing library implemented in Java, with ports to other languages.

# Software
*Software for creating a development environment.*

## Compiler
*List of C or C++ compilers*

* [8cc](https://github.com/rui314/8cc) - A Small C Compiler.
* [c](https://github.com/ryanmjacobs/c) - Compile and execute C "scripts" in one go!
* [ccache](https://ccache.dev/) - Compiler cache designed to speed up recompilation.
* [Clang](http://clang.llvm.org/) - A C compiler for LLVM. Supports C++11/14/1z C11. Developed by LLVM Team.
* [GCC](https://gcc.gnu.org/) - GNU Compiler Collection. Supports C++11/14/1z C11 and OpenMP.
* [PCC](http://pcc.ludd.ltu.se/) - A very old C compiler. Supports C99.
* [Intel C++ Compiler](https://software.intel.com/en-us/c-compilers) - Developed by Intel.
* [LLVM](http://llvm.org/) - Collection of modular and reusable compiler and toolchain technologies.
* [Microsoft Visual C++](https://msdn.microsoft.com/en-us/vstudio/hh386302.aspx) - MSVC, developed by Microsoft.
* [Mold](https://github.com/rui314/mold) - faster drop-in replacement for existing Unix linkers.
* [Open WatCom](http://www.openwatcom.org/index.php/Main_Page) - Watcom C, C++, and Fortran cross compilers and tools.
* [Oracle Solaris Studio](http://www.oracle.com/technetwork/server-storage/solarisstudio/overview/index.html) - C, C++ and Fortran compiler for SPARC and x86. Supports C++11. Available on Linux and Solaris.
* [TCC](http://bellard.org/tcc/) - Tiny C Compiler.

## Online Compiler
*List of online C or C++ compilers*

* [C++ shell](http://cpp.sh/) - C++ shell
* [C Playground - Online C Programming IDE](https://programiz.pro/ide/c) - An online IDE to practice C Programming where you can write, edit, and run code online.
* [codepad](http://codepad.org/) - An online compiler/interpreter, and a simple collaboration tool.
* [coliru](http://coliru.stacked-crooked.com/) - Online compiler/shell with support for various C++ compilers.
* [Compiler Explorer](http://gcc.godbolt.org/) - An interactive compiler with assembly output available.
* [CompileOnline](http://www.tutorialspoint.com/codingground.htm) - Compile and Execute C++ online on Linux.
* [Ideone](http://ideone.com/) - An online compiler and debugging tool which allows you to compile source code and execute it online in more than 60 programming languages.
* [InterviewBit](https://www.interviewbit.com/online-cpp-compiler/) - A simple and easy to use online C++ compiler.
* [Online C++ compiler](https://www.onlinegdb.com/online_c++_compiler) - online compiler and debugger for c/c++.
* [OneCompiler](https://onecompiler.com/) - An online compiler supporting over 70 programming languages and database systems.
* [paiza.io](https://paiza.io/en) - An online C/C++ compiler with multiple files supporting feature, Github(gist) integration and collaborative editing.
* [repl.it](https://repl.it) - A powerful yet simple tools and platforms for educators, learners, and developers.
* [Programiz](https://www.programiz.com/cpp-programming/online-compiler) - An online compiler for learners and developers.
* [Rextester](http://rextester.com/runcode) - Online compiler which provides several compilers(Clang, GCC, MSVC) and several editors.
* [Rise4Fun](http://webcompiler.cloudapp.net/) - Online Visual C++ compiler.
* [Try It Online](https://tio.run/) - TIO is a family of online interpreters for an evergrowing list of practical and recreational programming languages.
* [Wandbox](http://melpon.org/wandbox/) - An online Clang/GCC compiler with Boost available.

## Debugger
*List of C or C++ debuggers*

* [Comparison of debuggers](https://en.wikipedia.org/wiki/Comparison_of_debuggers) - A list of Debuggers from Wikipedia.
* [GDB](https://www.gnu.org/software/gdb/) - GNU Debugger.
* [LLDB](http://lldb.llvm.org/) - The LLDB Debugger.
* [Metashell](https://metashell.readthedocs.org) - An interactive template metaprogramming shell which includes the MDB metadebugger.
* [RAD Debugger](https://github.com/EpicGames/raddebugger) - A native, user-mode, multi-process, graphical debugger.
* [rr](https://github.com/rr-debugger/rr) - Lightweight tool for recording, replaying and debugging execution of applications (trees of processes and threads). Debugging extends gdb with very efficient reverse-execution, which in combination with standard gdb/x86 features like hardware data watchpoints, makes debugging much more fun. 
* [Valgrind](http://valgrind.org/) - A tool for memory debugging, memory leak detection, and profiling.
* [x64dbg](http://x64dbg.com/) - An open-source x64/x32 debugger for windows.

## Integrated Development Environment
*List of C or C++ nominal IDEs.*

* [Anjuta DevStudio](https://sourceforge.net/projects/anjuta/) - The GNOME IDE.
* [AppCode](http://www.jetbrains.com/objc/) - an IDE for Objective-C, C, C++, and JavaScript development built on JetBrains’ IntelliJ IDEA platform.
* [Cevelop](https://www.cevelop.com) - Cross-platform C and C++ IDE based on Eclipse CDT with additional plug-ins.
* [CLion](http://www.jetbrains.com/clion/) - Cross-platform C and C++ IDE from JetBrains.
* [Code::Blocks](http://www.codeblocks.org/) - A free C, C++ and Fortran IDE.
* [CodeLite](http://codelite.org/) - Another cross-plaform, free C and C++ IDE.
* [color_coded](https://github.com/jeaye/color_coded) - A vim plugin for libclang-based highlighting.
* [cquery](https://github.com/cquery-project/cquery/) - A C++ code completion engine for vscode, emacs, vim, etc.
* [Dev-C++](http://sourceforge.net/projects/orwelldevcpp/) - A portable C/C++/C++11 IDE.
* [Eclipse CDT](http://www.eclipse.org/cdt/) - A fully functional C and C++ IDE based on the Eclipse platform.
* [Embarcadero Dev-CPP](https://github.com/Embarcadero/Dev-Cpp) - A fork of Dev-C++ that comes preinstalled with new themes and modern compilers. [Website](https://www.embarcadero.com/free-tools/dev-cpp)
* [Geany](http://www.geany.org/) - Small, fast, cross-platform IDE.
* [IBM VisualAge](http://www-03.ibm.com/software/products/en/visgen) - A family of computer integrated development environments from IBM.
* [ImHex](https://github.com/WerWolv/ImHex) - A Hex Editor for Reverse Engineers.
* [Irony-mode](https://github.com/Sarcasm/irony-mode) - A C/C++ minor mode for Emacs powered by libclang.
* [juCi++](https://github.com/cppit/jucipp) - Cross-platform, lightweight C++ IDE with libclang integration.
* [KDevelop](https://www.kdevelop.org/) - A free, open source IDE.
* [Microsoft Visual Studio](https://www.visualstudio.com/) - An IDE from Microsoft.
* [NetBeans](https://netbeans.org/) - An IDE for developing primarily with Java, but also with other languages, in particular PHP, C/C++, and HTML5.
* [Qt Creator](http://www.qt.io/developers/) - A cross-platform C++, JavaScript and QML IDE which is part of the SDK for Qt.
* [rtags](https://github.com/Andersbakken/rtags) - A c/c++ client/server indexer with for integration with emacs based on clang.
* [Xcode](https://developer.apple.com/xcode/) - Developed by Apple.
* [Yatool](https://github.com/yandex/yatool) - Yatool is a cross-platform distribution, building, testing, and debugging toolkit focused on monorepositories.
* [YouCompleteMe](https://valloric.github.io/YouCompleteMe/) - YouCompleteMe is a fast, as-you-type, fuzzy-search code completion engine for Vim.


## Build Systems

* [Bazel](https://bazel.build) - A multi-language, fast and scalable build system from Google.
* [Bear](https://github.com/rizsotto/Bear) - A tool to generate compilation database for clang tooling.
* [Buck](https://github.com/facebook/buck) - A fast build system that encourages the creation of small, reusable modules over a variety of platforms and languages including C++ developed and used at Facebook. Written in Java.
* [build2](https://build2.org) - cross-platform build, packaging and dependency management toolchain for developing and packaging C/C++ projects.
* [clib](https://github.com/clibs/clib) - Package manager for the C programming language.
* [CMake](https://cmake.org/) - Cross-platform free and open-source software for managing the build process of software using a compiler-independent method.
* [cmake-cookbook](https://github.com/dev-cafe/cmake-cookbook) - This repository collects sources for the recipes contained in the [CMake Cookbook](https://www.packtpub.com/application-development/cmake-cookbook) published by Packt.
* [C++ Archive Network](https://cppan.org/) - Cross-platform C++ Dependency Manager with a lot of packages available.
* [Cget](https://github.com/pfultz2/cget) - Cmake package retrieval. [website](http://cget.readthedocs.io)
* [Conan](https://conan.io/) - C/C++ Package Manager, open sourced.
* [CPM](https://github.com/iauns/cpm) - A C++ Package Manager based on CMake and Git.
* [CPM.cmake](https://github.com/cpm-cmake/CPM.cmake) - CPM.cmake is a cross-platform CMake script that adds dependency management capabilities to CMake. It's built as a thin wrapper around CMake's [FetchContent](https://cmake.org/cmake/help/latest/module/FetchContent.html) module that adds version control, caching, a simple API [and more](https://github.com/cpm-cmake/CPM.cmake#comparison-to-pure-fetchcontent--externalproject).
* [cmake_template](https://github.com/cpp-best-practices/cmake_template) - CMake for C++ Best Practices.
* [dockcross](https://github.com/dockcross/dockcross) - Cross compiling toolchains in Docker images.
* [FASTBuild](http://www.fastbuild.org/docs/home.html) - High performance, open-source build system supporting highly scalable compilation, caching and network distribution.
* [huje](https://codeberg.org/linuxenko/huje) - Source code distribution system, that uses git as its builtin vcs , such as gitea or gogs.
* [Hunter](https://www.github.com/ruslo/hunter) - CMake driven cross-platform package manager for C++.
* [MesonBuild](http://mesonbuild.com) - An open source build system meant to be both extremely fast, and, even more importantly, as user friendly as possible.
* [Ninja](https://ninja-build.org/) - A small build system with a focus on speed.
* [Sccache](https://github.com/mozilla/sccache) - A fast compiler cache for C/C++, with cross-platform support and cloud backed storage options.
* [Scons](http://www.scons.org/) - A software construction tool configured with a Python script.
* [Sconsolidator](http://www.sconsolidator.com/) - Scons build system integration for Eclipse CDT.
* [Spack](https://spack.io/) - A flexible package manager that supports multiple versions, configurations, platforms, and compilers.
* [SW](https://software-network.org/) - Cross-platform C++ (and other langs) Build System and Package Manager with a lot of packages available.
* [tundra](https://github.com/deplinenoise/tundra) - High-performance code build system designed to give the best possible incremental build times even for very large software projects.
* [tup](http://gittup.org/tup/) - File-based build system that monitors in the background for changed files.
* [Premake](http://premake.github.io) - A tool configured with a Lua script to generate project files for Visual Studio, GNU Make, Xcode, Code::Blocks, and more across Windows, Mac OS X, and Linux.
* [Vcpkg](https://github.com/microsoft/vcpkg) - C++ library manager for Windows, Linux, and MacOS.
* [waf](https://gitlab.com/ita1024/waf) - Python-based framework for configuring, compiling and installing applications.
* [XMake](https://xmake.io/) - A C/C++ cross-platform build utility based on Lua.

## Static Code Analysis
*List of tools for improving quality and reducing defects by code analysis*

* [Clang Static Analyzer](http://clang-analyzer.llvm.org/index.html) - A source code analysis tool that finds bugs in C, C++, and Objective-C programs.
* [Control Flag](https://github.com/IntelLabs/control-flag) - A system to flag anomalous source code expressions by learning typical expressions from training data.
* [Cppcheck](http://cppcheck.sourceforge.net/) - A tool for static C/C++ code analysis. - [source](https://github.com/danmar/cppcheck)
* [CppDepend](https://www.cppdepend.com/) - Simplifies managing a complex C/C++ code base by analyzing and visualizing code dependencies, by defining design rules, by doing impact analysis, and comparing different versions of the code.
* [cpplint](https://github.com/cpplint/cpplint) - A C++ style checker following Google's C++ style guide.
* [PVS-Studio](http://www.viva64.com/en/pvs-studio/) - A tool for bug detection in the source code of programs, written in C, C++ and C#.
* [Trunk](https://trunk.io/) - Toolkit to check, test, merge, and monitor code.
* [cpp-dependencies](https://github.com/tomtom-international/cpp-dependencies) - Tool to check C++ #include dependencies (dependency graphs created in .dot format).
* [include-what-you-use](https://github.com/include-what-you-use/include-what-you-use) - A tool for use with clang to analyze includes in C and C++ source files.
* [Infer](https://github.com/facebook/infer) - A static analyzer for Java, C and Objective-C.
* [OCLint](http://oclint.org/) - A static source code analysis tool to improve quality and reduce defects for C, C++ and Objective-C.
* [OptView2](https://github.com/OfekShilon/optview2) - Inspect missed Clang optimizations.
* [Linticator](http://linticator.com) - Eclipse CDT integration of Pc-/FlexeLint.
* [IKOS](https://github.com/NASA-SW-VnV/ikos) - Static analyzer for C/C++ based on the theory of Abstract Interpretation.
* [List of tools for static code analysis](https://en.wikipedia.org/wiki/List_of_tools_for_static_code_analysis#C.2FC.2B.2B) - A list of tools for static code analysis from Wikipedia.

## System tools

* [AQEMU](https://github.com/tobimensch/aqemu) - Official AQEMU repository - a GUI for virtual machines using QEMU as the backend.
* [Beyla](https://github.com/grafana/beyla) - eBPF-based auto-instrumentation of HTTP/HTTPS/GRPC Go services, as well as HTTP/HTTPS services written in other languages (intercepting Kernel-level socket operations as well as OpenSSL invocations).
* [Btop++](https://github.com/aristocratos/btop) - A monitor of resources.
* [Hexdump](https://github.com/KeithBrown39423/Hexdump) - The alternative cross-platform hex dumping utility.
* [iotop](https://github.com/Tomas-M/iotop) - A top utility for IO.
* [Linux-System-Monitor](https://github.com/dgargdipin/linux_process_status) - Linux system monitor with ncurses gui.
* [Netdata](https://github.com/netdata/netdata) - Monitor your servers, containers, and applications, in high-resolution and in real-time. [www.netdata.cloud](https://www.netdata.cloud/)
* [nettrace](https://github.com/OpenCloudOS/nettrace) - nettrace is a eBPF-based tool to trace network packet and diagnose network problem.
* [pe-bear](https://github.com/hasherezade/pe-bear) - PE-bear is a multiplatform reversing tool for PE files. Its objective is to deliver fast and flexible “first view” for malware analysts, stable and capable to handle malformed PE files.
* [socketrace](https://github.com/Asphaltt/socketrace) - eBPF-based tool for tracing socket events in Linux kernel with advanced filtering and aggregation capabilities. It allows you to introspect of socket events in the kernel, no matter tcp/udp/unix domain/netlink sockets.
* [tc-dump](https://github.com/Asphaltt/tc-dump) - A network packet info dumping tool like tcpdump based on `tc-bpf`.
* [ugrep](https://github.com/Genivia/ugrep) - NEW ugrep 5.0: an ultra fast, user-friendly, compatible grep. Ugrep combines the best features of other grep, adds new features, and searches fast. [ugrep.com](https://ugrep.com/)

## Testing tools

* [apib](https://github.com/apigee/apib) - A simple, fast HTTP performance-testing tool.
* [doctest](https://github.com/doctest/doctest) - The fastest feature-rich C++11/14/17/20/23 single-header testing framework
* [criterion](https://github.com/p-ranav/criterion) - Microbenchmarking for Modern C++.
* [dperf](https://github.com/baidu/dperf) - dperf is a DPDK based 100Gbps network performance and load testing software.
* [fio](https://github.com/axboe/fio) - Flexible I/O Tester.
* [httpress](https://github.com/virtuozzo/httpress) - High performance HTTP server stress & benchmark utility.
* [libtree](https://github.com/haampie/libtree) - ldd as a tree.
* [pping](https://github.com/pollere/pping) - Passive ping network monitoring utility (C++).
* [siege](https://github.com/JoeDog/siege) - Siege is an http load tester and benchmarking utility.
* [sysbench](https://github.com/akopytov/sysbench) - Scriptable multi-threaded benchmark tool based on LuaJIT. It is most frequently used for database benchmarks.
* [wrk](https://github.com/wg/wrk) - Modern HTTP benchmarking tool.
* [wrk2](https://github.com/giltene/wrk2) - A constant throughput, correct latency recording variant of wrk.

## Coding Style Tools

* [Artistic Style](http://astyle.sourceforge.net/) - A tool to format C/C++/C#/Obj-C/Java code. Also known as astyle.
* [ClangFormat](http://clang.llvm.org/docs/ClangFormat.html) - A tool to format C/C++/Obj-C code.
* [Clang-Tidy](http://clang.llvm.org/extra/clang-tidy.html) - Clang-based C++ linter tool.
* [EditorConfig](https://editorconfig.org/) - EditorConfig helps maintain consistent coding styles across different editors and IDEs.
* [Prettier](https://github.com/prettier/prettier) - Prettier is an opinionated code formatter. [website](https://prettier.io/)
* [Uncrustify](https://github.com/bengardner/uncrustify) - Code beautifier.

# Resources
*Various resources, such as books, websites, and articles for improving your C++ development skills and knowledge.*

## API Design

* [Beautiful Native Libraries](http://lucumr.pocoo.org/2013/8/18/beautiful-native-libraries/)
* [Designing Qt-Style C++ APIs](https://doc.qt.io/archives/qq/qq13-apis.html)

## Articles
*Fantastic C++ related articles.*

* [CppCon 2023 Presentation Materials](https://github.com/CppCon/CppCon2023) - CppCon 2023 Presentation Materials.
* [CppCon 2022 Presentation Materials](https://github.com/CppCon/CppCon2022) - CppCon 2022 Presentation Materials.
* [CppCon 2021 Presentation Materials](https://github.com/CppCon/CppCon2021) - CppCon 2021 Presentation Materials.
* [CppCon 2020 Presentation Materials](https://github.com/CppCon/CppCon2020) - CppCon 2020 Presentation Materials.
* [CppCon 2019 Presentation Materials](https://github.com/CppCon/CppCon2019) - CppCon 2019 Presentation Materials.
* [CppCon 2018 Presentation Materials](https://github.com/CppCon/CppCon2018) - CppCon 2018 Presentation Materials.
* [CppCon 2017 Presentation Materials](https://github.com/CppCon/CppCon2017) - CppCon 2017 Presentation Materials.
* [CppCon 2016 Presentation Materials](https://github.com/CppCon/CppCon2016) - CppCon 2016 Presentation Materials.
* [CppCon 2015 Presentation Materials](https://github.com/CppCon/CppCon2015) - CppCon 2015 Presentation Materials.
* [CppCon 2014 Presentation Materials](https://github.com/CppCon/CppCon2014) - CppCon 2014 Presentation Materials.
* [C++Now 2018 Presentations](https://github.com/boostcon/cppnow_presentations_2018) - Presentation materials presented at C++Now 2018.
* [C++Now 2017 Presentations](https://github.com/boostcon/cppnow_presentations_2017) - Presentation materials presented at C++Now 2017.
* [C++Now 2016 Presentations](https://github.com/boostcon/cppnow_presentations_2016) - Presentation materials presented at C++Now 2016.
* [C++Now 2015 Presentations](https://github.com/boostcon/cppnow_presentations_2015) - Presentation materials presented at C++Now 2015.
* [C++Now 2014 Presentations](https://github.com/boostcon/cppnow_presentations_2014) - Presentation materials presented at C++Now 2014.
* [C++Now 2013 Presentations](https://github.com/boostcon/cppnow_presentations_2013) - Presentation materials presented at C++Now 2013.
* [C++Now 2012 Presentations](https://github.com/boostcon/cppnow_presentations_2012) - Presentation materials presented at C++Now 2012.
* [C++20. Coroutines](https://habr.com/ru/articles/519464/) - В этой статье мы подробно разберем понятие сопрограмм (coroutines), их классификацию, детально рассмотрим реализацию, допущения и компромиссы, предлагаемые новым стандартом C++20.
* [Multiplexing high performance IO server](https://github.com/Yufccode/Multiplexing-high-performance-IO-server) - Here are three types of high-performance IO servers, implemented through multiplexing. They are select, poll, and epoll, respectively. The code implementation language is C/C++.
* [Анатомия асинхронных фреймворков в С++ и других языках](https://habr.com/ru/companies/yandex/articles/647853/)
* [Яндекс выложил в опенсорс бету фреймворка userver](https://habr.com/ru/companies/yandex/articles/674902/)

## Beginner resources online

* [A tutorial on pointers](https://pdos.csail.mit.edu/6.828/2017/readings/pointers.pdf)
* [A tutorial on portable Makefiles](http://nullprogram.com/blog/2017/08/20/)
* [Building C Projects](http://nethack4.org/blog/building-c.html)
* [C Programming Wikibook](https://en.wikibooks.org/wiki/C_Programming)
* [Introduction to \`fun' C](https://gist.github.com/eatonphil/21b3d6569f24ad164365)
* [Learn C: Free and Open-Source Interactive C Tutorial](https://www.learn-c.org)
* [Learning C with GDB](https://www.recurse.com/blog/5-learning-c-with-gdb)
* [memcpy vs memmove](https://web.archive.org/web/20170620131430/https://www.tedunangst.com/flak/post/memcpy-vs-memmove)
* [Mini Project Template](https://github.com/stepin654321/MiniProject_Template) 
* [POSIX Threads Programming tutorial](https://computing.llnl.gov/tutorials/pthreads/)
* [Qt 6 C++ GUI Development for Beginners](https://github.com/rutura/Qt-6-C-GUI-Development-for-Beginners) - Source Code for the Qt 6 C++ GUI Development for Beginners : The Fundamentals course on udemy
* [The GNU C Programming Tutorial](http://www.crasseux.com/books/ctut.pdf)
* [Templating in C](http://blog.pkh.me/p/20-templating-in-c.html)
* [What a C programmer should know about memory](https://marek.vavrusa.com/memory/)
* [CodeforWin: Learn C Programming, Data Structures Tutorials and Exercises online](https://codeforwin.org/2015/09/singly-linked-list-data-structure-in-c.html)

## Books
*Fantastic C or C++ related books.*

* [Application Development with Qt Creator – Third Edition](https://github.com/PacktPublishing/Application-Development-with-Qt-Creator-Third-Edition)
* [cppbestpractices](https://github.com/lefticus/cppbestpractices) - Collaborative Collection of C++ Best Practices.
* [Free C Books](https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#c) - vhf/free-programming-books/C.
* [Free C++ Books](https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#cpp) - vhf/free-programming-books/C++.
* [List of Free C or C++ Books](https://github.com/fffaraz/awesome-cpp/blob/master/books.md)
* [Modern C++ Tutorial: C++11/14/17/20 On the Fly](https://github.com/changkun/modern-cpp-tutorial)
* [ubbook](https://github.com/Nekrolm/ubbook) - Путеводитель C++ программиста по неопределенному поведению

## Coding Standards

* [Cert C++](https://resources.sei.cmu.edu/downloads/secure-coding/assets/sei-cert-cpp-coding-standard-2016-v01.pdf)
* [Misra C++ 2008](https://www.cppdepend.com/misra-cpp)
* [Autosar C++ 2014](https://www.autosar.org/fileadmin/user_upload/standards/adaptive/18-10/AUTOSAR_RS_CPP14Guidelines.pdf)

## Coding Style

* [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines) - "Official" set of C++ guidelines, reviewed by the author of C++.
* [C++ Dos and Don'ts](http://www.chromium.org/developers/coding-style/cpp-dos-and-donts) - The Chromium Projects > For Developers > Coding Style > C++ Dos and Don'ts.
* [google-styleguide](https://github.com/google/styleguide) - Style guides for Google-originated open-source projects.
* [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)
* [GNU Coding Standard](http://www.gnu.org/prep/standards/standards.html)
* [Linux kernel coding style](https://www.kernel.org/doc/Documentation/process/coding-style.rst)
* [LLVM Coding Standards](http://llvm.org/docs/CodingStandards.html)

## Podcasts

* [CppCast](http://cppcast.com) - The first podcast by C++ developers for C++ developers.
* [CppChat](http://cpp.chat) - A (sometimes) weekly look at what's going on in the world of C++ chatting with a guest from the community.

## Talks

* [C++ Conferences](https://github.com/eoan-ermine/cpp-conferences) - Catalog of C++ conferences.
* [CppCon Talks](https://www.youtube.com/user/CppCon/videos) - The C++ conference.
* [Quick game development with C++11/C++14](https://github.com/SuperV1234/cppcon2014) - CppCon 2014 talk by Vittorio Romeo.
* [Presentation on Hana for C++Now 2015](https://github.com/ldionne/hana-cppnow-2015)

## Videos
*Fantastic C or C++ related videos.*

* [List of C or C++ YouTube Videos](https://github.com/fffaraz/awesome-cpp/blob/master/videos.md)
* [Awesome C Programming Tutorials in Hi Def [HD]](https://www.youtube.com/playlist?list=PLCB9F975ECF01953C) - A collection of detailed C Programming Language Tutorials for Beginners and New Programmers.
* [C++](https://www.youtube.com/playlist?list=PL2F919ADECA5E39A6) - by VoidRealms.
* [C++ Qt Programming](https://www.youtube.com/playlist?list=PL2D1942A4688E9D63) - by VoidRealms.
* [C++ Programming Tutorials Playlist](https://www.youtube.com/playlist?list=PLAE85DE8440AA6B83) - TheNewBoston Official Buckys C++ Programming Tutorials Playlist.
* [C++ Programming Tutorials from thenewboston](https://www.youtube.com/playlist?list=PLF541C2C1F671AEF6) - These are all of thenewboston's C++ programming tutorials.
* [C++ GUI with Qt Playlist](https://www.youtube.com/playlist?list=PLD0D54219E5F2544D) - Official Playlist for thenewboston C++ GUI with Qt tutorials.
* [Caleb Curry's C Programming Tutorials](https://www.youtube.com/playlist?list=PL_c9BZzLwBRKKqOc9TJz1pP0ASrxLMtp2) - An all-in-one playlist of tutorials for C programming.
* [C Programming Tutorials](https://www.youtube.com/playlist?list=PL78280D6BE6F05D34) - All of TheNewBoston's C programming tutorials are right here.
* [Bo Qian's playlist](https://www.youtube.com/user/BoQianTheProgrammer/playlists) - Boost Library, C++ Standard Library, Modern C++, Advanced C++, Advanced STL, ...
* [The Cherno's C++ Playlist](https://www.youtube.com/playlist?list=PLlrATfBNZ98dudnM48yfGUldqGD0S4FFb) - Extensive C++ tutorial series by The Cherno.

## Websites
*Useful C or C++ related websites.*

* [Standard C++](https://isocpp.org/) :zap: - News, Status & Discussion about Standard C++.
* [Build Bench](https://build-bench.com/) - Compare C++ Builds
* [Quick Bench](https://quick-bench.com/) - Quick C++ Benchmarks
* [CppCon](http://cppcon.org/) - The C++ Conference.
* [C++ reference](http://cppreference.com) - A complete online reference for the C and C++ languages and standard libraries.
* [C++ by Example](http://www.cbyexample.com) - Learn C++ by Example.
* [cplusplus.com](http://www.cplusplus.com) - The C++ Resources Network.
* [C FAQ](http://c-faq.com/) - C frequently asked questions.
* [C++ FAQ](http://www.parashift.com/c++-faq/) - C++ frequently asked questions.
* [C++ FQA Lite](http://yosefk.com/c++fqa/) - C++ frequently questioned answers.
* [Guru of the Week](http://www.gotw.ca/gotw/) - A regular series of C++ programming problems created and written by Herb Sutter.
* [Meeting C++](http://meetingcpp.com/)
* [C++ Quiz](https://cppquiz.org) - A simple online quiz that you can use to test your knowledge of the C++ programming language.
* [PVS-Studio’s challenge](https://quiz.pvs-studio.com) - PVS-Studio’s C++ quiz in which you're asked to find errors in code fragments of open source projects.
* [Udemy C++ Courses and Tutorials](https://www.udemy.com/topic/C-plus-plus-tutorials/)
* [C++ Hints](http://cpphints.com/) - Every business day hints about most often C++ mistakes and ways to solve them from PVS-Studio Team.
* [C++ tutorial](https://hackr.io/tutorials/learn-c-plus-plus) - A user ranked online tutorial bank site displaying multiple courses to learn C++ from.
* [C++ Tutorial for Beginners](https://www.scaler.com/topics/cpp) - A comprenhensive tutorial on C++ curated by trained experts.
* [Framework Benchmarks](https://github.com/TechEmpower/FrameworkBenchmarks/tree/master/frameworks/C%2B%2B) - Source for the TechEmpower Framework Benchmarks project for C/C++

## Weblogs
*Useful C or C++ related weblogs.*

* [Coding For Speed](https://codingforspeed.com/) - Coding For Speed DOT COM, Less Execution Time.
* [Eric Niebler](http://ericniebler.com/)
* [Sticky Bits](https://blog.feabhas.com/)
* [Paul Fultz II's Blog](http://pfultz2.com/blog/)
* [ridiculousfish](http://ridiculousfish.com/blog/posts/will-it-optimize.html) - Will It Optimize?
* [Embedded in Academia](http://blog.regehr.org/)
* [Simplify C++](https://arne-mertz.de/)
* [Fluent C++](https://www.fluentcpp.com/) 
* [Bartek's Coding Blog](https://www.bfilipek.com/?m=1)
* [Kenny Kerr](https://kennykerr.ca/articles/)
* [Sutter’s Mill](https://herbsutter.com/gotw/)
* [thoughts on cpp](https://thoughts-on-cpp.com/)
* [Vorbrodt's C++ Blog](https://vorbrodt.blog/)
* [foonathan::blog()](https://foonathan.net/index.html)
* [cppexpert.online](https://cppexpert.online) - Learn the best C++ practices by improving code snippets with some problems or with bad practices.

## Other Awesome Projects
*Collection of useful codes, snippets, ...*

* [30 Seconds of C++](https://github.com/Bhupesh-V/30-seconds-of-cpp)
* [all algorithms](https://github.com/AllAlgorithms/c) - All algorithms.
* [algorithms](https://github.com/xtaci/algorithms) - Algorithms & Data Structures in C++.
* [AMAZON SDE TEST SERIES](https://github.com/mansikagrawal/AMAZON-SDE-TEST-SERIES) - The repo contains the amazon interview questions topic wise.
* [c-algorithms](https://github.com/fragglet/c-algorithms) - C algorithms library.
* [C++ Awesome Pack](https://github.com/juniandotnet/cpp-awesome-pack) - Contains some awesome c++ codes, packed in one.
* [CP-Templates](https://github.com/Priyansh19077/CP-Templates) - Template codes for Competitive Programming with optimised implementations of various algorithms.
* [CPP code snippets](https://github.com/keineahnung2345/cpp-code-snippets) - Some useful C++ code snippets.
* [Data Structures and Algorithms](https://github.com/codemistic/Data-Structures-and-Algorithms) - A repository to help the open-source community with DSA related contributions.
* [GEEKS FOR GEEKS INTERVIEW PRACTICE](https://github.com/mansikagrawal/GEEKS-FOR-GEEKS-INTERVIEW-PRACTICE) - This repo will contain company based questions and answers for various companies.
* [Learn Algorithms](https://github.com/nonstriater/Learn-Algorithms)
* [The-C-20-Masterclass-Source-Code](https://github.com/rutura/The-C-20-Masterclass-Source-Code) - Source code for the C++ 20 Masterclass on udemy.
* [List of open-source C++ libraries](https://en.cppreference.com/w/cpp/links/libs)


# Other Awesome Lists
*Other amazingly awesome lists*

* [lists](https://github.com/jnv/lists) - List of (awesome) lists curated on GitHub.
* [awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness) - A curated list of awesome awesomeness.
* [awesome](https://github.com/sindresorhus/awesome) - A curated list of awesome lists.
* [C++ links](https://github.com/MattPD/cpplinks) - A categorized list of C++ resources.
* [Awesome C++](https://getawesomeness.herokuapp.com/get/cpp) - getAwesomeness()'s mirror.
* [Awesome C++](https://cpp.libhunt.com/) - LibHunt's mirror.
* [Awesome C](https://notabug.org/koz.ross/awesome-c) 1
* [Awesome C](https://github.com/aleksandar-todorovic/awesome-c) 2
* [Awesome Modern C++](https://github.com/rigtorp/awesome-modern-cpp) - A collection of resources on modern C++.
* [AwesomePerfCpp](https://github.com/fenbf/AwesomePerfCpp) - A curated list of awesome C/C++ performance optimization resources.
* [free-programming-books](https://github.com/vhf/free-programming-books) - List of Freely Available Programming Books.
* [Inqlude](http://inqlude.org/) - The Qt library archive.
* [papers-we-love](https://github.com/papers-we-love/papers-we-love) - Papers from the computer science community to read and discuss.
* [awesome-algorithms](https://github.com/tayllan/awesome-algorithms) - A curated list of awesome places to learn and/or practice algorithms.
* [awesome-hpp](https://github.com/p-ranav/awesome-hpp) - A curated list of awesome header-only C++ libraries.
* [awesome-talks](https://github.com/JanVanRyswyck/awesome-talks) - A lot of screencasts, recordings of user group gatherings and conference talks.
* [Projects](https://github.com/karan/Projects) - A list of practical projects that anyone can solve in any programming language.
* [Awesome interview questions](https://github.com/MaximAbramchuck/awesome-interviews) - A list of lists of interview questions for the most popular technologies, including C and C++.
* [nothings/single_file_libs](https://github.com/nothings/single_file_libs) - List of single-file C/C++ libraries.
* [awesome-billing](https://github.com/kdeldycke/awesome-billing) - Billing & Payments Knowledge for Cloud Platforms.

# Interviews

* [interview](https://github.com/huihut/interview) - This repository is a summary of the basic knowledge of recruiting job seekers and beginners in the direction of C/C++ technology, including language, program library, data structure, algorithm, system, network, link loading library, interview experience, recruitment, recommendation.